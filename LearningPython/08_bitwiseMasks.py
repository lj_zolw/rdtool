NONE = 0
ONE = 0b0001
TWO = 0b0010
FOUR = 0b0100
EIGHT = 0b1000

flags = 0
flags |= ONE
flags |= FOUR

print("-------------")

# should be 5
print(flags)
print("-------------")

if flags & ONE > 0:
    print("Contains ONE")

if flags & TWO > 0:
    print("Contains TWO")

if flags & FOUR > 0:
    print("Contains FOUR")

if flags & EIGHT > 0:
    print("Contains EIGHT")
