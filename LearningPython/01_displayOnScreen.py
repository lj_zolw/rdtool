print("Putting a string with print.")

parameter = 10
print("Outputting a parameter with print ", parameter, " following text")

print(3 * ("cat gods: " + str(parameter) + " forever\n"))

print("""Now I have
multiline
print option.""")
