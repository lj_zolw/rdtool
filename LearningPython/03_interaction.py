# chomp is needed to make sure we eliminate whitespaces
# and return.

print("Receiving 3 inputs.")
print("Input 1:")
val1 = input()
print("Input 2:")
val2 = input()
print("Input 3:")
val3 = input()
print("Received: " + val1 + ", " + val2 + ", " + val3)

