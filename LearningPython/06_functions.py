def two_argument_function(first, second):
    print(first + " " + second)

def bar(first, second, third, **options):
    if options.get("action") == "sum":
        print("The sum is: %d" % (first + second + third))

    if options.get("number") == "first":
        return first

two_argument_function("cat", str(2))

result = bar(1, 2, 3, action="sum", number="first")
print("Result: %d" % result)
