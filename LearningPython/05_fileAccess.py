﻿# encoding: UTF-8
import os.path

inputFile = "D:\\Data\\Programmer\\Python\\Workspace\\rdtool\\LearningPython\\inputText.txt"
outputFile = "D:\\Data\\Programmer\\Python\\Workspace\\rdtool\\LearningPython\\outputText.txt"

exists = os.path.isfile(inputFile)
print("Does the output file exist? " + str(exists))

toRead = open(inputFile, encoding='utf-8')
readText = toRead.read()
toRead.close()

print("Contents of the file:")
print(readText)

toWrite = open(outputFile, encoding='utf-8', mode='w')
toWrite.write(readText)
toWrite.close()
