class ReportMasterlistResource:

    dict = {}

    def __init__(self):
        self.dict[self.C01_M02_MPW_Locations()] = "C01M02_MPW_locations.txt"
        self.dict[self.C01_M03_MPW_LocationsDeeds()] = "C01M03_MPW_locations_deeds.txt"
        self.dict[self.C01_M06_Simple()] = "C01M06_correct.txt"
        self.dict[self.C02_M10_OutOfTime()] = "C02M10_outoftime_post_link.txt"
        self.dict[self.C09_M10_SeqNoWrong()] = "C09M10_seqNoWrong.txt"
        self.dict[self.C01_M02_MPW_ConsequencesProgressionLocations()] = "C01M02_MPW_consequences_progression_locations.txt"

    def filename_matching_key(self, key):
        return self.dict[key]

    def keys(self):
        return list(self.dict.keys())

    # pseudo-enum

    def C01_M02_MPW_Locations(self):
        return "C01_M02_MPW_Locations"

    def C01_M03_MPW_LocationsDeeds(self):
        return "C01_M03_MPW_LocationsDeeds"

    def C01_M02_MPW_ConsequencesProgressionLocations(self):
        return "C01_M02_MPW_ConsequencesProgressionLocations"

    def C01_M06_Simple(self):
        return "C01_M06_Simple"

    def C02_M10_OutOfTime(self):
        return "C02_M10_OutOfTime"

    def C09_M10_SeqNoWrong(self):
        return "C09_M10_SeqNoWrong"
