class ReportCollectionResource:

    dict = {}

    def __init__(self):
        self.dict[self.C01M06_CorrectComplete()] = "C01M06_CorrectComplete.txt"
        self.dict[self.C01M02_MPW_Simplest_Locations()] = "C01M02_MpwSimplestLocations.txt"
        self.dict[self.C01M03_MPW_locations_with_deeds()] = "C01M03_MpwLocationsWithDeeds.txt"
        self.dict[self.C01M02_MPW_ConsequencesProgressionLocations()] = "C01M02_MpwConsequencesProgressionLocations.txt"

    def filename_matching_key(self, key):
        return self.dict[key]

    def keys(self):
        return list(self.dict.keys())

    # pseudo-enum methods

    def C01M06_CorrectComplete(self):
        return "C01M06_CorrectComplete"

    def C01M02_MPW_Simplest_Locations(self):
        return "C01M02_MPW_Simplest_Locations"

    def C01M03_MPW_locations_with_deeds(self):
        return "C01M03_MPW_locations_with_deeds"

    def C01M02_MPW_ConsequencesProgressionLocations(self):
        return "C01M02_MpwConsequencesProgressionLocations"
