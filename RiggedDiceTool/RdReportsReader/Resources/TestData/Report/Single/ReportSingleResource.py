class ReportSingleResource:

    dict = {}

    def __init__(self):
        self.dict[self.CZL_MoznaDoprowadzicMagaDo()] = "CZL-mozna-doprowadzic-maga-do.txt"
        self.dict[self.CZL_AleNieMoznaZmusicGoDo()] = "CZL-ale-nie-mozna-zmusic-go-do.txt"
        self.dict[self.CZL_ZwlaszczaGdyCosJest()] = "CZL-zwlaszcza-gdy-cos-jest-ciagle.txt"
        self.dict[self.CZL_BansheeCorkaMandragory()] = "CZL-banshee-corka-mandragory.txt"
        self.dict[self.CZL_RenowacjaObrazuAndromedy()] = "CZL-renowacja-obrazu-andromedy.txt"
        self.dict[self.CZL_SekretyRodzinySzczypiorkow()] = "CZL-sekrety-rezydencji-szczypiorkow.txt"
        self.dict[self.MPW_IrytkaSprzezona()] = "MPW-irytka-sprzezona.txt"
        self.dict[self.MPW_FrontalneWejscieMillennium()] = "MPW-frontalne-wejscie-millennium.txt"
        self.dict[self.MPW_SymptomyKryzysuSwiecy()] = "MPW-symptomy-kryzysu-swiecy.txt"
        self.dict[self.MPW_MalaSzaraMyszka()] = "MPW-mala-szara-myszka.txt"
        self.dict[self.MPW_UkrascZeSwiecyZajcewow()] = "MPW-ukrasc-ze-swiecy-zajcewow.txt"

    def filename_matching_key(self, key):
        return self.dict[key]

    def keys(self):
        return list(self.dict.keys())

    # pseudo-enum

    def CZL_MoznaDoprowadzicMagaDo(self):
        return "CZL_MoznaDoprowadzicMagaDo"

    def CZL_AleNieMoznaZmusicGoDo(self):
        return "CZL_AleNieMoznaZmusicGoDo"

    def CZL_ZwlaszczaGdyCosJest(self):
        return "CZL_ZwlaszczaGdyCosJest"

    def CZL_BansheeCorkaMandragory(self):
        return "CZL_BansheeCorkaMandragory"

    def CZL_RenowacjaObrazuAndromedy(self):
        return "CZL_RenowacjaObrazuAndromedy"

    def CZL_SekretyRodzinySzczypiorkow(self):
        return "CZL_SekretyRodzinySzczypiorkow"

    def MPW_IrytkaSprzezona(self):
        return "MPW_IrytkaSprzezona"

    def MPW_FrontalneWejscieMillennium(self):
        return "MPW_FrontalneWejscieMillennium"

    def MPW_SymptomyKryzysuSwiecy(self):
        return "MPW_SymptomyKryzysuSwiecy"

    def MPW_MalaSzaraMyszka(self):
        return "MPW_MalaSzaraMyszka"

    def MPW_UkrascZeSwiecyZajcewow(self):
        return "MPW_UkrascZeSwiecyZajcewow"
