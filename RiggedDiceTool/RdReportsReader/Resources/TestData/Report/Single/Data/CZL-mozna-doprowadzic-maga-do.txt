+ Konspekt pisany:

Nazywam się Kasia Nowak, ale mówią na mnie Andromeda. To jest... mówiliby, gdyby nie mój mecenas...
Z jakiegoś powodu uznał, że to świetny pomysł, żeby ludzie myśleli, że pod moim pseudonimem artystycznym kryje się mężczyzna. W sumie nie przeszkadza mi to. Wychodzi na to, że Andromeda to tajemniczy artysta, który nigdy nie pokazuje się publicznie.

Mam całkowicie zwyczajne imię i nazwisko - rodzice chyba po prostu nie mieli weny... Przez to musiałam sobie wymyślić pseudonim artystyczny. Mam siostrę, oddaną kurę domową i świetnego szwagra, którzy już dorobili się dwójki słodkich, kochanych berbeci. No dobra, czasem są nieznośni, ale ciocia jest od tego, żeby ich rozpieszczać, a rodzice niech się potem martwią.
Nie odwiedzam ich często - nie chcę ich narażać na coś... dziwnego.

Nieświadomie mój mecenas oddał mi przysługę - dzięki temu, że Andromedę uważa się za faceta, żaden mag nie podejrzewa, że to kobieta stoi za tymi łamiącymi tajemnicę obrazami. No dobrze... prawie żaden. Jeden wie. Ale to taki słodki i nieporadny dzieciak... co wcale nie znaczy, że niegroźny.

Tak... wiem o magach. Wymazywali mi już pamięć tyle razy, że dawno straciłam rachubę. Z jakiegoś powodu za każdym razem, prędzej czy później wszystko sobie przypominam. Nie jest to normalne. Wiem o tym, bo kilka razy sprawdziłam. Naprawdę robię, co mogę, żeby się przed żadnym magiem nie zdradzić...
Fakt, że w mojej obecności jeden z nich rozważał, czy nie zawalić na mnie budynku bo to łatwiejsze od wymazania pamięci, tylko zwiększa moją determinację.

Magia jest... potężna. Gdzieś czytałam, że wystarczająco wysoki poziom technologii niczym nie różni się od magii, ale nie mogę - po prostu nie mogę uwierzyć w to, że wśród nas kryje się tak niesamowicie rozwinięta cywilizacja. Zwłaszcza, że magowie - tak sami siebie nazywają - nie używają wielu gadżetów. No i... jaka technologia mogłaby stworzyć jakąś istotę z powietrza? Albo wezwać ducha zmarłego?

Nie, to jest coś więcej i nikt mi nie wmówi, że jest inaczej.

Czy magia jest zła? Nie, widziałam różne jej działania. I te złe i te dobre. Magowie... choć mają moc, myślę, że są takimi samymi ludźmi jak dowolny Jan Kowalski. Jedni są dobrzy, inni są źli. Dotąd miałam szczęście - nawet, kiedy trafiałam na tych, dla których zabić to jak splunąć, obok był ktoś ze zgoła innymi poglądami.
Uwierzcie, nie ma nic tak przerażającego, jak stać całkowicie sparaliżowanym, bez możliwości zrobienia czegokolwiek, podczas gdy dwóch zupełnie obcych facetów kłóci się, co będzie lepsze - zabić czy wymazać pamięć...

Nie szukam magii. Tak po prawdzie staram się jej unikać.
Chyba jestem przeklęta... nie wychodzi mi.
Zaczęło się na studiach... a potem było gorzej.

Czasem jest tak, że natykam się na magię raz na rok, czasem... kiedyś było to pierwszego dnia po przyjeździe do nowego miejsca.
Moim sposobem na unikanie magii jest zmiana miejsca. Podobno piorun nigdy nie uderza dwa razy w to samo miejsce - to bzdura. Magia... jeśli w pobliżu są magowie to prędzej czy później zdarzy się coś magicznego. A - znając moje szczęście - ja będę w pobliżu... lub nawet w samym środku.
Dlatego jak tylko coś takiego się zdarzy, uciekam. Przenoszę się.

Daje mi to jeszcze jeden zysk - choć potężni, magowie nie są wszechwiedzący i nie dzielą się informacjami. Wiem, bo gdyby to robili, dawno już by mnie namierzyli.

Przenoszenie się z miejsca na miejsce w sumie mi nie przeszkadza. Nawet to lubię. Szkoda tylko, że jak już się zadomowię, to dzieje się... coś. Dziwnego. A potem wymazują mi pamięć.

Zwykle wiem, kiedy coś magicznego dzieje się niedaleko. To takie dziwne uczucie, które przez lata nauczyłam się rozpoznawać... połączenie lekkiego zatrucia pokarmowego, poczucia oderwania od rzeczywistości i PMS. Ale to i tak nie pomaga. Nigdy nie udało mi się uniknąć wmieszania w magię... cóż, przynajmniej daje mi to inspirację do dzieł.

Ale nawet tu magia nie dała mi spokoju - jak narysowałam pierwszy obraz, dokładnie przedstawiający to, co widziałam, to przyszedł jakiś terminus i znów wymazał mi pamięć. Nie wiem, jak mnie znalazł. Zresztą... czy to istotne? Miał magię. Moje obrazy idą przez mecenasa, a magowie mogą czytać pamięć. Wiem z autopsji. Co ciekawe, kiedy przeczytali moją, nie doszukali się nic o tym, że wiem o magii. To na pewno nie jest normalne. Tamci byli całkiem kompetentni, a przecież profesjonalista nie polegałby tak na czymś, co może zawieść.

Magowie starają się ukrywać swoje istnienie. W sumie ich rozumiem...

Tylko czemu ja muszę mieć takiego pecha?! Sprawdzałam ułożenie gwiazd w momencie swoich narodzin, wszystkie horoskopy, jakie mogłam znaleźć, wróżyłam z fusów, plułam przez lewe ramię i w ogóle. Sięgnęłam nawet do "twardego" okultyzmu, ale to nie jest coś dla mnie... Jeszcze nie jestem tak zdesperowana. Zawsze interesowałam się okultyzmem. Trochę. Znam symbole ochronne różnych obrządków, sposoby robienia amuletów, przyzywania duchów i tak dalej...

Niestety, na magię to wszystko nie działa. Ale nie tracę nadziei. Wciąż szukam.

Na razie pilnuję, żeby zawsze mieć przy sobie srebrny wisiorek na stalowym łańcuszku. I żeby wisiorek nigdy nie dotknął ciała. Przekonałam się o tym boleśnie - oparzenia od srebrnego łańcuszka leczyłam dobry miesiąc. A mag, który wymazywał mi pamięć odwalił niezłą fuszerkę - jego zdaniem oparzyłam się, kiedy gorąca nitka spaghetti owinęła mi się dookoła ręki... Co on, w kuchni nie był?! Z drugiej strony to był ten z mniej przyjemnych, pewnie nie był...

Ale za to przynajmniej ten nowy terminus jest słodki. Uwziął się, żeby obserwować moje obrazy i doszukiwać się w nich wskazówek zdradzenia ich wielkiej tajemnicy - on to nazywa złamaniem Maskarady. Tak, przez wielkie "M". Co on, "Zmierzchu" się naczytał, czy co? To brzmi idealnie jak tekst stamtąd... Co gorsza, chłopak ma rację, że mi się takie rzeczy przytrafiają...

Ale czemu, czemu, do jasnej a nagłej, musi psuć mi robotę?! Maluję dwa tygodnie w pocie czoła, a potem przychodzi on i nie dość że znów wymazuje mi pamięć, to jeszcze robi z mojego dzieła kicz ostatni..!
...Co gorsza, mój mecenas te właśnie kicze uwielbia. Zachowuję je i opycham mu jak potrzebuję większej kasy... Choć te najgorsze po prostu niszczę... Pod pewnymi rzeczami się nie podpiszę, magia czy nie!

I żeby jeszcze to był tylko ten młodzik. Raz czy dwa wlazło mi do mieszkania kilku takich i zrobiło to samo... tylko jeszcze gorsze szkody spowodowali. Z ich rozmów wynikało, że wyłapali moje obrazy w wyniku rutynowych działań...
Cóż, z czasem nauczyłam się, jak malować, żeby ich nie prowokować.

Chyba najbardziej udany obraz, którego nie ruszyli, to był ten z tym świetnie ubranym magiem z mieczem w ręku...
Ten miał chyba nadludzkie moce. Posiekany, okrwawiony, ale szedł... I tylko w jego spojrzeniu było coś...
A potem, zanim wymazał mi pamięć, przeprosił. I... uwierzyłam mu.

Obrazy... źródło mojego utrzymania. Moja siostra nie wierzy, że z tego można żyć. Kiedyś kupiła ode mnie jeden obraz... Z litości! Jakbym go opchnęła mecenasowi, dostałabym trzy razy tyle, a ona kupiła go z litości! Ale niech tak będzie, przynajmniej będzie się lepiej czuła.

Jednak jej litość nade mną nie jest dość wielka, żeby iść ze mną na imprezę. Uważa, że zaleję się w trupa i będzie musiała mnie odnieść do domu. A ja... od dawna się nie upijam. Nie to, że nie lubię... Po prostu nie chcę ryzykować, że powiem o jedno zdanie za dużo.

Ech... dawno już nie działo się nic dziwnego w moim pobliżu i nikt nie wymazał mi pamięci. Doświadczenie mi mówi, że niedługo będę znów musiała uciekać...
A wiem, że nie wymazano mi pamięci, bo pamiętam to uczucie... Magowie nazywają to skażeniem. Wymazanie pamięci powoduje, że część magii jakby zostawała w człowieku... Od tego czuję się gorzej. Stąd wiem, że nic takiego się nie stało.

Ciekawe, kiedy znów mnie coś trafi...

Ten młodzik powinien się pojawić jakoś w czwartek... pokazuje się u mnie co miesiąc, z niemal idealną dokładnością. Oczywiście, za każdym razem wymazuje wspomnienie o swojej wizycie, więc za każdym razem muszę udawać zaskoczenie.
Reagowałam już różnie... Krzyków nikt nie usłyszał, cios patelnią nie dosięgnął celu - chroniła go jakaś bariera. Chowanie się nic nie dało. Rozmowa nic nie zmieniła... Choć na rozmowie najwięcej zyskałam - trochę mi opowiedział. Zresztą, w końcu czemu nie, i tak to wszystko wymaże, prawda?

Samira?
Fajna dziewczyna. Iluzjonistka. Nie ma nic wspólnego z magią. Kiedyś ją zalałam i wyznała mi w wielkim sekrecie, że wszystkie jej sztuczki to lustra... no i laptop. Oczywiście, z dużo mniejszym sensem, ale cóż... nieważne. Ważne, że nie jest magiem.
Nigdy nie sądziłam, że dojdzie do tego, że nie będę potrafiła zaufać komuś, kogo nie potrafię spić... No cóż, fuchy jako barmanka się przydały.
Ale nieważne. Samira to fajna babka. Zna się na swojej robocie i nie pozwoli byle komu sobie w kaszę dmuchać.
Ciekawe, ile razy jej kasowali pamięć - jest tak dobra, że nawet mag mógłby się nabrać.


================
Senior: malarz

Niski senior: aktor, barman

Wysoki junior: okultysta, imprezowicz, wiedza o magii

Junior: kierowca
======================

----
----

-2. Andromeda często przemieszcza się z miejsca na miejsce, zwykle po kolejnym zetknięciu się z magią i wymazaniu pamięci. Do tej mieściny przybyła dwa miesiące temu, mimo, że nikt nie wymazał jej pamięci przed przeprowadzką - tym razem zrobiła to na wyraźną prośbę swojego mecenasa. Było to bardzo nietypowe (jak na niego), ale nie było to nic, czego nie mógłby zrobić normalnie.

-1. Po przybyciu pośród lokalnej bohemy artystycznej, Andromeda szczególną uwagę zwróciła na Samirę - "niezawodną czarodziejkę". Naturalnie, podejrzewała ją o bycie magiem. Jak już trochę lepiej się poznały, Andromeda wzięła Samirę do baru i umiejętnie zalała w trupa. Samira, wypytana o sekrety swojej sztuki, wyjawiła, że to wszystko lustra, światła i laptop, ale przede wszystkim lustra. I faktycznie, na żadnym z przedstawień Samiry, srebrny medalionik Andromedy nawet się nie nagrzał.

1. Na wielką prośbę burmistrza i oficjeli, Andromeda zgodziła się zorganizować swoją galerię. Oczywiście, udawała własną asystentkę... W końcu wszyscy wiedzą, że Andromeda to facet (oczywiście gej), prawda..? Niestety, mimo, że galeria była zamówiona, początkowo przyszło tylko kilka osób, w czym Samira.

----
**Z myśli Kasi**
Eeej... No co to ma być? Zamawiają galerię, dopraszają się wręcz o nią a teraz pies z kulawą nogą się nie pojawi? Mecenas nie będzie zadowolony...

2. Jeden z gości, ubrany jak na pogrzeb, zapytał, czy Andromeda mógłby namalować dla niego obraz na zamówienie. Kasia już się ucieszyła, ale potem usłyszała, o jaki obraz chodzi.
Malowanie psów zdechłych lata temu to nie jest to, co Kasie lubią najbardziej... Spróbowała dowiedzieć się, o co chodzi...

----
**Z przemyśleń Samiry, w galerii**
Jak za machnięciem czarodziejskiej różdżki, entuzjazm Andromedy zniknął, gdy tylko usłyszała o psie.
No dobra... Leszczynie coś odwaliło. Jego żona umarła pięć lat temu. Jego pies padł trzy lata temu. Dzisiaj, w dzień nie powiązany z żadną rocznicą żadnego z tych wydarzeń (nie jestem tu tak długo, ale wiem)... Chce, by Andromeda namalowała mu portret. Psa. Żeby jeszcze Kasię połechtał. Ale nie - ona tylko dlatego, bo akurat jest pod ręką i będzie szybko i dobrze... Wtrąciłam się oczywiście do rozmowy. Powiedziałam, że Andromeda to drogi malarz, a portrety psów w ultra-realistycznej dokładności są jeszcze droższe niż zwykle. O dziwo, łyknął to.
Muszę zorganizować dla Andromedy coś miłego. Zasłużyła sobie za ten niewypał z galerią. A naprawdę obrazy ma piękne...

----
**Z myśli Kasi**

No nie wiem... Czy ten dzień może pójść gorzej?
Portrety psów mam malować? Ze zdjęcia? Facet potrzebuje kogoś od photoshopa, a nie mnie...
Co to ma być, "Kobieta z psem". To już wolałabym robić replikę damy z łasiczką...
Zwłaszcza, że ani ona ani ten rasowy kundel nie byli szczególnie wdzięcznym materiałem...
Ale jest w tym coś dziwnego. Facet wyraźnie plątał się w zeznaniach, kiedy zapytałam, po co mu ten portret teraz. W końcu przyznał, że chce go dać żonie... Ale przecież ona nie żyje... Mowy nie ma. Moje obrazy nie będą gnić w katakumbach. Nawet te kicze tak uwielbiane przez mecenasa na to nie zasługują...
Prośba była na tyle dziwna, że wypytałam faceta (oczywiście nie wprost), o rzeczy mogące wskazywać na magię. Ale nie, nic takiego nie powiedział. I akurat nie kręcił i nic nie ukrywał.
Chociaż tyle... Magia za tym bezpośrednio nie stoi... Ale to i tak trochę śmierdzi.
Za pięć dni będzie tu mój oswojony terminus. Najwyżej go na to napuszczę.
Wiem! Zacznę prowadzić pamiętnik na jego użytek. Niech się dzieciak ucieszy.

----
----

3. W końcu pojawiają się oficjele, chwilę po przybyciu ekipy telewizyjnej.
Wszystko wraca do normy, choć jest coś niezwykłego - spora ich część jest ubrana na czarno. I to nie tylko oficjeli, ale też zwykłych ludzi. Jakieś piętnaście procent - sporo; dość, by byli zauważalni. Andromeda prosi Samirę o wybadanie tematu, a sama pyta burmistrza, o co chodzi.

----
**Z myśli Kasi**

...Pięknie. Rocznica śmierci księdza i nikt mi o tym nie powiedział. I wygląda na to, że wszyscy zostali tak samo zaskoczeni jak ja. Oficjalnie, ten dzień może pójść gorzej.
Ale taki masowy spontan? Flash mob na grobie? Eej... To naprawdę nie w porządku. I... nienormalne.
Może i dobrze, że ten dzieciak niedługo będzie... Aha, akurat wtedy, gdy Samira zaprosiła mnie na swój występ... No cóż, trochę go pounikam. Mam wprawę.

----
**Z myśli Samiry**

No dobrze. Andromeda prosiła swoją ulubioną adeptkę sztuk tajemnych (mnie), abym spróbowała dowiedzieć się coś więcej o całej tej sprawie. Okazuje się, że rok temu jakiś pijak przejechał księdza i... tyle. Historia jakich wiele. Więcej, ksiądz nie był jakimś unikalnym wzorem cnót. Był... zwyczajny. Podobno w porządku. To, co nie jest zwyczajne, to fakt, że kierowca targany wyrzutami sumienia, popełnił samobójstwo jakiś tydzień później.
To naprawdę nie jest powód, by robić z księdza męczennika lub bohatera.
A oni robią. Spontanicznie. Nie podoba mi się to.
Cóż, może niedługo czas zmienić miejsce pobytu?

----
----

4. Mimo wszystko galeria okazuje się sukcesem, przez trzy dni odwiedza ją całkiem sporo osób. Niektórzy nawet coś kupują. Andromeda stał się modnym malarzem... W tej miejscowości. A fakt, że nikt go nigdy nie widział, jedynie zwiększa atrakcyjność.

----
**Z myśli Kasi**
W sumie to nawet nieźle wyszło. I jakaś kasa z tego będzie...
Fajnie ^_^

----
----

5. Pod koniec trzeciego dnia, tuż przed zamknięciem, przychodzi kobieta, która szybko przegląda obrazy, aż zatrzymuje się zafascynowana jednym z tych mroczniejszych ("Bezsilność"). Ten obraz zainspirowany był kontaktem z tymi mniej przyjemnymi terminusami... Dwie osoby stoją nad trzecią, wyraźnie bezsilną, i zdają się wydawać wyrok... Wszystko utrzymane w mrocznej tonacji, sądzący mają nierozpoznawalne twarze, a na twarzy leżącej osoby maluje się strach i właśnie bezsilność.
Kobieta kupuje ten obraz. Wypytana przez Kasię, opowiada, dlaczego tak ją zafascynował...

----
**Rozmowa Kasi i Samiry**

- Właśnie zamknęłam galerię. Nie uwierzysz, kto przyszedł tuż przed zamknięciem...
- Burmistrz? Poprosić o to, byś udekorowała obrazami ratusz?
- Nie... ciekawiej.
- Grabarz? Może też chciał portret psa? <śmiech>
- Nie, dasz mi powiedzieć? Matka tego faceta, co przejechał księdza. Kupiła jeden z obrazów, mówiąc, że dokładnie oddaje to, jak postąpiono z jej synem...
- O... Możesz powiedzieć mi coś jeszcze?
- Ona twierdzi, że jej syn był trzeźwy, że to ksiądz był zalany. Mówi, że lubił wypić, ale władza wszystko zatuszowała. Jej zdaniem oni wydali wyrok na jej syna. Jeśli tak się czuje, nie dziwię się, że kupiła "Bezsilność".
- Nie było mnie tu wtedy. Tak, jak Ty, dużo podróżuję. Ale przecież ten kierowca był spokrewniony z burmistrzem, prawda?
- Ale to był rok wyborczy.
- Wyparł się krewnego? Andromedo, chcesz powiedzieć, że ten burmistrz, którego znamy, poświęcił swojego krewnego, by mieć większe szanse w wyborach?
- Większe? Jakiekolwiek. Gdyby się go nie wyparł, ludzie by go odrzucili. Co nie zmienia faktu, że to... okropne.
- No... Nie widzę tego.
- Cóż... nie wiem, jak długo będę chciała tu zostać. Tu się dzieje coś dziwnego...
- Zrobiło się nieprzyjemnie. Tu się zgadzam. Też myślałam o wyjeździe.

----
----

6. Po podsumowaniu wyników galerii (całkiem niezły sukces) Andromeda postanawia przygotować plakat reklamujący występy Samiry. W międzyczasie zaczyna prowadzić "pamiętnik dla terminusa", w którym uwzględniła to, czego dowiedziała się od kilku osób, które przyszły zamówić obrazy u Andromedy.
Odwiedzili ją - tego samego dnia - wikary i grabarz, ten drugi zaraz po tym, jak wyszedł ten pierwszy.

----
**Z myśli Kasi po wizycie wikarego**

Fuj. Odrzuciłam tą ofertę. Malowanie zmarłego rok temu księdza pod dokładne dyktando zdjęć i widzimisię wikarego? W pozie władcy świata? Niszczyciela zła? Zwłaszcza po tym, czego się dowiedziałam? No i... ciekawe, ale wikary w swoim opisie nie uwzględnił żadnych symboli religijnych.
Nie, malowanie "do ostatniej plamy na zębie" mnie nie kręci...
Zwłaszcza, że wikary zachowywał się jak ostatni fanatyk. Boję się takich ludzi...

----
**Z myśli Kasi po wizycie grabarza**

To ewidentnie śmierdzi magią. Gdzie ten terminus jak mógłby się przydać?
Cóż... "kotwica", która wyrywa część ciebie i zostawia na cmentarzu, jeśli się oddalisz...
Nie dająca spać po nocach. Ten człowiek był zdesperowany. Był u lekarzy, znachorów... gdzie mógł. W końcu wyszukał symbol okultystyczny. To jeden z tych potężniejszych. Ponoć polecił mu go znachor.
Na plus trzeba grabarzowi oddać, że nie chciał użyć ludzkiej krwi - a ponoć krew musi być składnikiem farby... W dodatku ma to być malowane na specjalnie wygarbowanej, posrebrzanej skórze... Dobrze, że nie ludzkiej.
Problem w tym, że ja mu tego nie przygotuję - postawiłam warunek, że jeśli Andromeda zgodzi się przyjąć zlecenie, on dostarczy materiały.
Zrobiło mi się go żal, skontaktuję go z kilkoma okultystami, którzy z niego za bardzo nie zedrą... No i liczę na terminusa. W końcu od tego chyba jest. No i - przyczepił się do mnie, żeby coś, znaleźć, to mu to dam.

----
----

7. Zaintrygowana opowieścią grabarza Andromeda idzie obejrzeć cmentarz. Rozmawia z robotnikiem wykonującym pomnik zmarłego księdza (nie podoba mu się, że na pomniku księdza nie ma nawet najmniejszego krzyżyka) i nowym księdzem, siwiuteńkim staruszkiem, który nie rozumie, co się tu dzieje, i który mówi, że jego wierni zakrzyczeli go, gdy mówił, że wypada, żeby ksiądz nosił krzyż... Starutki ksiądz to dobra osoba, jest bardzo zmartwiony tym, co się dzieje i nie rozumie, co opętało wszystkich.
Wszystko, czego się dowiedziała, wpisuje w swój świeżo założony pamiętnik dla terminusa.

----
**Z myśli robotnika pracującego nad pomnikiem**

To nie po bożemu... Dziwni ludzie tu mieszkają. Swojego byłego księdza traktują jak świętego, nie czekając aż ogłosi się jego świętość, swojego aktualnego księdza nie słuchają... Ten pomnik... to ja go nie chcę robić. Ale szef każe. Ten pomik ma w sobie coś bluźnierczego. Nawet jednego, małego krzyżyka nie ma. A wizerunek księdza... Aniołowie nie są tak pysznie i dumnie pokazywani...

----
----

8. Przedstawienie Samiry. Wyszło doskonale. Samira pokazała kilka nowych sztuczek. W środku wystąpienia ktoś efektownie spadł z krzesła - Andromeda zorientowała się, że to jej terminus. <facepalm>

----
**Z myśli Samiry podczas wystąpienia**

Dziwne... lekko mi się kręci w głowie. I tak trochę... moje bransolety. Lepiej... mam! Lusterko do odczyniania uroków! Mam je przy sobie.
O... ktoś spadł z krzesła? To... niemożliwe. To zbieg okoliczności.

----
**Z myśli terminusa, leżącego na ziemi**

Och... Standardową praktyką zgodnie z edyktem tysiąc czterysta... nie pamiętam którym, mistrz by mnie obsobaczył... jest używanie lekkiego zaklęcia rozpoznawczego w kierunku podejrzanych iluzjonistów. Jeśli są magami, powinni odpowiedzieć. Ale ona... To nie może być srebro. Gdyby to było srebro, poparzyłaby się swoim własnym zaklęciem. Dlaczego walnęła mnie falą uderzeniową? Oczywiście, tarcza zaabsorbowała cios, ale uderzenie mnie przewróciło! Wszyscy się na mnie gapią! Ja z nią porozmawiam potem...

----
----

9. Za cichą namową Andromedy Samira poprosiła jako ochotnika do sztuczki magicznej terminusa. Gdy położyła mu ręce na ramionach, gość zwinął się z bólu i rzucił szczupakiem przed siebie. Samira ma grube, srebrne bransolety, a terminus (młody) chodzi na stałej tarczy ochronnej. Po Samirze nie dało się poznać reakcji srebra. Publiczność oczywiście uznała całą rzecz za ustawioną.

----
**Z myśli Samiry, bardzo wszystkim zaskoczonej**

OooKeeej. Andromeda dała mi subtelny znak, że może go wziąć na ochotnika. Jasne, czemu nie. Usiadł przede mną, i szepnął "terminus, świeca", czy coś w tym stylu. Naturalnie, odpowiedziałam "nie, żadnego ognia" i poprosiłam, by usiadł. Położyłam mu ręce na ramionach, a on... jak oparzony, rzucił się do przodu! Spojrzał na mnie... Jak Bogusław Linda lub Sylwester Stallone. Ale nie Seagal. Na twarzy Seagala niczego nie widać.

----
**Z myśli mocno poparzonego srebrem terminusa**

Moja tarcza weszła w reakcję!
Jak ona to wytrzymuje?!
To niemożliwe! I co ona bredzi?! Skąd ona jest? Na pewno włada magią! Ale dlaczego...
To na pewno jest srebro.
Nie mag i nie człowiek... //Vicinius//?

----
----

10. Po udanej imprezie Andromeda i Samira poszły na drinka. Andromeda powiedziała zaciekawionej Samirze, że ten chłopak trochę ją śledzi, ale że jest słodki. Aha. Rozeszły się. Andromeda poszła do siebie czekać na terminusa, a Samira do siebie... gdzie go znalazła.

----
**Spotkanie Samiry z terminusem**

- Ty! Leszcz śledzący Kasię! Co ty...
- Momencik...
- ...robisz w moim domu?! <bardzo podnosi głos> Won!
- Powiedziałem...
- Już cię tu nie ma! Znam aikido!
- Nawet nie próbuj! <ostro, z pogardą> Aaaaa! <Samira trafia go srebrnymi bransoletami w tarczę>
- Wynoś się! Precz! Won! Jazda!

----
----

11. Samira nie doceniła faktu, że w jej pokoju znajduje się obcy chłopak. Terminus wyszedł z pozycji dominującej, na co Samira przywaliła mu ręką ze srebrną bransoletą i terminus ucierpiał. Doszło do pyskówki i okładania terminusa czym popadnie, więc mag rzucił na Samirę czar zapomnienia. Samira używając jednego z luster odbiła czar na terminusa, przez co ten zapomniał wszystko o sobie samym. Przerażony, uciekł przez okno. Trzecie piętro. Samira natychmiast zadzwoniła do Andromedy.

----
**Z myśli Kasi**

Ale... czemu on na nią napadł? I jak ona go zbiła? XD
Nie, w sumie to nie jest zabawne...
Muszę coś zrobić, żeby ona nie dowiedziała się o magii, bo ją skrzywdzą...
A jak skrzywdzi terminusa, to już w ogóle ją skrzywdzą, dla zasady...
Ale JAK ona to zrobiła? I - co?!
No i... jedzie do mnie teraz. Jak on teraz tu przyjdzie, może być nieciekawie...

----
----

12. Terminus zapukał do drzwi Andromedy.

----
**Z myśli Kasi**

W sumie... może go chwilę zatrzymam. Ciekawa jestem, jak na siebie zareagują. A chyba ich powstrzymam przed pobiciem się... Mam nadzieję.

----
----

13. Terminus, w szoku po tym, co spotkało go u Samiry i zaskoczony tym, że Andromeda nie wpuszcza go do mieszkania, robi błąd. Mówi "zawsze mnie wpuszczałaś". Gdy Andromeda jest coraz bardziej zdenerwowana, czyści jej pamięć ostatnich kilku minut i udaje, że jest świadkiem Jehowy. Andromeda zagina go w religioznawstwie, ale robi to stopniowo, by dać czas Samirze na dotarcie.

14. Samira dociera do mieszkania Andromedy i dostrzegają się z terminusem w tym samym czasie.
Terminus cicho szepcze do Andromedy, żeby uciekała, że on Samiry nie zatrzyma. Samira szykuje się do zrobienia czegoś gwałtownego. Andromeda staje pomiędzy nimi, wymuszając w miarę pokojowe rozejście i wymuszając na magu obietnicę pojawienia się następnego dnia. Zaprasza Samirę do siebie. Mag odchodzi.

----
**Z myśli terminusa**

Nie wiem, kim jestem! Nie mam pojęcia! Pamiętam, kim jest ta pani Katarzyna - jest obiektem, który staram się chronić. Pamiętam, że ta "Samira" jest czymś dziwnym. Czymś zupełnie innym niż się spodziewałem. Ale co ja tu robię? Czy ja chronię Katarzynę przed Samirą? Ona o mnie nie pamięta. Jaka jest moja funkcja?
Nie znam mocy tej Samiry. Nie wiem, czym jest. Wiem, że z jakiegoś powodu ta Katarzyna jest dla mnie ważna. Muszę... muszę powstrzymać Samirę przed zrobieniem krzywdy Katarzynie.
Ale jak?
Może, jeśli zrobię odpowiedni hałas, pojawią się terminusi. Tak, czy inaczej, mam pewien plan...

----
----

15. Samira martwi się o swoje lustra, ale nie chce być sama, więc Andromeda zgadza się spędzić noc w domu Samiry.
Samira opowiada o lustrze odbijającym uroki - Andromeda podejrzewa, że terminus wyczyścił sobie pamięć...
Samira mówi też, że ma ponad dwieście luster. Potencjał magiczny tkwiący w starych lustrach przeraża Andromedę.

----
**Z myśli Kasi**

Odbijanie uroków odbijaniem uroków, ale żadnych starych luster w moim domu!
I tak mam już dość przygód z magią...

----
**Z myśli Samiry**

To nie może być prawda. Ja wiedziałam, że część z tych luster była wykorzystywana w różnych rytuałach okultystycznych, ale to były właśnie, no, rytuały okultystyczne! To nie jest prawda! Te rytuały...
Ten chłopak wyskoczył z wysokości drugiego piętra! To jakiś cyborg. I śledził Kasię? Cały czas śledził Kasię?
To musiała być jakaś... laserowa wiązka holograficzna... Coś ze światłem! Bo ja to odbiłam lustrem!
Przecież magia nie istnieje!
Jestem iluzjonistką, chyba wiedziałabym najlepiej?

----
----

16. Samira z Andromedą jadą do mieszkania Samiry.
Tam następuje atak przygotowany przez terminusa.
Na wejściu do części mieszkania Samiry dziewczyny zastały powyciągane lustra. Samira zorientowała się, że coś jest nie w porządku - ona ich tak nie zostawiła. W tym momencie nastąpił atak przez lustra.
Samira odbiła pierwszy atak lusterkiem odbijającym uroki, ale wówczas lustro, z którego atak przyszedł, roztrzaskało się.
Następnego ataku Samira już nie odbijała.

17. Andromeda próbowała zasłonić Samirę, licząc, że atakujący mag powstrzyma się przed uderzaniem w nią. To jednak nic nie dało. Obydwie mocno oberwały.

----
**Z myśli terminusa**

Dobrze. Być może ten vicinius czerpie moc z luster - inaczej czemu nie miałby ich rozbić? Na pewno nie chce zdradzić się przed Katarzyną. Szkoda. Gdyby udało mi się doprowadzić do złamania Maskarady miałbym pretekst do wezwania terminusów. A tak...
Cóż, niech cierpi.
Nie!
Katarzyna rzuciła się na nią! Co za katastrofa, też obrywa!
Nie mogę teraz przestać! Jeżeli Samira zobaczy, że może wykorzystać Katarzynę jako tarczę przede mną...
Uff...
Nie wytrzymała. Zaczęła niszczyć te lustra. Uff..

----
**Z myśli Samiry**

Urok! To lustro powinno...
Dobrze!
Nie! Moje lustro! To był cios fizyczny! Zniszczyłam moje lustro! Kupione trzy lata temu, najlepsze do sztuczki z szablami...
Czuję się... źle.
...
Urok... Nie odbijam.
Aaaa!
Jak upadek ze sceny...
//Znowu//!
//I znowu...//
//I znowu...//
...
Nie... zniszczę... swoich... kochanych... luster!
Kasiu, nie! Odsuń się!
Boże, nie! Nie Kasia!
...
Bądź przeklęty!
<odbija, niszcząc lustra>

----
**Z myśli Kasi**

Au! Czemu on to robi? Po co atakować Samirę? No dobra, dotknęła go srebrem, nawet ja nie wiedziałam, że nosi srebrne bransolety! Naprawdę jest aż tak małostkowy?
No dobra, ma magiczne lustra. Ale czy to naprawdę powód, żeby atakować zwykłego człowieka? Bo ma lustra?
Czemu?

----
----

18. Samira, nie mogąc znieść bombardowania, zaczęła odbijać ciosy, rozbijając swoje zwierciadła. Kasia wyciągnęła ją poza pomieszczenie z lustrami. Ataki ustały. Samira z przerażeniem zauważyła, że lustro odbijające jest pęknięte. Na pytanie Andromedy o co chodzi, odparła, że pęknięte lustro nie działa.
Andromeda zasugerowała, że może przynieść coś Samirze z jej mieszkania - w końcu ataki były wymierzone nie w nią a w Samirę.
Samira poprosiła o srebrny sztylet i piżamę.

19. Andromeda zabrała Samirę do siebie. Gdy weszły razem do środka, uruchomiła się pułapka.

----
**Z myśli Kasi**

Srebrny sztylet my ass!

----
----

20. Pole potężnego czaru mentalnego sprawiło, że dziewczyny zażyły podrzucone przez maga narkotyki. Następnie Samira, sterowana przez terminusa, zniszczyła obrazy Andromedy. Ze szczególnym okrucieństwem potraktowała nie skończony obraz malowany przez Andromedę właśnie dla niej. Ominęła jednak te obrazy, na których Andromedzie zależało najbardziej.

----
**Z myśli Kasi**

Zamorduję sukinkota! Zrobię mu z dupy jesień średniowiecza gówniarzowi jednemu! Jak on mógł?! Daruję zmianę moich obrazów w kicz, ale zmuszania artystki do niszczenia sztuki nie daruję!
Jego szczęście, że nie mogę się ruszyć.

----
----

21. Terminus wdziera się do umysłu Samiry i odkrywa, że jest ona tylko człowiekiem. Człowiek mu wklepał artefaktem, którego sam nie rozumie. Nie wiedząc nic o stanie swoich quarków, terminus realizuje swój oryginalny plan. Według niego obie dziewczyny pamiętają, że sobie popiły, Samira namówiła Kasię, by sobie poćpały, po czym doszło do strasznej kłótni, w wyniku której Samira zniszczyła Kasi obrazy, a Kasia potłukła Samirze lustra. Plan był dobry - dzięki temu mała jest szansa, by czarodziejka chciała napastować Kasię, bądź by Kasia chciała utrzymywać kontakty z czarodziejką. Jedyny problem w tym, że nie było czarodziejki...

----
**Z myśli Kasi chwilę przed wymazaniem pamięci**

O, nie chłoptasiu. Zapłacisz za to. Wszystko sobie przypomnę, a wtedy za to zapłacisz. Zabierasz mi przyjaciółkę? Chcesz nas rozdzielić? Zapomnij. Władza władzą, ale tego ci nie dam. Wszystko sobie przypomnę. I będziesz za to płacił. W ratach. Aż piekło wystygnie.

----
**Z myśli Samiry chwilę przed wymazaniem pamięci**

Nie! Tylko nie to! To niemożliwe! Ja taka nie jestem! Nie zgadzam się! Nie zapomnę! To nie może być prawda... Magia przecież nie istnieje! A jeśli istnieje, to przecież nie może być czymś tak złym... Tak bezrozumnie złym...

----
**Z myśli terminusa**

Ona jest człowiekiem. Nie jest magiem. Pokonał mnie człowiek!
Nie świadczy to o mnie najlepiej...
Tak, czy inaczej, jest niebezpieczna. Nie wiem, dlaczego tak zależało mi na ochronie pani Katarzyny, ale... by móc ją chronić, lepiej, by te dwie rozdzielić.
Mój plan jest wiarygodny, możliwy, a fakt, że mam pewne kryształy quark oznacza, że mam jakieś fundusze. Jeśli próbowałem jej kasować pamięć wcześniej, to z pewnością do tego one służyły.

----
----

22. Nieszczęśliwa Andromeda budzi się rano i przypomina sobie to, co wpisał jej terminus. Widząc ruinę obrazów, wypłakuje się w poduszkę. Później przypomina sobie prawdziwe wydarzenia i znów płacze bezsilnie. Potem zniszczone obrazy wynosi i pali.
Po powrocie do domu zaczyna szkic obrazu będącego alegorią wydarzeń poprzedniego dnia.
Rodzina zajęcy w swojej norce, przerażona, bo pana domu właśnie porywa ręka myśliwego. Myśliwy, sam przerażony, ściska nóż aż bieleją mu kostki.

Wiedziona impulsem, rozpoczyna również wstępny szkic do obrazu dla grabarza.

23. Samira wcale nie była w lepszym stanie niż Andromeda. Nawet w gorszym. W końcu to ona zniszczyła te obrazy. Terminus także, gdy minęła godzina zero, przypomniał sobie, kim jest, na czym mu zależało, co chciał zrobić, a co zrobił i był w stanie nie lepszym niż Samira czy Andromeda.

24. Wieczorem terminus pojawia się w mieszkaniu Andromedy. Ta nie kryje się ze swoim smutkiem.
Kiedy mag wyskakuje z typowym tekstem "na świadka Jehowy", Andromeda mówi mu, że nie ma pojęcia, co to smutek, rozczarowanie, żal, poczucie straty...
Szkice stoją na widoku - Andromeda specjalnie ustawiła je tak, by musiał je zauważyć.
Terminusowi wyraźnie było przykro. Wyraźnie nie wiedział, jak się zachować. Po chwili zastanowienia rzucił zaklęcie na Kasię. Zaklęcie to trochę zmieniało sytuację. Owszem, fakty pozostały faktami. Obrazy były zniszczone, a lustra potłuczone. Jednak to zaklęcie miało na celu przypominać Kasi te dobre rzeczy o Samirze. Ich miłe, wspólnie spędzone chwile. Zaklęcie to miało też umniejszać znaczenie tej kłótni - były po alkoholu i innych środkach. Normalnie by się tak nie zachowywały.

----
**Z myśli Kasi**

Po raz pierwszy w życiu potrafię powiedzieć, co on zrobił swoją magią, a co pamiętam. Pewnie dlatego, że jedno z drugim nic a nic nie pasuje. I choć jestem pewna, że to co mi wpisał, jest prawdziwe, wiem też, że nie jest... Dziwne uczucie.
Może ten chłopak nie jest taki do końca zły? Sprawiał wrażenie, że naprawdę się przejął...
Niemniej jednak, to go nic a nic nie usprawiedliwia. Było milion sposobów rozwiązania tego inaczej od samego początku.
Ale może... tylko może, będzie z niego coś więcej niż potwór, jakich wśród magów wielu...

----
**Z myśli terminusa**

Mam nadzieję, że tak choć trochę naprawię zło, które wyrządziłem.
Wiem, mistrz powiedziałby, że to tylko ludzie. I lepiej, by dwie tak specyficzne osoby nie znały się za dobrze. Ale one cierpią. Nie po to zostałem terminusem... Ech... Gdy tien Marcel raportował mistrzowi, że uniemożliwił dwóm ludzkim parom być razem by doprowadzić do jakichś tam wyników finansowych... jakaś tam prosta sprawa... To burzyłem się, że się bawi w Boga. A ja w sumie zrobiłem to samo...
Może... może jeżeli użyję trochę mniejszej, bardziej subtelnej magii, to może to wszystko się samo poukłada. Może o to chodziło tien Marcelowi... Bo mistrz to w ogóle nie chce ze mną rozmawiać.

----
----

25. Andromeda dochodzi do wniosku, że pojednanie z Samirą musi poczekać. Samira w końcu nie pamięta prawdy, a Andromeda nie odważy się jej o tym opowiedzieć...
W międzyczasie Andromeda postanawia pójść za tropem dziwnych wydarzeń w miasteczku... Może w ten sposób zmusi maga do zajęcia się sprawą...

----
**Z myśli Kasi**

...Jak to możliwe, że takie dziecko ma taką moc? To... straszne. Taka władza w tak młodym wieku jest bardzo niebezpieczna, jeśli nie ma kogoś, kto wpoi młodemu człowiekowi kompas moralny. A on... Wiem, jak magowie podchodzą do ludzi. Wiem, co o nas sądzą. Ten młody mag jest na najlepszej drodze do stania się potworem... Choć jest jeszcze dla niego nadzieja...
...
Taka moc...

----
**Z myśli Kasi**

Zastanawiałam się nad tym portretem psa...
Niech tam, namaluję. Ale facet się wykosztuje.

----
**Kilka różnych wpisów do 'pamiętnika dla terminusa'**

Kochany pamiętniczku, w tym mieście dzieje się coś dziwnego.
Ludzie chcą dokładnego, niemal fotograficznego odwzorowywania zmarłych osób. Nikt nie zgadza się, by pomnik, obraz czy jakiekolwiek przedstawienie zmarłego rok temu księdza nosiło symbol krzyża, grabarz prosi o namalowanie dla niego okultystycznych symboli ochronnych...
O co tu chodzi?

Kochany pamiętniczku.
Dziś znów czułam się tak dziwnie... Zwykle, kiedy tak dziwnie się czuję, znaczy to, że stało się coś złego. I tym razem tak też było... Samira... Co się stało? Czemu nam obu tak odbiło? Mam do niej żal... I do siebie. Zupełnie  nie rozumiem, co się stało i jak do tego doszło...



{{out of game: zabawne, że jakiś losowy Marcel ma imię, a nasz terminus jeszcze się go nie dorobił}}
{{out of game: Żółw: zobaczysz, nadamy imię panu od psa, zanim nadamy je terminusowi}}


+ Dramatis personae:

- czł: Andromeda (Kasia Nowak) jako wybitna malarka z unikalną pamięcią,  przerażona mocą dostępną magom

- czł: Samira Diakon jako wybitna iluzjonistka i NIE czarodziejka
- mag: August Bankierz jako bezimienny młody terminus pilnujący Andromedy, który bawi się w Boga