+ Konspekt pisany:
+++ Co się stało:

Grabarz zmartwiony stanem córki, zwłaszcza po jej poparzeniu srebrem przez biedną Kasię, szukał rytuałów, środków... czegokolwiek. Natrafił w końcu na maga który zdecydował się coś zrobić ze stanem jego córki. Chciał jej pomóc, jednak nie wiedział co jej jest i źle zdiagnozował swoją pacjentkę. Skupił się na Aquam i Alucinor a problemem było przede wszystkim Spiritus.

Jak mu nie wychodziło, zwiększył Vim by mieć katalizator dla Alucinor/Aquam i by ją uratować.

Niestety, córka grabarza została skażona.

Dziewczyna została przekształcona. Jej fizyczne rany od srebra zostały "schowane" pod maską iluzji (phantom of the opera). Jednocześnie zapętlił się jej system - nie została wylizana przez języcznik, nikt nie usunął srebra z jej ciała więc auto-działająca magia jedynie rozjątrzała rany od srebra. Jednocześnie nie zaleczone rany Spiritus w połączeniu z koszmarnym bólem fizycznym i magią mającą na celu zamaskować ból w układzie nerwowym doprowadziły do skażenia mentalnego nieszczęsnej dziewczyny i kolejnym uszkodzeniem jej Spiritus.

Powstało coś na kształt banshee.

Pierwszą, "automatyczną" ofiarą dziewczyny stał się jej własny ojciec. Wykorzystany jako katalizator przez maga-konowała, fala uderzeniowa powrotna uderzyła w niego jako źródło energii dla banshee. Wyssany z energii, pozbawiony częściowo "duszy", padł apatycznie. Dziewczyna próbuje go reanimować, pomóc mu... wie, że to ten mag jej to zrobił. Przekazując jemu energię, osłabia siebie i wzmacnia ból który czuje przez srebro, więc musi znaleźć źródło zewnętrzne...

Tymczasem mag, przerażony tym co się stało i potencjalnym złamaniem Maskarady przygotował zabójcę mającego na celu zabić córkę grabarza. Przygotował kota z ogonem skorpiona - najszybsze, co potrafił skonstruować.

A terminus? Cóż, został wycofany przed czasem. Nie zdążył obejrzeć innych ofiar mandragory, wezwał go jego mistrz "natychmiast" (patrz: wpis pod koniec konspektu).

+++ Misja właściwa:

01. Kasia i Samira przejechały się do mieściny Otrąb. Samira mówiła, że jest tam kolekcjoner osobliwości, który nie rozmawia z ludźmi z którymi nie chce rozmawiać, ale jej sprzedał kilka luster (więc może ma kilka nowych). Niestety, kolekcjoner nie chciał z nimi tym razem rozmawiać (nie otworzył), więc po nocy spędzonej u innych znajomych Samiry dziewczyny wróciły do domu.

02. Przed południem do Kasi podskoczył urwis (ten wynajęty kiedyś przez Samirę by zbierać wieści) i opowiedział o dalszych dziwnych wydarzeniach. Podobno lekarz, który dzień wcześniej szedł na cmentarz wpadł w apatię - "wszystko mu jedno" - leży taki bezwolny, wykonuje proste polecenia, nic się nie dzieje. Dodatkowo mała Sara (dziewczynka która "znalazła nazistowski skarb" - czyli lustra ukryte przez terminusa wcześniej) widziała w okolicach cmentarza dziwnego kota, jak to powiedziała, "wężokota". Namalowała go jako kota z wężowym ogonem, zakończonym wężową głową. Kasia poprosiła urwisa o spotkanie z małą. Umówił je w parku.

Tam Kasia dowiedziała się czegoś ważnego - mała skłamała. Kot miał ogon skorpiona, nie ogon jak wąż, ale je nikt nie uwierzył, więc powiedziała coś innego. Ale wszystko wskazuje na to, że kot ma własności kameleona.

Wspólnie z dziewczynką Kasia naszkicowała, jak skorpiowężokot wygląda naprawdę.

03. No dobra. Może zaczyna się tu dziać coś bardzo dziwnego. W związku z tym Kasia zdecydowała, że pójdzie na targ, dowiedzieć się więcej i zdobyć jakieś informacje od ludzi (innymi słowy, poplotkować). Tam zebrała kilka ciekawych informacji: przybył lekarz obejrzeć "ich" lekarza, ale na razie on nie ma pojęcia co to jest. Nic ciekawego nie zrobił. Poza tym, że sam się wybrał na cmentarz (i wrócił bezpiecznie). Oraz fakt, że matka samobójcy (z pierwszej misji tymi postaciami, ta, która wyhodowała mandragorę) opuściła szpital psychiatryczny i wróciła do domu. Była rozbita i miała roztrzaskaną duszę (Spiritus), ale "nagle" jej się poprawiło. Podobno to ona rzuciła klątwę na lekarza (choć nie wiadomo po co - ale złe wiedźmy robią takie rzeczy).

Kasia zaobserwowała jeszcze dwie rzeczy. Po pierwsze, zobaczyła tajemniczego kota o dziwnym, "wężowym" pyszczku na jednym z budynków. Jednak miał taki kolor, jak sam budynek (co potwierdza tezę o kameleonie). Wszystko wskazywało na to, że tylko Kasia dostrzegła kota a kot obserwował okolicę (dokładniej to patrzył na Banshee, planował ewentualny atak).

Drugą rzeczą, jaką zauważyła było to, że córka grabarza była na targu. Wyglądała dobrze - ani śladu oparzeń od srebra (z czego Kasia wywnioskowała, że nie trafiła jej wtedy na cmentarzu). Kupowała poza normalnymi rzeczami (Kasia ją podśledziła) też leki - przede wszystkim od oparzeń... lub odleżyn.

Wizyta na targu zakończyła się tym, że gruchnęła nowina. "Nowy" lekarz pojechał do księdza (tego starego, sympatycznego jegomościa). Podobno księdzu się coś stało i natychmiast jedzie do szpitala.

04. Kasi ten ksiądz zupełnie nie pasuje do wzoru. Lekarz miał coś wspólnego z mandragorą (leczył burmistrza), ale ten ksiądz? Kto chciałby unieszkodliwiać tego sympatycznego, starszego, całkowicie nieszkodliwego człowieka?
Zwierzyła się Samirze z tego, że nie rozumie o co chodzi. Ta postanowiła dowiedzieć się więcej o okolicznościach choroby księdza.

Tymczasem Kasia skończyła przygotowywany dla grabarza glif ochronny (misja 1 lub 2) i zdecydowała się skorzystać z okazji i go odwiedzić. Kątem oka, idąc do niego do domu dostrzegła owego tajemniczego kota. Zanim weszła do mieszkania grabarza, drzwi się same otworzyły i wyszli - burmistrz i "nowy" lekarz.

Lekarz bardzo chciał odwieść Kasię od wejścia do środka. Okazało się, że grabarz też leży - zapadł na podobną, ale nie mniej tajemniczą chorobę - zupełnie nie ma sił na nic, choć w nim tli się wola. Lekarz powiedział, że trzeba zaprowadzić kwarantannę, na co burmistrz zupełnie spanikował. Gdy tylko Kasia inteligentnie zauważyła, że przecież on (burmistrz) wyraźnie się z tego (mieszając specjalnie atak mandragory z tym co się dzieje teraz) wyleczył, lekarz się zasępił. Chciał z Kasią porozmawiać później.

Usłyszała też, że ten stary ksiądz umarł. Zapytany na co, "nowy" lekarz odparł sarkastycznie "atak serca".

Kasia odczekała, aż się oddalą i poszła do domu grabarza.

05. Tam spotkała córkę i matkę (która jednak szybko się oddaliła). Ojciec, ledwo świadomy, podziękował za glif ochronny. Z trudem wyszeptał, że nie zdążył mu pomóc, ale przynajmniej jego córka jest bezpieczna. Dziewczyna jest zrozpaczona - "wejdę w konszachty z samym diabłem" by tylko uratować ojca. Kasia, zaniepokojona tą sprawą, spytała grabarza czy jest wierzący. Jest. Ma na szyi krzyżyk z drewna sandałowego. Dziewczyna jest ateistką. Kasia zostawiła na szafce srebrny krzyżyk (jako narzędzie kontrolne).

Podczas rozmowy Kasia dostrzegła, że za oknem czai się wężokot.

Też podczas rozmowy jej poczucie zagrożenia zaczęło coraz bardziej rosnąć. Czuła, że jeśli zostanie dłużej, może grozić jej coś strasznego. Zdecydowała (już ciemno), że jednak czas wracać do domu...

06. Chciała, nie chciała, koło cmentarza przejść musiała. Słyszała stamtąd okrzyki triumfalne, krzyki radości. Kasia nie miała wyboru (swoim zdaniem) - musiała przejść i zobaczyć, o co chodzi. W końcu już ktoś umarł a ona jakby co ma gorącą linię do terminusa...

Podkradła się do cmentarza, z ciągle narastającym poziomem zagrożenia i zobaczyła, jak matka samobójcy (misja 1) stoi na cmentarzu i krzyczy do "swojego syna" (znaczy, w powietrze). Woła do niego - "wróciłeś", "chodź do mnie", "kocham Cię" i tym podobne. Woła o zemście i powrocie. Kasię coś tknęło i spojrzała do góry - dostrzegła, że na jednej z gałęzi znajduje się wężokot i patrzy uważnie na całą scenę, obserwując zarówno matkę jak i Kasię. Kasia nie patrzyła mu w oczy, ale patrzyła min. na niego. Kot zorientował się, że Kasia go widzi i że go widziała...

W tym momencie rozległ się wrzask Banshee - jak na mandragorę, bardzo słaby, lecz po tych samych kanałach które rozogniła w Kasi mandragora. Dla matki to była cudowna melodia, melodia, którą ona pragnęła usłyszeć. Dla Kasi to był koszmar, przypomnienie koszmaru. Zwinęła się, a gdy spojrzała do góry, kota już nie było...

Kasia zaczęła się wycofywać. Ostrożnie, potem prawie biegiem, z kijem w ręku. Uciec przed tym wszystkim, przed tym chorym kotem, uciec do domu.

07. Wracając do domu poczucie zagrożenia narastało coraz mocniej. Coś zaszeleściło w krzakach. Nagle, dookoła niej zrobiło się czarno, amulet poparzył ją w dekolt (lol :P) i Kasia została gate'owana do domu. Bez ubrania, srebra i czegokolwiek. De facto, wypadła z lustra-portalu. Można powiedzieć, że wypadła wprost na bardzo zdziwioną Samirę. Samira prawie zemdlała - przejście przez portal (Kasi) odbyło się kosztem energii Samiry. Kasia ją zbajerowała, że zapomniała, że w mieszkaniu jest ktoś obcy i zapomniała ręcznika wychodząc spod prysznica. Samira była w takim stanie, że w to uwierzyła.
Tymczasem kot-zabójca stwierdził, że nie musi dalej obserwować Kasi i wrócił do swojego głównego zadania.

08. W nocy miało miejsce kilka ważnych wydarzeń. Po pierwsze, kot złapał lekarza (nowego) jak ten gdzieś szedł i go użądlił. Lekarz ciężko zachorował (temperatura, maligna...) Już głębszą nocą banshee (także chcąc zapobiec kwarantannie) zdecydowała się oddać część energii osobom, które jej potrzebowały. Odwiedziła w nocy lekarza (starego), wyssała z niego jeszcze trochę energii "by wyczyścić ranę", po czym wypełniła go częścią przetworzonej przez siebie, skażonej energii. Technicznie rzecz biorąc, zrobiła z niego swojego niewolnika. Odwiedziła też matkę samobójcy i nadała jej cel. Wstrzyknęła w nią swoją skażoną energię oraz solidnie się nią pożywiła. Gdy wracała do domu, dopadł ją kot i ją także użądlił. Dedykowana, niszczycielska trucizna weszła do jej krwiobiegu (użądlił ją w nogę) Jednak skażenie magiczne i próg energii sprawiły, że trucizna zamiast ją zniszczyć powodowała tylko "wyciekanie" energii (co wymaga bardzo częstego pożywiania się) Dziewczyna, jeszcze tego nieświadoma, zignorowała atak kota i wróciła do domu, by napełnić ojca energią. Jego jednak napełniła energią czystszą.
Kot wykonał swoje zadanie, więc wrócił do właściciela.

Całą tą noc czuła Kasia. Transfery energetyczne odbywały się na tych samych kanałach, co ataki mandragory, więc Kasia czuła odległe echa tego wszystkiego. Nie wyszła z domu nocą. Zabunkrowała się za srebrnymi łańcuszkami i na szybko stworzonymi runami ochronnymi. O dziwo, pomogło, choć rano obudziła się zmęczona.

09. Rano Kasia sprawdziła ślady na parapecie - nie było żadnych (rozsypała mąkę wieczorem, by się przekonać, czy nie odwiedzi jej kot). Odwiedził ją przed południem grabarz, co potężnie ją zdziwiło - poprzedniego wieczora widziała go jak leżał bez sił.
Poprosił ją o coś, co pozwoliłoby mu zabezpieczyć rodzinę. Poprzedniego wieczora Kasia zostawiła u niego na ścianie srebrny krzyżyk - grabarz był osobą wierzącą, a Kasia uznała, że srebrny symbol wiary może pomoże, jeśli działa tu magia...
Problem w tym, że kiedy rano chciał go wziąć do ręki, poparzył się. Dlatego też uznał, że stał się wilkołakiem. Kasia nie zdążyła z nim dobrze porozmawiać, bo wpadła jego córka. Jak on mógł je zostawić nic nie mówiąc, jak poprzedniej nocy jeszcze leżał prawie nieprzytomny? Kasia zauważyła, że dziewczyna utyka na lewą nogę.
Chcąc dowiedzieć się czegoś więcej (i oszczędzić grabarzowi dalszego besztania), zaoferowała się, że oboje podwiezie do ich domu.

10. Tymczasem w miasteczku pojawił się nie lada problem. Stary lekarz zbadał nowego i stwierdził, że ukąsiła go żmija. Nic mu  nie będzie, po prostu chwilowo potrzebuje spokoju i antidotum. Stary lekarz w magiczny sposób ozdrowiał, grabarz ozdrowiał, mąciciel (nowy lekarz) jest chory... Jedyna nieoczekiwana sytuacja to śmierć księdza. Ale w końcu był stary. Nie ma potrzeby zaprowadzać żadnej kwarantanny.
Tymczasem Kasia odwiozła grabarza z córką do domu. Tam skorzystała z okazji, by porozmawiać z nią sam na sam. Dziewczyna była nieszczęśliwa z jakiegoś powodu, ale zdeterminowana. Powiedziała, że Kasia nie może w żaden sposób jej pomóc. Po sinlych naleganiach Kasi przyznała się, że faktycznie coś jej się stało w nogę. Dokładniej, powiedziała nawet Kasi o kocie.  Kasia zobaczyła ranę - porządne, głębokie ukłucie. A Kasia wie z innych źródeł, że majaczenia nowego lekarza mówią o czymś, co ma oczy sowy i ogólnie wskazuje na to, że gość mógł widzieć kota.
Sprawia zaczęła robić się mniej ciekawa.
W trakcie oględzin rany, Kasia dyskretnie, niby przypadkiem zbliżyła srebrny medalionik do nogi dziewczyny - ta, nawet go nie widząc, odruchowo się odsunęła.
Córka grabarza wyciągnęła od Kasi informację, że ta mieszka sama i ogólnie jest sama.

11. Kasia spotkała się z Samirą, która miała ciekawe informacje na temat księdza. Okazało się, że ksiądz przyjął kogoś do spowiedzi. Gospodyni jej to powiedziała. Podczas spowiedzi doszło do krzyków. Gospodyni rozpoznała jeden z krzyków jako "gdzie jest twój Bóg?", po czym rozrywający, zimny okrzyk ją obezwładnił i odebrał jej przytomność. Gdy się ocknęła, ksiądz już nie żył. Nie powiedziała tego policji ani lekarzowi, bo po pierwsze, takie rzeczy się nie zdarzają, po drugie, naoglądała się horrorów. Wie tylko, że przybyszem była kobieta.

12. W tym momencie Kasia postanowiła zadzwonić do terminusa. Sytuacja zaczęła wymykać się spod kontroli; najwyraźniej pojawił się ktoś, kto posiadał właściwości mandragory... Trochę tego za dużo się zrobiło na jadną Kasię, która wcale nie chciała mieć za dużo do czynienia z magią.
Na szczęście, terminus odebrał. Gdy usłyszał wieści, prawie spanikował. Ostrzegł Kasię, że w grę musi być wmieszany inny mag. Sama na to wpadła. Zadał jej kilka rzeczowych pytań i wywnioskował, że banshee musi być jedna z następujących osób: matka samobójcy, córka grabarza lub grabarz. Powód: tylko oni byli na tyle mocno skażeni i mieli na tyle dużo kontaktu z mandragorą, że mogło dojść do strzaskania spiritus. Terminus przygotował sprzęt i ekwipunek dla Kasi i powiedział jej, że wieczorem zawiezie go tam "niezaradny, ale bardzo wzruszający mag o dobrym sercu". Zdradził jej też lokalizację luster i ostrzegł, żeby ich nie używała pod żadnym pozorem... chyba, że nie ma wyjścia.
Terminus powiedział Kasi jeszcze jedną rzecz: Samira była kiedyś czarodziejką. Reakcja Kasi zdecydowanie go zaskoczyła "spoko, zamierzam się trzymać blisko niej". Zupełnie nie o to mu chodziło... Nie mógł jednak zaprzeczyć logice argumentu, że nie bardzo można zostawić Samirę, która co i rusz znajduje dziwne lustra zupełnie bez opieki i kontroli. A Kasia... ma swoje powody, żeby trzymać się blisko Samiry.

Kasia, jako, że było późno, musiała poczekać - nie chciała poruszać się po nocy. Rozłożyła srebrne łańcuszki jako ochronę i poszła spać.

13. W środku nocy Kasię obudził przerażający sen. Otworzyła oczy i w lustrze zobaczyła swoją twarz, ale poprzecinaną dziwnymi nitkami światła, z obcymi oczami i dziwnie przerażającym obliczem. Błyskawicznie ocknęła się, kiedy uderzył w nią zastrzyk adrenaliny. Ku swojemu wielkiemu wstrząsowi zorientowała się, że stoi przy drzwiach wejściowych, w prawej, uszkodzonej, ręce trzyma srebrny łańcuszek powodujący tak wielki ból, że prawie przestała go rejestrować. Jednocześnie na pewnym poziomie "słyszała" pieśń syreny, skłaniającą ją do otwarcia drzwi i wpuszczenia tego, co znajdowało się po drugiej stronie. Kasia nie wiedziała co robić.

Przestraszona, odsunęła się od wejścia, ale wiedziała, że to coś za drzwiami wkrótce się zorientuje, że ofiara nie do końca się poddała.
W stresie, Kasia przypomniała sobie epizod z portalem i nagle zapragnęła, żeby Samira była z nią. Świadomie przyzwała Samirę przez portal, by mieć katalizator dla swojej mocy (nie do końca świadoma tego, co robi, ale działając jak najbardziej prawidłowo i zgodnie ze swoją wolą). Następnie przyzwała lustro - broń, lustro, którego nie chciała wcześniej... Wyciągnęła je z lustra-portalu (doprowadzając MG do bólu głowy), po czym obróciła pełnię mocy broni przeciwko banshee.

Banshee wydała agonalny wrzask, budząc wszystkich mieszkańców (a emisja magii powodująca ogromny huk też nie pomogła). Kiedy tylko otrząsnęła się z szoku, Kasia wciągnęła nieszczęsne stworzenie do mieszkania i zamknęła drzwi.
Ku swojemu zaskoczeniu, ujrzała córkę grabarza. Gdy magia odeszła, trucizna skorpiokota zaczęła działać błyskawicznie. Kasia założyła natychmiast opaskę uciskową na nogę (widząc, że jej stopa zaczyna się kruszyć), lecz nie było to w stanie powstrzymać magicznej trucizny. W panice zadzwoniła do terminusa (trzecia rano!), ale on też nie wiedział, co z tym zrobić i jak uratować dziewczynę. Kasia mogła jedynie wydobyć ostatnie zeznanie od ofiary. I towarzyszyć dziewczynie w ostatnich chwilach jej życia.

Kasia dowiedziała się, jak to wyglądało. Dziewczyna pamięta tyle, że po silnym poparzeniu srebrem na cmentarzu i rozerwaniu spiritus ojciec znalazł maga, który zdecydował się jej pomóc. Niestety, jego pomoc wprowadziła ją w straszny głód, straszne cierpienie i pętlę, przez którą ona zabijała swojego ojca samym faktem swojego istnienia. Zdecydowała się "wejść w pakt z diabłem", żeby tylko go uratować. Nie chciała zabijać księdza, nie wiedziała, jaką ma moc. Szczerze tego żałuje. Przeprosiła Kasię, że ją zaatakowała, ale uznała, że po niej nikt nie będzie płakał, bo jest samotna, a przez tą truciznę, gdyby nie zaatakowała, nie doczekałaby rana. Dziewczyna nie chciała umierać, ale pogodziła się z tym, że wyrządziła tak wiele zła, więc nadszedł jej czas. Na oczach Kasi rozsypała się w proch.

Terminus wszystko słyszał - Kasia nie zerwała połączenia. Poprosił Kasię, by schowała trochę prochu do słoika - będzie mu potrzebny do znalezienia mordercy. Kasia spełniła tą prośbę. Została sama z naprawdę czarnymi myślami i Samirą, która wszystko przespała (bardzo wyssana z energii przez lustra).

Kasia nie poszła już spać tej nocy. Postanowiła pojechać do skrytki i przywieźć rzeczy terminusa. Ręka znów nieziemsko ją bolała, więc bardzo chciała czym prędzej dorwać języcznik...

=====================================

----
**Rozmowa Augusta z Marcelem, w dworku mistrza**

- Jak to: mam szlaban?!
- No, mistrz wyraził się dość jasno.
- Czy ja jestem dzieckiem, żeby mieć //szlaban//?
- Zdaniem mistrza, tak.
- To jest...
- Upokarzające? Wiem. Nie trzeba było zachowywać się jak szczeniak przy kontakcie przez lustro.
- Przez lustro?!
- Nie udawaj głupca. Wiesz, że potrafię zbadać odchylenia taumaturgiczne. Było coś nie tak w tym kontakcie, więc przyjrzałem się temu dokładniej. Wykorzystałeś nieznane sobie lustro do komunikacji! Lustro! Ze wszystkich artefaktów!
- Marcel, jeśli jesteś taki mądry, czemu mistrz odwołał mnie dopiero parę dni po tym kontakcie?
- Bo nie powiedziałem mu wcześniej.
- Bo nie pomyślałeś, by to zbadać wcześniej.
- Nie. Sprawdziłem wszystko. Od razu.
- To nieodpowiedzialne. Mistrz mówił...
- Wiesz co, należała ci się nauczka. Ale nie aż taka. Wolałem, by mistrz ochłonął i nie spanikował.
- Spanikował?
- Mistrz się o ciebie martwi, ty idioto! Nie masz ani intelektu, ani pomysłowości, ani specjalizacji, ani opiekunów, ani kolegów, ani dziewczyny... Nawet niewolnicy nie masz.
- Może z wyboru..?
- Raczej z ubóstwa i niekompetencji. Zrozum, mistrz dał ci szlaban. Zażyczył sobie czegoś jeszcze. Otworzył ci dostęp do swojej biblioteczki. - W głosie Marcela pojawiła się zawiść.
- Serio?!
- Tak. Masz tu siedzieć tak długo, aż znajdziesz sobie czarodzieja w "Żywotach Srebrnej Świecy", który będzie twoim modelowym czarodziejem.
- ...Ech... Przynajmniej...
- Aha, dopóki tego nie zrobisz, nie masz dostępu do żadnego innego skrzydła biblioteki - Marcel uśmiechnął się złośliwie.
- Żadnego?! Przecież to...
- To nic nie zmienia wobec tego, co miałeś do tej pory. Ale dopóki tego nie zrobisz, masz szlaban. Jak dziecko.
- Ech...

----
**Z myśli Augusta, po rozmowie z mistrzem**

Wybłagałem złagodzenie kary. Mistrz mi umożliwił dostęp do większości podstawowych ksiąg i do bardziej zaawansowanych dzieł o mandragorach. Oczywiście, zmył mi głowę - nie powinienem był porywać się na nieznaną mandragorę bez wsparcia. Ech, gdyby on wiedział, w jakiej sytuacji się znalazłem...
Za to mam czterdzieści tomów interaktywnych encyklopedii o życiu i działaniach magów Srebrnej Świecy, ze szczególnym aneksem dotyczącym Bankierzy.
...
Ciekawe, czy coś pisze o moich rodzicach... I w sumie o mnie.

----
**Z myśli oburzonego Augusta po przeczytaniu wpisów**

Jakieś bzdury. Moja matka nie zasłużyła na taki wpis. Może i nie jest najmądrzejszą czy najpotężniejszą czarodziejką, ale można na niej polegać i nigdy nikogo nie zawiodła. Ponadto, ani ja, ani ona nie mamy "ludziofilskich tendencji". To, że my traktujemy naszą służbę z szacunkiem (oczywiście, na tyle, na ile można szanować ludzi) nie oznacza, że od razu mamy być "ludziofilami".
Jak ta marna gałąź reformatorów pod wodzą Pączka.
Nie wierzę w to, że ona ma cokolwiek wspólnego z Czaplińskim...
Spytam ją. Jak tylko będę mógł się z nią spotkać. Gdzieś w encyklopedii jest błąd.

A o mnie... "młody i niedoświadczony terminus".
Tyle.
Poza drzewem genealogicznym.

Czyli zdaniem twórców encyklopedii jestem nieważny.
Przynajmniej ojcu oddali należne mu honory i cześć.

----
**Z myśli Augusta, parę godzin później**

Z tego, co widzę, nawet losy promientów (Jego Jaśnie Wielmożna Ekscelencja Lord Andrzej Sowiński) są opisane szczerze. Bez koloryzowania. Zdecydowanie poprawił mi humor fakt, że taki Marcel ma co prawda siedem linijek, ale trzy z nich opisują wielką porażkę. Może i jestem bezbarwny, ale nie jestem głupi. A on... coś takiego dla dziewczyny?
Co to, nie wie, jak zrobić eliksir usuwający miłość czy co? Co za dureń... Prawdziwy terminus musi być bezwzględny, skuteczny i niezawodny.
I samotny, by nie skrzywdzić swoich bliskich.
Głupi Marcel.

Właśnie... niepokojące są te wszystkie lustra, które non stop kręcą się dookoła Samiry. Muszę poszukać jakiegoś eksperta od luster w tych "Żywotach". Muszę się go poradzić - jak prawdopodobne jest, by jedna osoba, nie czarodziejka, taka jak Samira, zgromadziła tak wiele luster?
O co tu chodzi?

----
**Z myśli Andromedy**

I poszedł...
Znaczy... Poczekał, aż Samira pójdzie do siebie i użył artefaktu.
Dzieciak... taki młody i taki potężny...
I taki arogancki. A jednak... Zdążyłam go trochę polubić. On nie jest zły. Po prostu nie wie, że można inaczej.
Świat magów musi być smutny. Skoro ludzie są tak pogardzani...
A przecież jego ojciec utracił moc. On nie wie, że ja wiem... Nie powiedziałam mu, o czym majaczył...

Przed odejściem jeszcze opatrzył mi dłoń. Już prawie nie boli, choć trochę mi się nie podoba, jak wygląda... Będę musiała ją rozruszać... To nie będzie przyjemne... Ale to nic. Wygoi się i będzie dobrze. Musi być.

Dzieciak... może i terminus, ale dzieciak. A temu jego mistrzowi należałoby dać ochrzan i szlaban na trzy miesiące. Jak mógł puścić dzieciaka samopas?
Nie narzekam, akurat mi to bardzo pasuje, ale... Żal go. Chłopak ma potencjał na bycie bardzo konkretnym facetem... Tylko jeśli wszyscy traktują go jak ostatniego gówniarza, to jak ma się rozwijać?

No nic... mam nadzieję, że nie wpadnie w zbyt wielkie kłopoty...

Swoją drogą... chłopak myśli, że zabrał wszystkie lustra. Myli się. Udało mi się jedno zachować. On i tak ich nie przeliczył. Nie miał kiedy. A ja... Postanowiłam zachować jedno z nich. To... zielone... pękło. Rama go nie interesowała, powiedział, że można oddać je Samirze. Kazałam mu je zabrać.

Ale zachowałam sobie jedno z pozostałej dwójki. Nie broń... Tego lustra za bardzo się boję. Nie rozumiem go i nie chcę go rozumieć. Zresztą, byłoby dla mnie zbyt niebezpieczne. Nie, zachowałam sobie portal. Nie mam pojęcia, jak go używać i nie zamierzam go używać, ale... z jakiegoś powodu chciałam je mieć.
Schowam je gdzieś...
A nawet, gdyby napatoczył się mag... Artefakty czasami się zdarzają u ludzi, chłopak sam to powiedział...
Pod warunkiem, że jest to jedno lustro, a nie pięćdziesiąt. Tak czy owak, portal powinien być bezpieczny.

Ciekawe, co sobie chłopak pomyśli jak zajrzy do plecaka... Dostał małą apteczkę (z instrukcją obsługi) i lornetkę... po co mi dwie? No i kilka gier... Logicznych i zręcznościowych. Ciekawe, czy uda mu się rozłożyć i złożyć ponownie tą kostkę...

----
**Coś, czego nikt nigdy nie usłyszał**

Co za szczęście... Gdyby pojawił się tu jakikolwiek terminus w czasie tej akcji z lustrem wszystko by się rozsypało. Plan, tak pieczołowicie układany, prawie całkowicie został zniszczony. Przez jednego, młodego terminusa.
Naprawdę, niewiele brakowało...

----
**Z myśli Augusta**

A niech mnie... Praktycznie nie ma magów zajmujących się lustrami? Bardzo mało jest aktywnych magów.
"Nie żyje". "Nie żyje". "Pożarty przez lustro". "Nie żyje". "Utracił moc". "Wchłonął węzeł, stan nieznany"...
Jest czternastu aktywnych magów zajmujących się lustrami w Srebrnej Świecy, a kilkudziesięciu jest martwych lub... unieszkodliwionych?
To skąd te wszystkie lustra? Magowie spoza Świecy? Część z tych luster wyglądała na stare... Ale tu jest kilku magów, którzy skupiali się na wyszukiwaniu luster... Czemu więc nie odkryli Samiry?
Czyżby jedno z tych luster maskowało sygnaturę pozostałych? To jedyne logiczne wyjście. Ale które? To, które istnieje, czy to, które jest zniszczone? Kilka się potłukło.
...Muszę poszukać, może któryś ze zmarłych magów miał manufakturę luster? To nie ma sensu...

----
**Z myśli wstrząśniętego Augusta czytającego "Żywoty"**

Samira? Samira //Diakon//?
Aktualnie trzydzieści jeden lat. Utraciła moc podczas Zaćmienia. //Czarodziejka luster//...
Na pewno straciła moc. Dotykała srebra! Wniknąłem jej na samo dno pamięci...
Ale skąd lustra? Czemu lustra ciągle... jak to brzmi... Czemu lustra ciągle kręcą się dookoła niej..?
Myślałem, że znjadę osobę, która dostarcza jej lustra, a znalazłem coś zupełnie innego...
Z domu Wyszocek, wyszła za Diakona. Cieszyła się pewnym szacunkiem z uwagi na pewne swoje innowacje w procesie przyspieszenia kształcenia ludzi i lepszego modelowania ich psychiki... Miała pewne wykształcenie teoretyczne w tym kierunku.
Ogólnie mało lubiana, nie zintegrowała się korzystnie z wielkimi rodami Świecy... Stosunek do ludzi pragmatyczny. Czyli ani okrutna ani dbająca.
Jej mąż żyje do dziś, jak przyjęte, całkowicie zerwał kontakty z Samirą.

O! Ich małżeństwo było z miłości? To nie było małżeństwo polityczne? Z miłości? Na tak wysokim poziomie?
Myślałem, że takie nieszczęścia zdarzają się tylko w takich rodzinach jak moja...


----
**Z pamiętnika pewnej czarodziejki**

No nie. "Rufus, zanieś". "Rufus, przynieś". "Rufus, nie mów nikomu..." Rufus to nie jest jakiś pies! Co ten August sobie wyobraża? Że cokolwiek nie powie, to zaraz Rufus ma to zrobić? I jeszcze ta jego mania wielkości! Wszystko w największej tajemnicy... Jakby //on// miał jakiekolwiek sensowne sekrety...
//Jak ten mag mnie czasami irytuje...//


----
**Z myśli Augusta po zniszczeniu banshee**

Biedna... Nie przywykła do takich rzeczy. Nie to, co my, terminusi. Zawsze na pierwszej linii frontu... Skazani na to, by nie przywiązywać się do niczego, bo nawet najlepszy przyjaciel może ulec korupcji...
Na szczęście zawsze jest Rufus. Może mało kumaty, ale lojalny, wierny, uczciwy i sympatyczny. Dobrze, że przygotował sprzęt dla Katarzyny. Po tym, co sobie zrobiła srebrem, ten języcznik naprawdę się jej przydał...
Jeszcze nie wiem, jak, ale wybłagam u mistrza, żeby zezwolił mi na to...
Muszę odnaleźć mordercę tej dziewczyny.
Musi ponieść stosowną karę.
Wiem, że zdaniem Katarzyny magom wolno wszystko, ale to nieprawda. Nie wolno zabijać bez dobrego powodu. A tamten mag doprowadził do sytuacji, w której prawie doszło do poważnego złamania Maskarady...

Właśnie! Przecież w pobliżu jest rezydent Srebrnej Świecy! Dlaczego on ani razu nie zainteresował się tematem? Czemu nie skontaktował się ze mną, nie sprawdził, o co chodzi z mandragorą...

Katarzyna też nic nie mówiła, by było jej coś wiadome o takiej osobie.
Racja, ona przecież by nawet nie wiedziała! To tylko człowiek.
Ale ja... Gdybym ja był na jego miejscu, sprawdziłbym to. Miałbym jakąś sieć sensorów czy coś... Przecież jest rezydentem. Jest odpowiedzialny, by takie rzeczy się nie działy, by wezwać terminusa!
Sprawdzę to przy okazji.

----
**Z myśli zaniepokojonej Kasi, nastęnego poranka po zniszczeniu banshee**

Samira źle wygląda.
Naprawdę wygląda, jakby była na największym kacu swojego życia. I to jeszcze zanim się obudziła.
Co więcej, zupełnie nie poruszyły ją te wszystkie hałasy...

I... o co tu chodzi? Dlaczego sama jej obecność sprawia, że czuję się... lepiej? Pełniesza..? Na pewno silniejsza. I ona... też tak ma. Przecież sama mi powiedziała, że kiedy jest jej źle, to myśli o mnie...
Nie znam jej przecież. Owszem, spotkałyśmy się raz czy dwa, ale...

O co tu chodzi..?

----
**Z myśli Kasi**

Naprawdę mam nadzieję, że August dopadnie tego, kto to zrobił.
Choć... wątpię, żeby tamten poniósł jakąkolwiek karę gorszą od oberwania po łapach.
W końcu ta dziewczyna była tylko człowiekiem...

+ Dramatis personae:

- czł: Andromeda (Kasia Nowak) jako człowiek ZNOWU wykonujący robotę za terminusa

- czł: Samira Diakon, wybitna iluzjonistka i NIE czarodziejka (choć eks-czarodziejka luster)
- mag: August Bankierz, młody terminus pilnujący Andromedy, który odkrywa, kim była Samira
- mag: Marcel Bankierz, uczeń tego samego mistrza co August, ale bardziej poważany i ogólnie lepszy :P.

+ Lokalizacje:
[[code]]
- miejscowość Otrąb
--- kolekcjoner osobliwości
[[/code]]