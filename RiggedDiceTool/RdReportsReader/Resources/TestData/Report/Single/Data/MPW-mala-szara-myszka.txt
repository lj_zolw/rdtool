+ Konspekt pisany

+++++ Kontynuacja:

Bezpośrednia kontynuacja:
[[[inwazja-konspekty:160629-rezydencja-e-polujemy-na-drone|160629 - Rezydencja? E, polujemy na dronę! (KB, DK, AB)]]]

+++++ Kontekst ogólny sytuacji

- Ozydiusz, Aleksander Sowiński, Sabina Sowińska nie żyją. Świeca jest pozbawiona głowy w Kopalinie. Zaczyna się frakcjonalizacja.
- Emilia rzuciła bombę - magitech Wiktora ma przydatną wiedzę, klucz do zniszczenia Szlachty. Pancerz magitechowy Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Emilia dowodzi siłami Kurtyny.
- Aneta Rainer przejęła dowodzenie nad siłami Lojalistów.
- Wiktor skonsolidował siły Szlachty w Kopalinie.
- Hipernet funkcjonuje z problemami. Mechaniczni terminusi wykonują ostatni rozkaz Ozydiusza.
- Ciągła plaga Irytki Sprzężonej. Mechaniczni terminusi i viciniusy trzymają porządek. Anioły Nadzorcze się dezaktywowały.
- Amanda Diakon zmusiła Mirabelkę Diakon do współpracy ze sobą.
- Rodzina Świecy vs Mausowie wchodzą w kolejną fazę eskalacji. Elea i Rufus uciekają się do drastycznych działań; seiras Maus nie pomaga. Bankierze vs Mausowie.
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji). Hektor pomoże z Baltazarem i Malią.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Amanda Diakon / Pasożyt (Millennium) powoli wchodzą na teren Kopalina pod pozorem sił Mirabelki. Jej cel to Ozydiusz i CruentusReverto oraz Milena.
- Diakoni zablokowani przez Mojrę; silny sceptycyzm co do działań Blakenbauerów (thanks, Romeo). Akcja z Wandą Ketran nie pomaga; Estrella nie wybaczy.
- Oktawian, Adela Mausowie i Dominik Bankierz zniknęli lub zginęli na wykopaliskach.
- Blakenbauerowie, jakimś cudem, są w sojuszu ze wszystkimi siłami.
- Świeca Daemonica jest odcięta od Fazy Materialnej przez bombę pryzmatyczną.

- Ktoś napuszcza na siebie Świecę i KADEM, Kurtynę i Szlachtę, Millennium i Świecę.
- Milena Diakon widziała Spustoszenie (?). Skąd i jak?
- Baltazar Maus będzie poświęcony na ołtarzu Rodziny Świecy i bezpieczeństwa (nie w tej misji).
- Pancerz Tamary i "kołysanka". Powiązanie ze Spustoszeniem?
- Adela Maus - zniknęła, Oktawian Maus - koma "od miecza", Dominik Bankierz nie żyje.
- The Governess w masce Wandy Ketran się pojawiła. Zmusiła Blakenbauerów do wycofania się z działań w Kopalinie mordując wszystko co oni kochają.

+++++ Punkt zerowy:
+++++ Misja właściwa:

Day 1:

The team which was sent to the Trocin has returned to the Mansion. Seeing the Desolation, the team sent the warning signal to Tymotheus and asked him to reconsider the location of the second Mansion. For now it is not a good moment to build a Mansion, especially not in the place where the Desolation has its lookout. Moira agreed, that it might not be wise to weaken the power of the candle especially here, especially now.

So here we are, with a manic murderer calling herself ‘The Governess’. Alina reported, that she has seen a drone similar to the one afflicted by Desolation in Trocin. That drone was used by Wanda. It was called “Czarny Błysk” and was used in the “Działo plazmowe” cybergoth club. That is, before she got kidnapped by the good Blakenbauer forces.

So once again we return to Wanda. She may have more in common with Desolation that initially seemed; however she was not aware of that - before the conundrum with Arazille Edwin has scanned her memory and make sure she is harmless to precious Marcelin. But it was not difficult to remember what was the source of the drone Wanda had. It was produced in a small company called “Skrzydłoróg” which is governed by Dagmara Czeluść. The drone given to Wanda was given for her to propagate the idea of these drones; she was to be a trendsetter in the cybergoth club. Hector considered this to be quite ominous especially with connection between those drones and Desolation.

So, Skrzydłoróg and Dagmara. Patrycja started to search through the records on the both the company and its owner. And it so happens, that Dagmara tried to make this company for a while but he succeeded only after Skubny gave her a very generous loan. Hector frowned - this one more time where Skubny is connected with all this. This is actually surprising to Edwin, but he said nothing; he decided to use his own contacts to make sure what is happening on the “Skubny front”.

So it seems that Dagmara is sponsored by Skubny. And it seems that the company is pretty fresh and new - eight years. But it started truly operating two years ago. Not a very fishy, but under those circumstances everything is fishy. So, nobody said that Blakenbauers cannot investigate the Desolation and the drones, did they? The special forces were sent to the company to acquire a drone and snoop around.

One logical path was Patrycja trying to get as much information on all of that as possible and she started to rummage through police records, archives, taxes, logistics… And another was sending Artur to buy a single drone. To support him, Alina and Dionizy were sent.

Artur, being a cutie he is, has managed to offend the clerk several times. He masqueraded himself - very well - as a simple thug willing to buy a drone for pal who is an idiot because he uses a white BMW. And Artur was a jerk as well. He has managed to convince poor clerk to call for her supervisor – Dagmara. And Dagmara actually stood up to Artur and told him that most of the drones which are being built are sent already. They are sold. There are no drones for him to buy. Artur told her that he is willing to buy the exhibition drone - the one other people are using to see if they want a drone. This particular drone is unlikely to ever be used anyway, his stupid pal is not going to actually do anything else than watch it.

Dagmara seems to be kind of an idealist - as he does not like Artur, she wanted him to go away and didn’t want to sell him the drone. She even said it is the same drone which was used in the “Działo plazmowe”. Artur said, that he is willing to pay twice he doesn’t care. He has money. Dagmara sold it to him and afterwards explained to the clerk that if she didn’t he might have attacked someone and mugged them to get it. Dagmara is completely convinced that Artur is going to use it to peep into girl’s houses.

Meanwhile, Alina noticed that a truck has appeared to get to the “Skrzydłoróg” company. They have started unloading some crates which are completely unmarked. The truck is also one of those generic trucks you can hire to transport stuff; it is not connected to any company at all. Alina has decided to sic a drone made by Klara on this truck. It might give them some information about what is really going on here.

Patrycja has unearthed something really interesting - it seems that the amount of traffic towards and from the company is simply too high; they need to have something underground. It is physically impossible for them to store all the materials or produce all those drones. Also, the unmarked truck is nowhere on the list of suppliers or receivers.

Klara has cut the drone which was brought to her by Artur and she dissected it to compare with the drones from Trocin. It seems that those drones are of the identical design. The drone Wanda was using did not have any component of Desolation, nor it had a pigeon inside. But there were places proving, that a pigeon could be introduced over there and it was not a problem to replace rudimentary AI with Desolation. So it seems that desolated drones come from this particular factory. Quite a find.

Klara has a drone which traversed the words on the top of the truck has arrived at its destination. It was the steelworks in CITY. The drone decided to escape without learning more information simply because it would have been found and for now the greatest advantage is to remain silent and undetected.

Day 2:

So there exists a connection between Desolation, steelworks and drones. Interesting indeed. But then the YouTube channel controlled by The Governess lit up and The Governess decided to recite a poem to her favorite prosecutor, Hektor Blakenbauer:

“Mała szara myszka w informacji sieci,
Była nieostrożna i w pułapkę leci,
Próżno jej się trudzić, próżno jej uciekać,
Bo jej przeznaczenie już tu na nią czeka.”

It was unlisted like other videos on The Governess’ channel. Hector has decided to profile her, to understand what is she trying to do. It hit him - this channel is monitored only by The Governess and him. So this message was sent to him. She wanted him to read this poem. Meanwhile, he received the phone call from the Cleo. Cleo asked, if he had some secret orders for Patrycja, because Patrycja is nowhere to be seen. She was supposed to return home but she didn’t.

Slightly stressed, Hector made an investigation – Patrycja entered her house, but never got to her flat. She got kidnapped. And when Dionizy hacked again into The Governess’ computer and phone he heard a looped recording. It was the recording of Patrycja talking with The Governess were Patrycja - obviously scared - asked what is going on and why is he here, while The Governess explained that Hector has done something horrible and Patrycja may pay for this in the short term future.

Hector almost snapped from rage. But for now there’s not much he can do...

+ Progresja

Hektor Blakenbauer, traci Patrycję Krowiowską
Skrzydłoróg, nie nadąża z produkcją dron by dorównać sprzedaży; daje 1 Fire więcej.
Patrycja Krowiowska, wpada w ręce The Governess.
The Governess, łapie Patrycję Krowiowską.

+ Konsekwencje

Blakenbauerowie odkrywają powiązanie między hutą "Chrobry" w Czeliminie, Skrzydłorogiem w Bażantowie i Spustoszonymi dronami w Trocinie. Informują o tym Mojrę (informacja trafia do Agresta). The Governess porywa Patrycję Krowiowską. Hektor odkrywa powiązanie między Dagmarą Czeluść a Skubnym. Klara odkrywa, że Spustoszone drony powstają według planu w Skrzydłorogu.

+ Dramatis personae

mag: Hektor Blakenbauer, który oburzał się na wszechobecność Skubnego i rozpaczliwie szukał Patrycji. Zrozumiał, że The Governess się z nim komunikuje...
mag: Klara Blakenbauer, która dronami wykryła połączenie między miastami i Spustoszeniem oraz odkryła, że drona "Czarny Błysk" ze Skrzydłorogu może być wektorem Spustoszenia.
vic: Dionizy Kret, pomagał w hackowaniu komórki Patrycji by ją zlokalizować. Też: wyczaił, że Skrzydłorogu chronią ludzie Skubnego.

mag: The Governess, porwała Patrycję by zatrzymać Blakenbauerów przed dalszymi odkryciami. Komunikuje się z Hektorem przez kanał Youtube.
czł: Patrycja Krowiowska, która powiązała linie logistyczne dron ze Skrzydłorogu i znalazła powiązanie ze Skubnym. Porwana w odwecie przez The Governess.
czł: Artur Bryś, nieelegancki dla kobiet jak zwykle, udawał dresa kupującego kumplowi dresowi dronę. Obraził Grażynę i zdrażnił Dagmarę. Ale kupił dronę.
czł: Dagmara Czeluść, idealistka mająca dziwne powiązanie z Szymonem Skubnym; szefowa firmy Skrzydłoróg.
czł: Grażyna Czegrzyn, nieszczęsna asystentka klienta (sprzedawczyni + prezenterka) w Skrzydłorogu która musiała mieć do czynienia z Arturem Brysiem...
czł: Szymon Skubny, który okazał się być niebywale przyjazny i pomocny pani Dagmarze Czeluść, dając jej tanie pożyczki na firmę i sponsorując drony.

frk: firma Skrzydłoróg, cierpi na chwilową nadsprzedaż dron i jest chroniona przez ludzi Skubnego.

+ Lokalizacje

# Świat
 # Śląsk
  # Bażantów
   # Nieużytek wschodni, obszar raczej rekultywowany i mało zamieszkany; otworzyli tam inkubator przemysłu
    # Skrzydłoróg, firma produkująca drony i mająca większy IN/OUT niż to możliwe ze względu na magazyny
  # Czelimin
   # Pańczyk
    # huta Chrobry, skąd wyjeżdżają ciężarówki dostarczające coś do Skrzydłorogu w Bażantowie
  # Kopalin
   # Obrzeża
    # Rezydencja Blakenbauerów, centrum dowodzenia gdzie nic wielkiego się nie działo

+ Wątki

- Spustoszenie
- The Governess

+ Skrypt