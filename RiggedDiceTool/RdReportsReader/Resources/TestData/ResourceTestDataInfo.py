from RdReportsReader.Src.CommonTools.FileOps.AbsoluteFileLocation import AbsoluteFileLocation


class ResourceTestDataInfo:

    def ProfileMasterlistFolderPath(self):
        return AbsoluteFileLocation().of_file(__file__, "./Profile/Masterlist/Data")

    def ProfileSingleFolderPath(self):
        return AbsoluteFileLocation().of_file(__file__, "./Profile/Single/Data")

    def ProfileCollectionFolderPath(self):
        return AbsoluteFileLocation().of_file(__file__, "./Profile/Collection/Data")

    def ReportMasterlistFolderPath(self):
        return AbsoluteFileLocation().of_file(__file__, "./Report/Masterlist/Data")

    def ReportSingleFolderPath(self):
        return AbsoluteFileLocation().of_file(__file__, "./Report/Single/Data")

    def ReportCollectionFolderPath(self):
        return AbsoluteFileLocation().of_file(__file__, "./Report/Collection/Data")
