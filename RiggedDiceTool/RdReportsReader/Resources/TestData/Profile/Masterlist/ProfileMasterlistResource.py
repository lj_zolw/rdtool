class ProfileMasterlistResource:

    dict = {}

    def __init__(self):
        self.dict[self.F03S04P08_Simple()] = "F03S04P08_Simple.txt"

    def filename_matching_key(self, key):
        return self.dict[key]

    def keys(self):
        return list(self.dict.keys())

    # pseudo-enum methods

    def F03S04P08_Simple(self):
        return "F03S04P08_Simple"
