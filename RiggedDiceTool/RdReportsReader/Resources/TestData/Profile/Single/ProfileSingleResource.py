class ProfileSingleResource:

    dict = {}

    def __init__(self):
        self.dict[self.AndreaWilgacz_1411()] = "inwazja-1411-andrea-wilgacz.txt"
        self.dict[self.Andromeda_KasiaNowak_1411()] = "inwazja-1411-andromeda-kasia-nowak.txt"
        self.dict[self.AugustynSzczypiorek_1411()] = "inwazja-1411-augustyn-szczypiorek.txt"
        self.dict[self.DianaWeiner_1411()] = "inwazja-1411-diana-weiner.txt"
        self.dict[self.DracenaDiakon_1411] = "inwazja-1411-dracena-diakon.txt"
        self.dict[self.RadoslawSzczypiorek_1411()] = "inwazja-1411-radoslaw-szczypiorek.txt"
        self.dict[self.SamiraDiakon_1411()] = "inwazja-1411-samira-diakon.txt"
        self.dict[self.ZannaSzczypiorek_1411()] = "inwazja-1411-zanna-szczypiorek.txt"
        self.dict[self.KrystianKorzunio_1601()] = "inwazja-1601-krystian-korzunio.txt"

    def filename_matching_key(self, key):
        return self.dict[key]

    def keys(self):
        return list(self.dict.keys())

    # pseudo-enum methods

    def AndreaWilgacz_1411(self):
        return "AndreaWilgacz_1411"

    def Andromeda_KasiaNowak_1411(self):
        return "Andromeda_KasiaNowak_1411"

    def AugustynSzczypiorek_1411(self):
        return "AugustynSzczypiorek_1411"

    def DianaWeiner_1411(self):
        return "DianaWeiner_1411"

    def DracenaDiakon_1411(self):
        return "DracenaDiakon_1411"

    def RadoslawSzczypiorek_1411(self):
        return 'RadoslawSzczypiorek_1411'

    def SamiraDiakon_1411(self):
        return "SamiraDiakon_1411"

    def ZannaSzczypiorek_1411(self):
        return "ZannaSzczypiorek_1411"

    def KrystianKorzunio_1601(self):
        return "KrystianKorzunio_1601"
