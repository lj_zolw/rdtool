+ Radosław Szczypiorek



+++ Na misjach:

+++++ Postać wystąpiła na: 4 misjach.
+++++ Postać wystąpiła w: 1 kampaniach: Czarodziejka Luster

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 130503 || 009 || [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]]||Czarodziejka Luster||
|| 130506 || 010 || [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]]||Czarodziejka Luster||
|| 130511 || 011 || [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]]||Czarodziejka Luster||
|| 131008 || 012 || [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]]||Czarodziejka Luster||

+++ Dokonania:

|| [[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|Renowacja obrazu Andromedy (An)]]] || (44 l) syn Augustyna i Żanny, który chciał zrobić ojcu prezent w formie renowacji jego portretu ||
|| [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|Sekrety rezydencji Szczypiorków (An)]]] || (44 l) syn Augustyna i Żanny i brat Jolanty. Wyraźnie silniejszy pozycją niż siostra, drugi po ojcu w rodzie. ||
|| [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|Ołtarz Podniesionej Dłoni (An)]]] || (44 l) który uprzednio porwał Samirę w imię większego dobra a jeszcze wcześniej zdradzał inklinacje do młodocianej Ingi. ||
|| [[[konspekty-zacmienie:131008-moj-aniol|<<Mój Anioł>> (An)]]] || (44 l) : syn Augustyna i Żanny. Prawdziwy potwór rodu Szczypiorków. KIA. ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||Andromeda (Kasia Nowak)||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||August Bankierz||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Augustyn Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Inga Wójt||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Samira Diakon||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Żanna Szczypiorek||4||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]], [[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Sandra Stryjek||3||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Jolanta Wójt||2||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]], [[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Antoni Wójt||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Artur Szmelc||1||[[[konspekty-zacmienie:130503-renowacja-obrazu-andromedy|130503]]]||
||Dariusz Germont||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||
||Feliks Bozur||1||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]]||
||Herbert Zioło||1||[[[konspekty-zacmienie:130511-oltarz-podniesionej-dloni|130511]]]||
||Izabela Kruczek||1||[[[konspekty-zacmienie:130506-sekrety-rezydencji-szczypiorkow|130506]]]||
||Zofia Szczypiorek||1||[[[konspekty-zacmienie:131008-moj-aniol|131008]]]||