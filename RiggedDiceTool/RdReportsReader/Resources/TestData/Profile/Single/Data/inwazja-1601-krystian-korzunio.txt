+ Krystian Korzunio

+++ Jaki jest:

Lojalny wobec Zajcewów, patriota, pragmatycznie bezwzględny, mówi tylko to co trzeba, zawsze próbuje poszerzyć swoje wpływy, wie kiedy się wycofać, ascetyczny, ostrożny

+++ Co umie:

fałszowanie śladów: 3 (6)
przekupstwo i korumpowanie: 3 (6)

włamywacz: 2 (4)
członek SB: 2 (4)
walka w mieście: 2 (4)
zapewnia dyskrecję: 2 (4)

+++ Kogo zna:

wiecznie poszerzana sieć informatorów: 3 (6)

kontakty w świecie przestępczym Zajcewów: 2 (4)
kolegów z czasów służby w SB: 2 (4)
"Kły Kaina"; kult do czyszczenia Maskarady: 2 (4)

+++ Co ma do dyspozycji:

haki na oficjeli w różnych miastach z czasów SB: 2 (4)
prywatny mały oddział (5-10) osób jako mięśnie: 2 (4)
nieletalne uzbrojenie; baseball, nóż, gaz pieprzowy : 2 (4)

+++ ElMet:

- brak; nie jest magiem

+++ Specjalne:



+++ Inklinacje:

Baza: wzmocniona (8)

||~ SCA ||~ SCD ||~ SCF ||~ KNO ||~ SPN ||~ CRF  ||~ FRT ||~ NMB ||
||  0  ||  +1  ||  -1  ||   +1  ||  -2  ||  0   ||  +1  ||  0  ||

Wyprowadzony profil:

||~ SCA ||~ SCD ||~ SCF ||~ KNO ||~ SPN ||~ CRF  ||~ FRT ||~ NMB ||
||  8  ||  9  ||  7  ||   8  ||  6  ||  9   ||  9  ||  8  ||

+++ Archetyp:

[[[mechanika-inwazja-archetypy-cleaner|Cleaner]]]
[[[mechanika-inwazja-archetypy-collector|Collector]]] (informacji / środków)

+++ Inne:

Krystian Korzunio used to be an officer of internal affairs, atm he is 52 years old. He is a very dangerous man, specializing in “acquisition and problem-solving”. In the same time he does not want to actually hurt people. He simply performs his job and performs his job well. Used to be an inspector having the rank of a lieutenant. During that time he has managed to create a wide array of people whom he knows and who knew him. When he managed to find some supernatural stuff and when he’s paths crossed with the paths of Zajcew, he was conscripted and became their man. He still considers himself a Polish patriot, even if the faction he comes from is generally created by general Polish population, but he doesn’t care. Does not use firearms; prefers good old-fashioned baseball bats and knives. Avoids killing people and causing irreversible harm – he will not hesitate a moment if he is left without other options, though.

+++ Na misjach:

+++++ Postać wystąpiła na: 5 misjach.
+++++ Postać wystąpiła w: 1 kampaniach: Ucieczka do Przodka

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 151013 || 072 || [[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|Kontrolowany odwrót z zamtuza (PT)]]]||Ucieczka do Przodka||
|| 151101 || 073 || [[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|Mafia Gali w szpitalu (PT)]]]||Ucieczka do Przodka||
|| 151220 || 074 || [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|Z Null Fieldem w garażu... (PT)]]]||Ucieczka do Przodka||
|| 151230 || 077 || [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|Zajcew ze śmietnika partnerem... (PT)]]]||Ucieczka do Przodka||
|| 160106 || 078 || [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|...I kult zostaje rozgromiony (PT)]]]||Ucieczka do Przodka||

+++ Dokonania:

|| [[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|Kontrolowany odwrót z zamtuza (PT)]]] || czyściciel Zajcewów, który próbuje posprzątać to co napaskudził Tymek i Gala. Zaczął od ściągnięcia "młodego wilka". ||
|| [[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|Mafia Gali w szpitalu (PT)]]] || czyściciel Zajcewów, który na rozkaz Gali wysadził dwa całkowicie niewinne samochody. ||
|| [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|Z Null Fieldem w garażu... (PT)]]] || kontynuuje usuwanie śladów; min. sprowadził "Kły Kaina" do Przodka by uwiarygodnić "sektę" i działania Gali i wiły. ||
|| [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|Zajcew ze śmietnika partnerem... (PT)]]] || który wpierw ściągnął sektę a teraz pracując dla Joachima (i Pauliny) chce zniszczyć sektę. Ostrzegł przed atakiem na kościół i go bronił. ||
|| [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|...I kult zostaje rozgromiony (PT)]]] || czyściciel który chciał ograniczyć ludzkie straty i który transportował Paulinę tam gdzie trzeba. Sojusznik Pauliny i 'agent' Joachima dowodzący akcją z wielu stron. ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||Maria Newa||5||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|151101]]], [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]], [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Paulina Tarczyńska||5||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|151101]]], [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]], [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Bartosz Bławatek||4||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]], [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Jerzy Karmelik||4||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|151101]]], [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]]||
||Artur Kurczak||3||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]]||
||Joachim Zajcew||3||[[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]], [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Agnieszka Mariacka||2||[[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]], [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Alicja Gąszcz||2||[[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]]||
||Gala Zajcew||2||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|151101]]]||
||Marzena Dorszaj||2||[[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|151101]]], [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]]||
||Ryszard Herman||2||[[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|151101]]], [[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]]||
||Tomasz Leżniak||2||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Tymoteusz Maus||2||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]], [[[konspekty-zacmienie:151101-mafia-gali-w-szpitalu|151101]]]||
||Wiesław Rekin||2||[[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]], [[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Łukasz Perkas||2||[[[konspekty-zacmienie:151220-z-nullfieldem-w-garazu|151220]]], [[[konspekty-zacmienie:151230-zajcew-ze-smietnika-partnerem|151230]]]||
||Franciszek Marlin||1||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]]||
||Grażyna Tuloz||1||[[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Hubert Rębski||1||[[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||
||Janusz Karzeł||1||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]]||
||Lea Swoboda||1||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]]||
||Maciej Orank||1||[[[konspekty-zacmienie:151013-kontrolowany-odwrot-zamt|151013]]]||
||Ochrona Kobra Przodek||1||[[[konspekty-zacmienie:160106-i-kult-zostaje-rozgromiony|160106]]]||