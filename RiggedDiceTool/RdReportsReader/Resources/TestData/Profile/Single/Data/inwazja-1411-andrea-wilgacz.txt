+ Andrea Wilgacz

+++ Co potrafi:

magiczny analityk +++
znany w półświatkach paser informacji +++
zawsze znajdzie swoją ofiarę +++
detektyw +++

terminus drugoliniowy ++
archiwistka ++
wróżka ++
dywersja ++

włamania +
pojedzie wszystkim +
pułapki +

+++ Jaka jest:

musi poznać prawdę +++
przedkłada (swoją) moralność ponad prawo +++
terminus z powołania: ma swój obraz tego, czym terminus jest i wciąż jest terminusem w głębi duszy, niezależnie od papierów +++

ma mentalność szczura ++
lojalna ++
pragmatyczna ++

ludzie jej wierzą +

+++ Kogo zna:

szefa lokalnej mafii (traktuje ją jak przybraną córkę) +++
ludzi półświatka (magicznego i nie) +++

burdelmamę burdelu z kralothami ++
kadrę średnio renomowanej uczelni magicznej, na której zrobiła doktorat ++
kilku średnio przepadających za nią terminusów ++

okolicznych detektywów (głównie ludzi, kilku magów) +

+++ Jakie ma zasoby:

Franciszek Maus
Kontakty w rodzinie Weiner



Specjalne:

terminusi SŚ nie biorą jej na poważnie
doktorat: wykorzystanie legend i wierzeń w utrzymaniu Maskarady

[[code]]
===========|existo|regulo|commuto|teneo|
energia  1 |  x   |      |  x    |  x  |
aevitas  1 |      |      |       |  x  |
atomos   1 |      |      |  x    |  x  |
aether   1 |      |  x   |       |  x  |
mentis   1 |  x   |      |       |  x  |
---------------------------------------|
[[/code]]

-----

+++ Na misjach:

+++++ Postać wystąpiła na: 26 misjach.
+++++ Postać wystąpiła w: 3 kampaniach: <nie rozdzielone>, Mała prywatna wojenka, Druga Inwazja

||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||
|| 140408 || 023 || [[[konspekty-zacmienie:140408-czarny-kamaz|Czarny Kamaz (AW, WZ)]]]||<nie rozdzielone>||
|| 140409 || 024 || [[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|Czwarta Frakcja Zajcewów (AW, WZ)]]]||<nie rozdzielone>||
|| 140503 || 025 || [[[konspekty-zacmienie:140503-wolanie-o-pomoc|Wołanie o pomoc (AW, Til (temp), Viv (temp))]]]||Mała prywatna wojenka||
|| 140505 || 026 || [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|Musiał zginąć, bo Maus (AW)]]]||Mała prywatna wojenka||
|| 140508 || 027 || [[[konspekty-zacmienie:140508-lord-jonatan|"Lord Jonatan" (AW)]]]||Mała prywatna wojenka||
|| 150718 || 047 || [[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|Splątane tropy: Spustoszenie? (AW)]]]||Mała prywatna wojenka||
|| 150913 || 051 || [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|Andrea węszy koło Szlachty (AW)]]]||Mała prywatna wojenka||
|| 150920 || 056 || [[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|Sprawa Baltazara Mausa (AW)]]]||Mała prywatna wojenka||
|| 150922 || 063 || [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|Och nie! Porwali Ignata! (AW, SD)]]]||Mała prywatna wojenka||
|| 140103 || 097 || [[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|Tak bardzo nie artefakt(AW, WZ)]]]||Druga Inwazja||
|| 140109 || 098 || [[[konspekty-zacmienie:140109-uczniowie-moriatha|Uczniowie Moriatha(AW, WZ)]]]||Druga Inwazja||
|| 140114 || 099 || [[[konspekty-zacmienie:140114-zaginiony-czlonek|Zaginiony członek(AW, HB)]]]||Druga Inwazja||
|| 140121 || 100 || [[[konspekty-zacmienie:140121-znikniecie-sophistii|Zniknięcie Sophistii(AW, HB)]]]||Druga Inwazja||
|| 140201 || 101 || [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|Ona zdradza, on zdradza(AW)]]]||Druga Inwazja||
|| 140208 || 102 || [[[konspekty-zacmienie:140208-na-ratunek-terminusce|Na ratunek terminusce(AW, WZ)]]]||Druga Inwazja||
|| 140213 || 103 || [[[konspekty-zacmienie:140213-pulapka-na-edwina|Pułapka na Edwina(AW, WZ, HB)]]]||Druga Inwazja||
|| 140219 || 104 || [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|Niespodziewane wsparcie(AW, WZ)]]]||Druga Inwazja||
|| 140227 || 105 || [[[konspekty-zacmienie:140227-sophistia-x-marcelin|Sophistia x Marcelin(AW, WZ, HB)]]]||Druga Inwazja||
|| 140320 || 106 || [[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|Sprawa magicznych samochodów(AW, HB)]]]||Druga Inwazja||
|| 140312 || 107 || [[[konspekty-zacmienie:140312-atak-na-rezydencje|Atak na rezydencję Blakenbauerów (AW, WZ, HB)]]]||Druga Inwazja||
|| 140401 || 108 || [[[konspekty-zacmienie:140401-mojra-moriath|Mojra, Moriath (AW, WZ, HB)]]]||Druga Inwazja||
|| 140604 || 109 || [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|Patriarcha Blakenbauer (AW, WZ, HB)]]]||Druga Inwazja||
|| 140611 || 110 || [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|Rezydencja Blakenbauerów (AW, WZ, HB)]]]||Druga Inwazja||
|| 140618 || 111 || [[[konspekty-zacmienie:140618-upadek-agresta|Upadek Agresta (AW, WZ, HB)]]]||Druga Inwazja||
|| 140625 || 112 || [[[konspekty-zacmienie:140625-ostatnia-saith|Ostatnia Saith (AW, WZ, HB)]]]||Druga Inwazja||
|| 140708 || 113 || [[[konspekty-zacmienie:140708-druga-inwazja|Druga Inwazja (AW, WZ, HB)]]]||Druga Inwazja||

+++ Dokonania:

|| [[[konspekty-zacmienie:140408-czarny-kamaz|Czarny Kamaz (AW, WZ)]]] || wstrzyknięta przez Mariana Agresta obserwatorka świata Zajcewów mająca raportować działania "Prawdziwej Świecy". ||
|| [[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|Czwarta Frakcja Zajcewów (AW, WZ)]]] || łączniczka SŚ w sprawie Kamaza. ||
|| [[[konspekty-zacmienie:140503-wolanie-o-pomoc|Wołanie o pomoc (AW, Til (temp), Viv (temp))]]] || terminus analityczny działający jako liniowy ||
|| [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|Musiał zginąć, bo Maus (AW)]]] || śledcza próbująca zrozumieć prawdę o Rodzinie Świecy i Mausach ||
|| [[[konspekty-zacmienie:140508-lord-jonatan|"Lord Jonatan" (AW)]]] || ta, która zatrzymała Aderialitha sprzężonego z cieniem Karradraela. ||
|| [[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|Splątane tropy: Spustoszenie? (AW)]]] || inspektor śledczy Wydziału Wewnętrznego Srebrnej Świecy, przesunięta z tematu "Rodzina Świecy" na temat "Tamara Muszkiet". ||
|| [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|Andrea węszy koło Szlachty (AW)]]] || zbierająca informacje o Szlachcie i Spustoszeniu używając uprawnień Wydziału Wewnętrznego. ||
|| [[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|Sprawa Baltazara Mausa (AW)]]] || która zaczyna dostrzegać powiązania między: Oktawianem, Mileną, Szlachtą, Baltazarem Mausem, Elizawietą, KADEMem? Podejrzewa kogoś z zewnątrz, lub odległą siłę w Świecy. ||
|| [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|Och nie! Porwali Ignata! (AW, SD)]]] || którą plan Silurii zwiódł. Zaskoczył ją stan Mileny, sny o Spustoszeniu i rewelacje Elizawiety. Też: Ignat. ||
|| [[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|Tak bardzo nie artefakt(AW, WZ)]]] || detektyw szukająca kolii w kształcie kralotha by zdrażnić jej właścicielkę. ||
|| [[[konspekty-zacmienie:140109-uczniowie-moriatha|Uczniowie Moriatha(AW, WZ)]]] || czarodziejka odkrywająca tajemnice przeszłości i nieszczęsnego ducha Moriatha. ||
|| [[[konspekty-zacmienie:140114-zaginiony-czlonek|Zaginiony członek(AW, HB)]]] || detektyw wynajęta do odnalezienia psa... i znalazła tego psa mimo że trzeba było Zupaczce auto wywieźć na lawecie. ||
|| [[[konspekty-zacmienie:140121-znikniecie-sophistii|Zniknięcie Sophistii(AW, HB)]]] || bezwzględna detektyw szukająca Sophistii i ciężko poraniona ofiara Estery Piryt w formie pośredniej z Dromopod Iserat. ||
|| [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|Ona zdradza, on zdradza(AW)]]] || detektyw która nie chce się angażować w sprawy sercowe... które jednak okazują się dużo bardziej poważne. ||
|| [[[konspekty-zacmienie:140208-na-ratunek-terminusce|Na ratunek terminusce(AW, WZ)]]] || czarodziejka znająca kilka osób które, jak się okazuje, warto znać (min. Juliusza Szamana). ||
|| [[[konspekty-zacmienie:140213-pulapka-na-edwina|Pułapka na Edwina(AW, WZ, HB)]]] || która skupia się na agregacji informacji, na dezinformacji i namierzaniu celów dla Wacława. ||
|| [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|Niespodziewane wsparcie(AW, WZ)]]] || detektyw i osoba umiejąca rozmawiać z nagą Krystalią w dziwnych okolicznościach. ||
|| [[[konspekty-zacmienie:140227-sophistia-x-marcelin|Sophistia x Marcelin(AW, WZ, HB)]]] || która ściągnęła Zupaczkę by znaleźć Sophistię. Też Wacława. Służy też jako analityk magiczny i powiedziała Hektorowi co z Marcelinem. ||
|| [[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|Sprawa magicznych samochodów(AW, HB)]]] || osoba negocjująca z Hektorem uwolnienie Siedeła a potem rozwiązująca tajemniczy artefakt złożony w formie samochodów (robota Quasar) ||
|| [[[konspekty-zacmienie:140312-atak-na-rezydencje|Atak na rezydencję Blakenbauerów (AW, WZ, HB)]]] || analityk magiczny i osoba kojarząca źródła informacji ze wszystkich możliwych stron by odsłonić o co chodziło z projektem "Moriath". ||
|| [[[konspekty-zacmienie:140401-mojra-moriath|Mojra, Moriath (AW, WZ, HB)]]] || która z Wacławem wymusiła od Hektora prawdę. Jako analityk magiczny dała radę pomóc Hektorowi odnaleźć Edwina. ||
|| [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|Patriarcha Blakenbauer (AW, WZ, HB)]]] || ta, co chce skontaktować Irinę z Quasar i blokuje ruchy Mariana Agresta mające zatrzymać Zespół. ||
|| [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|Rezydencja Blakenbauerów (AW, WZ, HB)]]] || która ku swemu zdumieniu odkryła, że Marian Agrest ma jakąś dziwną agendę połączoną z eksterminacją magów... ||
|| [[[konspekty-zacmienie:140618-upadek-agresta|Upadek Agresta (AW, WZ, HB)]]] || odwalająca robotę detektywistyczną i nawiązująca potencjalne sojusze; skupia się na Agreście i Mai Kos i pomaga Czerwcowi. ||
|| [[[konspekty-zacmienie:140625-ostatnia-saith|Ostatnia Saith (AW, WZ, HB)]]] || która dowiaduje się, że tak naprawdę ona i Agrest stali po tej samej stronie. Odkrywa sposób w jaki działa Inwazja. ||
|| [[[konspekty-zacmienie:140708-druga-inwazja|Druga Inwazja (AW, WZ, HB)]]] || którą uratowała Teresa przed Inwazją; jej sojusznicy wreszcie zaczęli współpracować i Andrea nadal ma dostęp do tajnych danych SŚ. ||

+++ Relacje z postaciami:

||~ Z kim ||~ Intensywność ||~ Na misjach ||
||Wacław Zajcew||15||[[[konspekty-zacmienie:140408-czarny-kamaz|140408]]], [[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|140409]]], [[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140109-uczniowie-moriatha|140109]]], [[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Hektor Blakenbauer||12||[[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|140320]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Marian Agrest||11||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]], [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]], [[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Marcelin Blakenbauer||10||[[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]], [[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|140320]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Waldemar Zupaczka||10||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140109-uczniowie-moriatha|140109]]], [[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Irina Zajcew||9||[[[konspekty-zacmienie:140408-czarny-kamaz|140408]]], [[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|140409]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]], [[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Grzegorz Czerwiec||8||[[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Teresa Żyraf||8||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Amelia Eter||7||[[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|140320]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]]||
||Krystalia Diakon||7||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Mojra||7||[[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Estera Piryt||6||[[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Dariusz Kopyto||5||[[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Edwin Blakenbauer||5||[[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Ika||5||[[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|140320]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Karolina Maus||5||[[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Tatiana Zajcew||5||[[[konspekty-zacmienie:140408-czarny-kamaz|140408]]], [[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|140409]]], [[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Anna Kozak||4||[[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]], [[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Artur Żupan||4||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]], [[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]]||
||Dracena Diakon||4||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Jan Szczupak||4||[[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]]||
||Juliusz Szaman||4||[[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Maja Kos||4||[[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Milena Diakon||4||[[[konspekty-zacmienie:140508-lord-jonatan|140508]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]], [[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Otton Blakenbauer||4||[[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]]||
||Quasar||4||[[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|140320]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Sebastian Tecznia||4||[[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Aleksander Sowiński||3||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]], [[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]]||
||Benjamin Zajcew||3||[[[konspekty-zacmienie:140408-czarny-kamaz|140408]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Borys Kumin||3||[[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140227-sophistia-x-marcelin|140227]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]]||
||Bożena Zajcew||3||[[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|140409]]], [[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140213-pulapka-na-edwina|140213]]]||
||Elżbieta Niemoc||3||[[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Ernest Maus||3||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]], [[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Gustaw Siedeł||3||[[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|140320]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]]||
||Izabela Łaniewska||3||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]], [[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Karol Poczciwiec||3||[[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]], [[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Mateusz Nieborak||3||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]], [[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Nela Welon||3||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]], [[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]]||
||Ozydiusz Bankierz||3||[[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]], [[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Swietłana Zajcew||3||[[[konspekty-zacmienie:140408-czarny-kamaz|140408]]], [[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|140409]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Alfred Kukułka||2||[[[konspekty-zacmienie:140219-niespodziewane-wsparcie|140219]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]]||
||Bolesław Bankierz||2||[[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]]||
||Cierń||2||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]], [[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Elizawieta Zajcew||2||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Emilia Szudek||2||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]]||
||Franciszek Maus||2||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]], [[[konspekty-zacmienie:140611-rezydencja-blakenbauerow|140611]]]||
||Ignat Zajcew||2||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Ilarion Zajcew||2||[[[konspekty-zacmienie:140408-czarny-kamaz|140408]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]]||
||Jan Weiner||2||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]], [[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Judyta Karnisz||2||[[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Karol Maus||2||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]], [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Karradrael||2||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]], [[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Kermit Diakon||2||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Klotylda Świątek||2||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]], [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Kornelia Modrzejewska||2||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]], [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Kwiatuszek||2||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]], [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Malwina Krówka||2||[[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Margaret Blakenbauer||2||[[[konspekty-zacmienie:140401-mojra-moriath|140401]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]]||
||Oktawian Maus||2||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Radosław Krówka||2||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]], [[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]]||
||Rebeka Piryt||2||[[[konspekty-zacmienie:140109-uczniowie-moriatha|140109]]], [[[konspekty-zacmienie:140401-mojra-moriath|140401]]]||
||Remigiusz Zajcew||2||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]], [[[konspekty-zacmienie:140312-atak-na-rezydencje|140312]]]||
||Salazar Bankierz||2||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Tamara Muszkiet||2||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]], [[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Vuko Milić||2||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]], [[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Whisperwind||2||[[[konspekty-zacmienie:140618-upadek-agresta|140618]]], [[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Wojciech Tecznia||2||[[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]], [[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Yakim Zajcew||2||[[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|140409]]], [[[konspekty-zacmienie:140604-patriarcha-blakenbauer|140604]]]||
||Adam Pączek||1||[[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Adam Wołkowiec||1||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Adela Maus||1||[[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Adrian Kropiak||1||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]]||
||Amanda Diakon||1||[[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Andrzej Sowiński||1||[[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Antygona Diakon||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Aurel Czarko||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Aurelia Maus||1||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Baltazar Maus||1||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]]||
||Bogdan Bankierz||1||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]]||
||Bolesław Derwisz||1||[[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Cezary Sito||1||[[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Czarny Kamaz 314||1||[[[konspekty-zacmienie:140408-czarny-kamaz|140408]]]||
||Damian Bródka||1||[[[konspekty-zacmienie:140320-sprawa-magicznych-samoch|140320]]]||
||Diana Larent||1||[[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]]||
||Dominik Bankierz||1||[[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Draconis Diakon||1||[[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Elea Maus||1||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]]||
||Estrella Diakon||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||GS 'Aegis' 0003||1||[[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Gabriela Resort||1||[[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Gala Zajcew||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Hlargahlotl||1||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Irena Resort||1||[[[konspekty-zacmienie:150913-andrea-weszy-kolo-szlachty|150913]]]||
||Ireneusz Bankierz||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Jonatan Maus||1||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Julia Weiner||1||[[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Jędrzej Zdun||1||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]]||
||Kaspian Bankierz||1||[[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]]||
||Kornelia Szudek||1||[[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]]||
||Krystian Maus||1||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]]||
||Krzysztof Wieczorek||1||[[[konspekty-zacmienie:140503-wolanie-o-pomoc|140503]]]||
||Maciej Zajcew||1||[[[konspekty-zacmienie:140409-czwarta-frakcja-zajcewow|140409]]]||
||Malia Bankierz||1||[[[konspekty-zacmienie:150920-sprawa-baltazara-mausa|150920]]]||
||Marcel Bankierz||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Marian Rustyk||1||[[[konspekty-zacmienie:140201-ona-zdradza-on-zdradza|140201]]]||
||Marian Welkrat||1||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Mateusz Krówka||1||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]]||
||Mieszko Bankierz||1||[[[konspekty-zacmienie:140708-druga-inwazja|140708]]]||
||Mordecja Diakon||1||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]]||
||Netheria Diakon||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Ofelia Caesar||1||[[[konspekty-zacmienie:140103-tak-bardzo-nie-artefakt|140103]]]||
||Otton Blakenbauer: KIA||1||[[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Paweł Grzęda||1||[[[konspekty-zacmienie:140114-zaginiony-czlonek|140114]]]||
||Renata Maus||1||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Rufus Eter||1||[[[konspekty-zacmienie:140121-znikniecie-sophistii|140121]]]||
||Saith Catapult||1||[[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Saith Flamecaller||1||[[[konspekty-zacmienie:140618-upadek-agresta|140618]]]||
||Saith Kameleon||1||[[[konspekty-zacmienie:140625-ostatnia-saith|140625]]]||
||Sieciech Bankierz||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Siluria Diakon||1||[[[konspekty-zacmienie:150922-och-nie-porwali-ignata|150922]]]||
||Tadeusz Aster||1||[[[konspekty-zacmienie:140505-musial-zginac-bo-maus|140505]]]||
||Tadeusz Baran||1||[[[konspekty-zacmienie:140508-lord-jonatan|140508]]]||
||Tadeusz Czerwiecki||1||[[[konspekty-zacmienie:140401-mojra-moriath|140401]]]||
||Tymoteusz Maus||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Wiktor Sowiński||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||
||Zdzisław Zajcew||1||[[[konspekty-zacmienie:140208-na-ratunek-terminusce|140208]]]||
||Zenon Weiner||1||[[[konspekty-zacmienie:150718-splatane-tropy-spustoszenie|150718]]]||