class ProfileCollectionResource:

    dict = {}

    def __init__(self):
        self.dict[self.F03S04P08_SimpleComplete()] = "F03S04P08_SimpleComplete.txt"

    def filename_matching_key(self, key):
        return self.dict[key]

    def keys(self):
        return list(self.dict.keys())

    # pseudo-enum methods

    def F03S04P08_SimpleComplete(self):
        return "F03S04P08_SimpleComplete"
