import os


class AbsoluteFileLocation:

    def of_file(self, reference_file, target_file):
        file_location = os.path.dirname(os.path.abspath(reference_file))
        combined_file_location = os.path.normpath(os.path.join(file_location, target_file))
        return combined_file_location
