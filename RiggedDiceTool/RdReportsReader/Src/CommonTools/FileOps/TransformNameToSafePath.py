from RdReportsReader.Src.CommonTools.TextTransform.SpecialSymbolsCorrector import SpecialSymbolsCorrector


class TransformNameToSafePath:

    def file_with_path_to_save_at(self, name, rd_save_path):

        spaceless_name = name.replace(" ", "")
        char_file_name = SpecialSymbolsCorrector().strip_special_symbols(spaceless_name) + ".txt"
        target_file = rd_save_path + "/" + char_file_name
        return target_file
