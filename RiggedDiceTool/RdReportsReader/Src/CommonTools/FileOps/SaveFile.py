class SaveFile:

    def store(self, what, where):
        to_write = open(where, 'w', encoding='utf-8')
        to_write.write(what)
        to_write.close()
