class ReadFile:

    def from_path(self, file_location):

        file_to_read = open(file_location, mode='r', encoding='utf-8')
        read_text = file_to_read.read()
        file_to_read.close()

        return read_text
