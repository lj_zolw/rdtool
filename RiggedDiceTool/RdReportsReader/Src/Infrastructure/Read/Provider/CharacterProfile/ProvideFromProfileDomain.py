from RdReportsReader.Src.DomModel.CharacterProfile.Masterlist.Creation.ProfileMasterlistFactory import ProfileMasterlistFactory
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Partial.Read.Collection.Creation.ReadProfileCollectionFactory import ReadProfileCollectionFactory


class ProvideFromProfileDomain:

    def __init__(self, source):
        self.source = source

    def masterlist(self):
        profile_masterlist_text = self.source.profileMasterlistText()
        profile_masterlist = ProfileMasterlistFactory().createFromText(profile_masterlist_text)
        return profile_masterlist

    def collection(self):
        rd_profiles = self.source.rdProfiles()
        profiles_collection = ReadProfileCollectionFactory().createFromRdProfilesMasterlist(rd_profiles, self.masterlist())
        return profiles_collection
