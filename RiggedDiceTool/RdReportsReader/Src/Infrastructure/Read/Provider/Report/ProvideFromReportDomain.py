from RdReportsReader.Src.DomModel.Report.Collection.Creation.CreateReportCollection import CreateReportCollection
from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Creation.ReportMasterListFactory import ReportMasterlistFactory


class ProvideFromReportDomain:

    def __init__(self, source):
        self.source = source

    def masterlist(self):
        report_masterlist_text = self.source.reportMasterlistText()
        report_masterlist = ReportMasterlistFactory().createFromText(report_masterlist_text)
        return report_masterlist

    def collection(self):

        rd_reports = self.source.rdReports()
        mission_report_collection = CreateReportCollection().having(rd_reports, self.masterlist())
        return mission_report_collection
