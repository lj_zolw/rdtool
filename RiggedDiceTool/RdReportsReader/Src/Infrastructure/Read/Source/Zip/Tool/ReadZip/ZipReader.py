from RdReportsReader.Src.Configuration.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig
from .Command.SelectNewestRdMasterFile import SelectNewestRdMasterFile
from .Command.UnpackRdMasterZipFile import UnpackRdMasterZipFile


class ZipReader:
    
    def unpackIntoTempFile(self):

        rd_master_file_location = ProvideZipSourceConfig().folder_with_zip_backup_file() + "/"
        master_file_pattern = ProvideZipSourceConfig().zip_backup_file_pattern()
        newest_zip_master_file = SelectNewestRdMasterFile().fromZipFileFolder(rd_master_file_location, master_file_pattern)

        newest_zip_master_file_path = ProvideZipSourceConfig().folder_with_zip_backup_file() + "/" + newest_zip_master_file
        destination = ProvideZipSourceConfig().temp_dir_to_create_patch()
        UnpackRdMasterZipFile().unpackFromTo(newest_zip_master_file_path, destination)
