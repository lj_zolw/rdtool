import zipfile


class UnpackRdMasterZipFile:

    def unpackFromTo(self, zip_file_to_open, destination):

        zip = zipfile.ZipFile(zip_file_to_open)
        zip.extractall(destination)
        zip.close()
