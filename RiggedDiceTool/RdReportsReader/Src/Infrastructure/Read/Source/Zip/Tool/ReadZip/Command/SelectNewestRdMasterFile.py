import os
import re


class SelectNewestRdMasterFile:

    def fromZipFileFolder(self, zip_file_folder, master_file_pattern):

        all_zip_file_names = self.allZipFilesIn(zip_file_folder)
        most_recent_backup_file = self.selectMostRecentFile(all_zip_file_names, master_file_pattern)
        return most_recent_backup_file

    def selectMostRecentFile(self, all_zip_file_names, master_file_pattern):

        most_recent_file = ""
        most_recent_file_date = 0

        for single_filename in all_zip_file_names:
            depatterned = single_filename.replace(master_file_pattern, "")
            regex_split = re.search('(\d\d\d\d\d\d\d\d)(_.+)(.zip)', depatterned).groups()
            day_of_backup = regex_split[0]

            if day_of_backup:   # not none or empty
                day_from_this_file = int(day_of_backup)

                if day_from_this_file > most_recent_file_date:
                    most_recent_file_date = day_from_this_file
                    most_recent_file = single_filename

        return most_recent_file

    def allZipFilesIn(self, zip_file_folder):

        all_files = os.listdir(zip_file_folder)
        matching_files = [fi for fi in all_files if fi.endswith(".zip")]
        return matching_files
