from .Command.ReadProfileMasterlistFromZipFile import ReadProfileMasterlistFromZipFile
from RdReportsReader.Src.DomModel.CharacterProfile.Masterlist.Creation.ProfileMasterlistFactory import ProfileMasterlistFactory


class ZipProfileMasterlistSource:

    def masterlistText(self):
        return ReadProfileMasterlistFromZipFile().masterlistText()

    def masterlist(self):
        profiles_file = ReadProfileMasterlistFromZipFile().masterlistText()
        masterlist = ProfileMasterlistFactory().createFromText(profiles_file)
        return masterlist
