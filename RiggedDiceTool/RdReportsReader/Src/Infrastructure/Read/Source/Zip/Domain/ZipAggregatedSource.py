from ..Tool.ReadZip.ZipReader import ZipReader
from .Structure.Report.RdReports.ZipRdReportsSource import ZipRdReportsSource
from .Structure.Report.Masterlist.ZipReportMasterlistSource import ZipReportMasterlistSource
from .Structure.Profile.RdProfiles.ZipRdProfilesSource import ZipRdProfilesSource
from .Structure.Profile.Masterlist.ZipProfileMasterlistSource import ZipProfileMasterlistSource


class ZipAggregatedSource:

    def __init__(self):
        zipFile = ZipReader()
        zipFile.unpackIntoTempFile()

    def profileMasterlistText(self):
        return ZipProfileMasterlistSource().masterlistText()

    def rdProfiles(self):
        return ZipRdProfilesSource().rdProfiles()

    def reportMasterlistText(self):
        return ZipReportMasterlistSource().masterlistText()

    def rdReports(self):
        return ZipRdReportsSource().rdReports()
