from RdReportsReader.Src.CommonTools.FileOps.ReadFile import ReadFile
from RdReportsReader.Src.Configuration.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig
from RdReportsReader.Src.Infrastructure.Entities.Profile.RdProfile import RdProfile


class ReadRdProfilesFromZipSource:

    def withMetadataCollection(self, profile_metadata_collection):

        rd_profiles = []
        for singleMetadata in profile_metadata_collection:
            profile_text = self.readRdProfileTextFromLink(singleMetadata.ziplink())
            report = RdProfile(singleMetadata, profile_text)
            rd_profiles.append(report)

        return rd_profiles

    def readRdProfileTextFromLink(self, ziplink):

        file_location = ProvideZipSourceConfig().unpacked_folder_with_all_text_files() + ziplink + ".txt"
        return ReadFile().from_path(file_location)
