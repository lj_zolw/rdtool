from ..Masterlist.ZipReportMasterlistSource import ZipReportMasterlistSource
from .Command.ReadRdReportsFromZipSource import ReadRdReportsFromZipSource


class ZipRdReportsSource:

    def rdReports(self):

        masterlist = ZipReportMasterlistSource().masterlist()
        report_metadata_collection = masterlist.all_report_metadata()

        rd_reports = ReadRdReportsFromZipSource().withMetadataCollection(report_metadata_collection)
        return rd_reports
