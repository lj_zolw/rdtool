from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Creation.ReportMasterListFactory import ReportMasterlistFactory
from .Command.ReadReportMasterlistFromZipFile import ReadReportMasterlistFromZipFile


class ZipReportMasterlistSource:

    def masterlistText(self):
        return ReadReportMasterlistFromZipFile().masterlistText()

    def masterlist(self):
        reports_file = ReadReportMasterlistFromZipFile().masterlistText()
        masterlist = ReportMasterlistFactory().createFromText(reports_file)
        return masterlist
