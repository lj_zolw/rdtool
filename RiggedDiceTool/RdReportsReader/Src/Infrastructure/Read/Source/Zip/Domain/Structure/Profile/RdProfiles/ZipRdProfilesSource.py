from .Command.ReadRdProfilesFromZipSource import ReadRdProfilesFromZipSource
from ...Profile.Masterlist.ZipProfileMasterlistSource import ZipProfileMasterlistSource


class ZipRdProfilesSource:

    def rdProfiles(self):
        masterlist = ZipProfileMasterlistSource().masterlist()
        profile_metadata_collection = masterlist.all_metadata()

        rd_profiles = ReadRdProfilesFromZipSource().withMetadataCollection(profile_metadata_collection)
        return rd_profiles
