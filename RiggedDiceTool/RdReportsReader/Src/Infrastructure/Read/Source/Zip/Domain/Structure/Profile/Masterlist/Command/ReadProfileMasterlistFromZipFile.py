from RdReportsReader.Src.CommonTools.FileOps.ReadFile import ReadFile
from RdReportsReader.Src.Configuration.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig


class ReadProfileMasterlistFromZipFile:

    def masterlistText(self):

        file_location = ProvideZipSourceConfig().unpacked_folder_with_all_text_files() + ProvideZipSourceConfig().profile_masterlist_file_to_read()
        return ReadFile().from_path(file_location)
