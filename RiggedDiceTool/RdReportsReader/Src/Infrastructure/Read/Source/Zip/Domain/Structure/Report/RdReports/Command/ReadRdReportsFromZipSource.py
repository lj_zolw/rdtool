from RdReportsReader.Src.CommonTools.FileOps.ReadFile import ReadFile
from RdReportsReader.Src.Configuration.Providers.ProvideZipSourceConfig import ProvideZipSourceConfig
from RdReportsReader.Src.Infrastructure.Entities.Report.RdReport import RdReport


class ReadRdReportsFromZipSource:

    def withMetadataCollection(self, report_metadata_collection):

        rd_reports = []
        for singleMetadata in report_metadata_collection:
            report_text = self.readRdReportTextFromLink(singleMetadata.ziplink())
            report = RdReport(singleMetadata, report_text)
            rd_reports.append(report)

        return rd_reports

    def readRdReportTextFromLink(self, ziplink):

        file_location = ProvideZipSourceConfig().unpacked_folder_with_all_text_files() + ziplink + ".txt"
        return ReadFile().from_path(file_location)
