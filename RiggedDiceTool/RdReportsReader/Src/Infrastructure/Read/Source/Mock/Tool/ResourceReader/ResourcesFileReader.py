

class ResourcesFileReader:

    def loadResource(self, resource_folder, resource_name):

        resource_file_path = self.buildFilePath(resource_folder, resource_name)
        resource = self.readTextFrom(resource_file_path)
        return resource

    def buildFilePath(self, resource_folder, resource_name):
        return resource_folder + "/" + resource_name

    def readTextFrom(self, resource_file_path):

        toRead = open(resource_file_path, encoding='utf-8')
        readText = toRead.read()
        toRead.close()

        return readText
