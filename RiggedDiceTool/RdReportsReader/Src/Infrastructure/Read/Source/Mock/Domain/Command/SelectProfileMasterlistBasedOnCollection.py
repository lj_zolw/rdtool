import re

from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo
from RdReportsReader.Resources.TestData.Profile.Collection.ProfileCollectionResource import ProfileCollectionResource
from ...Tool.ResourceReader.ResourcesFileReader import ResourcesFileReader


class SelectProfileMasterlistBasedOnCollection:

    def masterlist_key_from_collection_key(self, selected_profile_collection):

        combined_resource_text = self._parse_collection_resource_file(selected_profile_collection)
        selected_masterlist = self._extract_selected_masterlist_key_from(combined_resource_text)
        return selected_masterlist

    def _extract_selected_masterlist_key_from(self, combined_resource_text):

        masterlist_regex = '(mlist:\s)(\w+)'
        extracted = re.search(masterlist_regex, combined_resource_text)
        masterlist_resource = extracted.groups()[-1]

        return masterlist_resource

    def _parse_collection_resource_file(self, selected_profile_collection):
        path = ProfileCollectionResource().filename_matching_key(selected_profile_collection)
        return ResourcesFileReader().loadResource(ResourceTestDataInfo().ProfileCollectionFolderPath(), path)
