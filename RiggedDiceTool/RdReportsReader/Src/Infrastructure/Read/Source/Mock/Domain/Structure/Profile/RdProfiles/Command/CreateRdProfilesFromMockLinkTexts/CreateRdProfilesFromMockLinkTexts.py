from RdReportsReader.Src.Infrastructure.Entities.Profile.RdProfile import RdProfile
from .Command.ConstructProfileMetadataFromPseudoLinkText import ConstructProfileMetadataFromPseudoLinkText


class CreateRdProfilesFromMockLinkTexts:

    def createFrom(self, rd_profile_link_texts, masterlist_text):

        rd_profiles = []
        for link_text in rd_profile_link_texts:
            profile_metadata = ConstructProfileMetadataFromPseudoLinkText().buildFrom(link_text[0], masterlist_text)
            profile_text = link_text[1]

            rd_profile = RdProfile(profile_metadata, profile_text)
            rd_profiles.append(rd_profile)

        return rd_profiles
