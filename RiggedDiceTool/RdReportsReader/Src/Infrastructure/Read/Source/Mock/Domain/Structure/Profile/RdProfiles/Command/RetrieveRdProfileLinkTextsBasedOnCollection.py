import re

from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo
from RdReportsReader.Resources.TestData.Profile.Collection.ProfileCollectionResource import ProfileCollectionResource
from RdReportsReader.Resources.TestData.Profile.Single.ProfileSingleResource import ProfileSingleResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Tool.ResourceReader.ResourcesFileReader import ResourcesFileReader


class RetrieveRdProfileLinkTextsBasedOnCollection:

    def basedOn(self, selected_profile_collection):

        combined_resource_text = self._load_collection_resource_file(selected_profile_collection)
        selected_profiles_paths = self._extract_selected_profile_paths_from(combined_resource_text)
        profile_link_texts = self._retrieve_profile_link_texts_from(selected_profiles_paths)

        return profile_link_texts

    def _retrieve_profile_link_texts_from(self, selected_profiles):

        profile_link_texts = []
        for resource in selected_profiles:
            text = ResourcesFileReader().loadResource(ResourceTestDataInfo().ProfileSingleFolderPath(), resource)
            link_text = [resource, text]
            profile_link_texts.append(link_text)

        return profile_link_texts

    def _extract_selected_profile_paths_from(self, combined_resource_text):

        masterlist_regex = '(profile:\s)(\w+)'
        extracted = re.findall(masterlist_regex, combined_resource_text)

        profile_key_names = []
        for pair in extracted:
            profile_key_names.append(pair[-1])

        profile_paths = []
        for key in profile_key_names:
            profile_paths.append(ProfileSingleResource().filename_matching_key(key))

        return profile_paths

    def _load_collection_resource_file(self, selected_collection):
        path = ProfileCollectionResource().filename_matching_key(selected_collection)
        return ResourcesFileReader().loadResource(ResourceTestDataInfo().ProfileCollectionFolderPath(), path)
