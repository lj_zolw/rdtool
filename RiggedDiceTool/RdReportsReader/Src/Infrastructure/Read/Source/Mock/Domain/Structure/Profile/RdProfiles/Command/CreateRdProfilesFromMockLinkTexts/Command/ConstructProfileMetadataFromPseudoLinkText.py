import re

from RdReportsReader.Src.DomModel.CharacterProfile.Metadata.Single.Creation.CreateSingleProfileMetadata import CreateSingleProfileMetadata


class ConstructProfileMetadataFromPseudoLinkText:

    def buildFrom(self, pseudolink, masterlist_text):

        masterlist_record = self.extractCorrespondingMasterlistRecordfrom(pseudolink, masterlist_text)
        metadata = CreateSingleProfileMetadata().fromRawRdMetadataRecord(masterlist_record)

        return metadata

    def extractCorrespondingMasterlistRecordfrom(self, pseudolink, masterlist_text):

        regex = '(.+karty-postaci-\d\d\d\d.)(.+)(\|.+)'
        split_records = re.findall(regex, masterlist_text)

        selected_record = None
        for candidate in split_records:
            if candidate[1] in pseudolink:
                selected_record = candidate

        rebuild_record = ''.join(selected_record)
        return rebuild_record
