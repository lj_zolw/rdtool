from RdReportsReader.Src.DomModel.Report.Metadata.Single.Creation.CreateSingleReportMetadata import CreateSingleReportMetadata
from RdReportsReader.Src.Infrastructure.Entities.Report.RdReport import RdReport


class CreateRdReportsFromMockLinkTexts:

    def createFrom(self, rd_report_link_texts, masterlist_text):

        rd_reports = []
        for link_text in rd_report_link_texts:
            report_metadata = self._construct_metadata_from_pseudo_link(link_text[0], masterlist_text)
            report_text = link_text[-1]

            rd_report = RdReport(report_metadata, report_text)
            rd_reports.append(rd_report)

        return rd_reports

    def _construct_metadata_from_pseudo_link(self, pseudolink, masterlist_text):

        pseudolink_core = pseudolink[4:-4]
        masterlist_lines = masterlist_text.split("\n")

        metadata_record = ""
        for line in masterlist_lines:
            if pseudolink_core in line:
                metadata_record = line
                break

        metadata = CreateSingleReportMetadata().fromRawRdMetadataRecord(metadata_record)
        return metadata
