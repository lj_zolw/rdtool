from ..Masterlist.MockReportMasterlistSource import MockReportMasterlistSource
from .Command.CreateRdReportsFromMockLinkTexts import CreateRdReportsFromMockLinkTexts
from .Command.RetrieveRdReportLinkTextsBasedOnCollection import RetrieveRdReportLinkTextsBasedOnCollection


class MockRdReportsSource:

    def return_selected_data_having_keys(self, selected_report_masterlist, selected_report_collection):

        rd_report_link_texts = RetrieveRdReportLinkTextsBasedOnCollection().basedOn(selected_report_collection)
        masterlist_text = self.load_selected_masterlist(selected_report_masterlist)
        rd_reports = CreateRdReportsFromMockLinkTexts().createFrom(rd_report_link_texts, masterlist_text)
        return rd_reports

    def load_selected_masterlist(self, report_masterlist_key):
        return MockReportMasterlistSource().returnSelectedMasterlistHavingKey(report_masterlist_key)
