from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Creation.ReportMasterListFactory import ReportMasterlistFactory
from .Command.SelectProfileMasterlistBasedOnCollection import SelectProfileMasterlistBasedOnCollection
from .Command.SelectReportMasterlistBasedOnCollection import SelectReportMasterlistBasedOnCollection
from .Structure.Profile.Masterlist.MockProfileMasterlistSource import MockProfileMasterlistSource
from .Structure.Profile.RdProfiles.MockRdProfilesSource import MockRdProfilesSource
from .Structure.Report.Masterlist.MockReportMasterlistSource import MockReportMasterlistSource
from .Structure.Report.RdReports.MockRdReportsSource import MockRdReportsSource


class MockAggregatedSource:

    def __init__(self):
        self.selected_profile_masterlist = None
        self.selected_profile_collection = None
        self.selected_report_masterlist = None
        self.selected_report_collection = None

    # Common source methods:

    def profileMasterlistText(self):
        if not self.selected_profile_masterlist:
            raise KeyError("ProfileMasterList dataset not selected from resources.")
        return MockProfileMasterlistSource().returnSelectedMasterlistHavingKey(self.selected_profile_masterlist)

    def rdProfiles(self):
        if not self.selected_profile_collection or not self.selected_profile_masterlist:
            raise KeyError("ProfileCollection dataset not selected from resources or malformed.")
        return MockRdProfilesSource().returnSelectedDataset(self.selected_profile_masterlist, self.selected_profile_collection)

    def reportMasterlistText(self):
        if not self.selected_report_masterlist:
            raise KeyError("ReportMasterList dataset not selected from resources.")
        return MockReportMasterlistSource().returnSelectedMasterlistHavingKey(self.selected_report_masterlist)

    def reportMasterlist(self):
        return ReportMasterlistFactory().createFromText(self.reportMasterlistText())

    def rdReports(self):
        if not self.selected_report_collection or not self.selected_report_masterlist:
            raise KeyError("ReportCollection dataset not selected from resources or malformed.")
        return MockRdReportsSource().return_selected_data_having_keys(self.selected_report_masterlist, self.selected_report_collection)

    # Mock specific methods:

    def selectProfileMasterlistResource(self, selectedProfileMasterlist):
        self.selected_profile_masterlist = selectedProfileMasterlist

    def selectProfileCollectionResource(self, selectedProfileCollection):
        self.selected_profile_masterlist = SelectProfileMasterlistBasedOnCollection().masterlist_key_from_collection_key(selectedProfileCollection)
        self.selected_profile_collection = selectedProfileCollection

    def selectReportMasterlistResource(self, selectedReportMasterlist):
        self.selected_report_masterlist = selectedReportMasterlist

    def selectReportCollectionResource(self, selected_report_collection):
        self.selected_report_masterlist = SelectReportMasterlistBasedOnCollection().masterlist_key_from_collection_key(selected_report_collection)
        self.selected_report_collection = selected_report_collection
