from ..Masterlist.MockProfileMasterlistSource import MockProfileMasterlistSource
from .Command.RetrieveRdProfileLinkTextsBasedOnCollection import RetrieveRdProfileLinkTextsBasedOnCollection
from .Command.CreateRdProfilesFromMockLinkTexts.CreateRdProfilesFromMockLinkTexts import CreateRdProfilesFromMockLinkTexts


class MockRdProfilesSource:

    def returnSelectedDataset(self, selected_profile_masterlist, selected_profile_collection):

        rd_profile_link_texts = RetrieveRdProfileLinkTextsBasedOnCollection().basedOn(selected_profile_collection)
        masterlist_text = self.loadSelectedMasterlist(selected_profile_masterlist)
        rd_profiles = CreateRdProfilesFromMockLinkTexts().createFrom(rd_profile_link_texts, masterlist_text)
        return rd_profiles

    def loadSelectedMasterlist(self, selected_profile_masterlist_key):
        return MockProfileMasterlistSource().returnSelectedMasterlistHavingKey(selected_profile_masterlist_key)
