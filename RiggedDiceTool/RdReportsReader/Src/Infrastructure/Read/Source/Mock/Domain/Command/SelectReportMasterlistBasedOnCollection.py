import re

from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo
from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from ...Tool.ResourceReader.ResourcesFileReader import ResourcesFileReader


class SelectReportMasterlistBasedOnCollection:

    def masterlist_key_from_collection_key(self, selected_report_collection):

        combined_resource_text = self._parse_collection_resource_file(selected_report_collection)
        selected_masterlist = self._extract_selected_masterlist_key_from(combined_resource_text)
        return selected_masterlist

    def _extract_selected_masterlist_key_from(self, combined_resource_text):

        masterlist_regex = '(mlist:\s)(\w+)'
        extracted = re.search(masterlist_regex, combined_resource_text)
        masterlist_resource = extracted.groups()[-1]

        return masterlist_resource

    def _parse_collection_resource_file(self, selected_report_collection):
        path = ReportCollectionResource().filename_matching_key(selected_report_collection)
        return ResourcesFileReader().loadResource(ResourceTestDataInfo().ReportCollectionFolderPath(), path)
