from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Tool.ResourceReader.ResourcesFileReader import ResourcesFileReader
from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo
from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource


class MockReportMasterlistSource:

    def returnSelectedDataset(self, resource_location):
        return ResourcesFileReader().loadResource(ResourceTestDataInfo().ReportMasterlistFolderPath(), resource_location)

    def returnSelectedMasterlistHavingKey(self, masterlist_key):
        value = ReportMasterlistResource().filename_matching_key(masterlist_key)
        return self.returnSelectedDataset(value)
