from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Tool.ResourceReader.ResourcesFileReader import ResourcesFileReader
from RdReportsReader.Resources.TestData.Profile.Masterlist.ProfileMasterlistResource import ProfileMasterlistResource
from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo


class MockProfileMasterlistSource:

    def returnSelectedDataset(self, resource_location):
        return ResourcesFileReader().loadResource(ResourceTestDataInfo().ProfileMasterlistFolderPath(), resource_location)

    def returnSelectedMasterlistHavingKey(self, masterlist_key):
        value = ProfileMasterlistResource().filename_matching_key(masterlist_key)
        return self.returnSelectedDataset(value)
