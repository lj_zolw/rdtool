import re

from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo
from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Resources.TestData.Report.Single.ReportSingleResource import ReportSingleResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Tool.ResourceReader.ResourcesFileReader import ResourcesFileReader


class RetrieveRdReportLinkTextsBasedOnCollection:

    def basedOn(self, selected_report_collection):

        combined_resource_text = self._load_collection_resource_file(selected_report_collection)
        selected_report_paths = self._extract_selected_report_paths_from(combined_resource_text)
        report_link_texts = self._retrieve_report_link_texts_from(selected_report_paths)

        return report_link_texts

    def _retrieve_report_link_texts_from(self, selected_report_paths):

        report_link_texts = []
        for resource in selected_report_paths:
            text = ResourcesFileReader().loadResource(ResourceTestDataInfo().ReportSingleFolderPath(), resource)
            link_text = [resource, text]
            report_link_texts.append(link_text)

        return report_link_texts

    def _extract_selected_report_paths_from(self, combined_resource_text):

        masterlist_regex = '(report:\s)(\w+)'
        extracted = re.findall(masterlist_regex, combined_resource_text)

        report_key_names = []
        for pair in extracted:
            report_key_names.append(pair[-1])

        report_paths = []
        for key in report_key_names:
            report_paths.append(ReportSingleResource().filename_matching_key(key))

        return report_paths

    def _load_collection_resource_file(self, selected_report_collection):
        path = ReportCollectionResource().filename_matching_key(selected_report_collection)
        return ResourcesFileReader().loadResource(ResourceTestDataInfo().ReportCollectionFolderPath(), path)
