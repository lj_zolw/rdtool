class PersistReport:

    def masterlist_at_dest(self, report_masterlist, data_destination):
        masterlist_text = report_masterlist.to_rd_text()
        data_destination.persist_report_masterlist(masterlist_text)

    def campaign_summary_collection_at_dest(self, campaign_summaries, data_destination):
        summary_list = campaign_summaries.summaries()
        for summary in summary_list:
            self._single_summary_at_dest(summary, data_destination)

    def _single_summary_at_dest(self, summary, data_destination):
        data_destination.persist_campaign_summary(summary.campaign_name(), summary.to_rd_text())
