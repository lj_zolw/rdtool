class PersistProfile:

    def complete_collection_at_dest(self, complete_profile_collection, destination):

        complete_profiles = complete_profile_collection.allProfiles()

        for profile in complete_profiles:
            self.single_profile_at_dest(profile, destination)

    def single_profile_at_dest(self, profile, destination):
        destination.persist_single_profile(profile.characterName(), profile.to_rd_text())
