class PersistLocation:

    def masterlist_at_dest(self, location_masterlist, data_destination):
        masterlist_text = location_masterlist.to_rd_text()
        data_destination.persist_location_masterlist(masterlist_text)
