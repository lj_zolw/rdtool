from .Command.FileRdProfileDestPersistSingle import FileRdProfileDestPersistSingle


class FileRdProfileDestination:

    def persist_single_profile(self, character_name, profile_text):
        FileRdProfileDestPersistSingle().persist_single_profile(character_name, profile_text)
