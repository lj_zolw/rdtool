import os

from RdReportsReader.Src.CommonTools.FileOps.SaveFile import SaveFile
from RdReportsReader.Src.CommonTools.FileOps.TransformNameToSafePath import TransformNameToSafePath
from RdReportsReader.Src.Configuration.Providers.ProvideGeneralFilepathConfig import ProvideGeneralFilepathConfig


class FileRdReportDestPersistCampaignSummary:

    def persist_summary_text(self, campaign_name, summary_text):

        rd_save_path = ProvideGeneralFilepathConfig().rd_dir_path
        summaries_save_path = rd_save_path + "/" + ProvideGeneralFilepathConfig().output_campaign_summaries_to_folder

        if not os.path.exists(summaries_save_path):
            os.makedirs(summaries_save_path)

        target_file = TransformNameToSafePath().file_with_path_to_save_at(campaign_name, summaries_save_path)

        try:
            SaveFile().store(summary_text, target_file)

        except Exception as e:
            print("Cannot persist CampaignSummary: " + target_file + ": " + str(e))
