import os

from RdReportsReader.Src.CommonTools.FileOps.SaveFile import SaveFile
from RdReportsReader.Src.CommonTools.FileOps.TransformNameToSafePath import TransformNameToSafePath
from RdReportsReader.Src.Configuration.Providers.ProvideGeneralFilepathConfig import ProvideGeneralFilepathConfig


class FileRdProfileDestPersistSingle:

    number_of_saves = 0

    def persist_single_profile(self, character_name, profile_text):

        profile_saving_subfolder = ProvideGeneralFilepathConfig().output_complete_profile_files_to_folder
        rd_save_path = self._path_to_persist_profiles_in(profile_saving_subfolder)

        if FileRdProfileDestPersistSingle.number_of_saves == 0:
            if not os.path.exists(rd_save_path):
                os.makedirs(rd_save_path)

        target_file = TransformNameToSafePath().file_with_path_to_save_at(character_name, rd_save_path)

        try:
            SaveFile().store(profile_text, target_file)
            self._announce_saves()

        except Exception as e:
            print("Cannot persist Profile: " + target_file + ": " + str(e))

    def _path_to_persist_profiles_in(self, profile_saving_subfolder):
        if not profile_saving_subfolder:
            rd_save_path = ProvideGeneralFilepathConfig().rd_dir_path
        else:
            rd_save_path = ProvideGeneralFilepathConfig().rd_dir_path + "/" + profile_saving_subfolder
        return rd_save_path

    def _announce_saves(self):
        FileRdProfileDestPersistSingle.number_of_saves += 1

        if FileRdProfileDestPersistSingle.number_of_saves % 50 == 0:
            print("Successfully saved: " + str(FileRdProfileDestPersistSingle.number_of_saves) + "+ Profiles.")
