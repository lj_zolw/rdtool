from .Command.FileRdReportDestPersistMasterlist import FileRdReportDestPersistMasterlist
from .Command.FileRdReportDestPersistCampaignSummary import FileRdReportDestPersistCampaignSummary


class FileRdReportDestination:

    def persist_masterlist(self, masterlist_text):
        FileRdReportDestPersistMasterlist().persistMasterlistText(masterlist_text)

    def persist_campaign_summary(self, campaign_name, summary_text):
        FileRdReportDestPersistCampaignSummary().persist_summary_text(campaign_name, summary_text)
