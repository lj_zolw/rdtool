from RdReportsReader.Src.CommonTools.FileOps.SaveFile import SaveFile
from RdReportsReader.Src.Configuration.Providers.ProvideGeneralFilepathConfig import ProvideGeneralFilepathConfig


class FileRdReportDestPersistMasterlist:

    def persistMasterlistText(self, masterlist_text):

        rd_save_path = ProvideGeneralFilepathConfig().rd_dir_path
        masterlist_save_path = rd_save_path + "/" + ProvideGeneralFilepathConfig().output_report_masterlist_file_to

        try:
            SaveFile().store(masterlist_text, masterlist_save_path)

            print("ReportMasterlist saved at: " + masterlist_save_path)

        except:
            print("Cannot persist ReportMasterlist: " + masterlist_save_path)
