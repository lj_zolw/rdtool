from .Structure.Report.FileRdReportDestination import FileRdReportDestination
from .Structure.Location.FileRdLocationDestination import FileRdLocationDestination
from .Structure.Profile.FileRdProfileDestination import FileRdProfileDestination


class FileAggregatedDestination:

    def persist_single_profile(self, character_name, profile_text):
        FileRdProfileDestination().persist_single_profile(character_name, profile_text)

    def persist_report_masterlist(self, masterlist_text):
        FileRdReportDestination().persist_masterlist(masterlist_text)

    def persist_location_masterlist(self, masterlist_text):
        FileRdLocationDestination().persist_masterlist(masterlist_text)

    def persist_campaign_summary(self, campaign_name, summary_text):
        FileRdReportDestination().persist_campaign_summary(campaign_name, summary_text)