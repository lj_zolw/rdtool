from .Command.FileRdLocationDestPersistMasterlist import FileRdLocationDestPersistMasterlist


class FileRdLocationDestination:

    def persist_masterlist(self, masterlist_text):
        FileRdLocationDestPersistMasterlist().persistMasterlistText(masterlist_text)
