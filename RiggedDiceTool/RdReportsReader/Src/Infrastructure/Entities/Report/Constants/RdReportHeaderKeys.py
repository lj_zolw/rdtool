class RdReportHeaderKeys:

    location_header = "lokalizacje"
    consequences_header = "konsekwencje"
    dramatis_personae_header = "dramatis personae"

    all_reserved_headers = [location_header, consequences_header, dramatis_personae_header]
