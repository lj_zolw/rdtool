from .Command.GetTextSectionContaining import GetTextSection
from .Command.GetNonReservedTextSections import GetNonReservedTextSections
from .Constants.RdReportHeaderKeys import RdReportHeaderKeys


class RdReport:

    report_metadata = None
    report_text = None

    def __init__(self, metadata, text):
        self.report_metadata = metadata
        self.report_text = text

    # properties

    def name(self):
        return self.report_metadata.name()

    def link(self):
        return self.report_metadata.weblink()

    def text(self):
        return self.report_text

    def metadata(self):
        return self.report_metadata

    # subset methods

    def dramatis_personae_text_section(self):
        return GetTextSection().containing_header(RdReportHeaderKeys.dramatis_personae_header, self.report_text)

    def location_text_section(self):
        return GetTextSection().containing_header(RdReportHeaderKeys.location_header, self.report_text)

    def consequences_text_section(self):
        return GetTextSection().containing_header(RdReportHeaderKeys.consequences_header, self.report_text)

    def non_reserved_text_section_block(self):
        return GetNonReservedTextSections().having_text(self.report_text)
