import re


class GetTextSection:

    def containing_header(self, matching_header, report_text):

        sections = re.split("^\+ ", report_text, flags=re.MULTILINE)

        for section in sections:
            if section:
                first_line = re.search('^.+', section).group(0)
                lowercased = first_line.lower()

                if matching_header.lower() in lowercased:
                    headerless_section = section.replace(first_line, "").strip()
                    return headerless_section

        return ""
