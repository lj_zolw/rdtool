import re

from RdReportsReader.Src.Infrastructure.Entities.Report.Constants.RdReportHeaderKeys import RdReportHeaderKeys


class GetNonReservedTextSections:

    def having_text(self, report_text):

        # this one is actually fun: we want to return only those sections used nowhere else.

        report_sections = re.split("^\+ ", report_text, flags=re.MULTILINE)

        report_text = ""
        for section in report_sections:
            if section:
                first_line = re.search('^.+', section).group(0)
                lowercased = first_line.lower()

                ignore = False
                if lowercased:
                    for reserved in RdReportHeaderKeys.all_reserved_headers:
                        if reserved.lower() in lowercased:
                            ignore = True

                if not ignore:
                    report_text = report_text + "+ " + section

        return report_text
