class RdProfile:

    profile_metadata = None
    profile_text = None

    def __init__(self, metadata, text):
        self.profile_metadata = metadata
        self.profile_text = text

    def characterName(self):
        return self.profile_metadata.characterName()

    def text(self):
        return self.profile_text
