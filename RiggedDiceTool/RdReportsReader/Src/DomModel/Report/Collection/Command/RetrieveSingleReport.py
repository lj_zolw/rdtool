class RetrieveSingleReport:

    def having_metadata(self, metadata_to_match, reports):
        for report in reports:
            if metadata_to_match.weblink() == report.link():
                return report

        return None
