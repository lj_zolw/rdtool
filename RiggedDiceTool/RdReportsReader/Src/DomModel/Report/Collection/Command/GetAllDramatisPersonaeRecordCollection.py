from RdReportsReader.Src.DomModel.Report.Single.Complete.Structure.DramatisPersonae.Structure.DpRecordCollection.DramatisPersonaeRecordCollection import DramatisPersonaeRecordCollection


class GetAllDramatisPersonaeRecordCollection:

    def from_reports(self, reports):

        all_dramatis_personae_records = []

        for single_report in reports:
            records_from_single_report = single_report.getDramatisPersonae().getDpRecords()

            for singleRecordFromSingleReport in records_from_single_report:
                all_dramatis_personae_records.append(singleRecordFromSingleReport)

        collection = DramatisPersonaeRecordCollection(all_dramatis_personae_records)
        return collection
