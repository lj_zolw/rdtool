from .GetAllDramatisPersonaeRecordCollection import GetAllDramatisPersonaeRecordCollection
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Partial.Generated.Collection.Creation.CreateGeneratedCharacterProfileCollection import CreateGeneratedCharacterProfileCollection


class GenerateProfilesFromReportCollection:

    def execute_on(self, mission_reports):

        dramatis_personae_records_collection = GetAllDramatisPersonaeRecordCollection().from_reports(mission_reports)
        actor_to_dp_records_collection = dramatis_personae_records_collection.createActorToRecordsCollection()

        actor_to_actor_relation_map_collection = dramatis_personae_records_collection.createActorToActorRelationMapCollection()
        actor_name_list = actor_to_dp_records_collection.all_actor_names_list()

        profile_collection = CreateGeneratedCharacterProfileCollection().forActorsFromSources(actor_name_list, actor_to_dp_records_collection, actor_to_actor_relation_map_collection)
        return profile_collection
