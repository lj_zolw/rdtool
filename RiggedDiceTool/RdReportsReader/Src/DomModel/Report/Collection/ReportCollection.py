from .Command.GenerateProfilesFromReportCollection import GenerateProfilesFromReportCollection
from .Command.RetrieveSingleReport import RetrieveSingleReport


class ReportCollection:

    reports = None

    def __init__(self, mission_reports):
        self.reports = mission_reports

    def mission_reports(self):
        return self.reports

    def all_locations_on_missions(self):
        all_locations_on_missions = []
        for single_report in self.reports:
            single_locations_in_report = single_report.getLocationsInReport()
            all_locations_on_missions.append(single_locations_in_report)
        return all_locations_on_missions

    def count(self):
        return len(self.reports)

    def generate_profiles(self):
        return GenerateProfilesFromReportCollection().execute_on(self.reports)

    def report_having_metadata(self, metadata):
        return RetrieveSingleReport().having_metadata(metadata, self.reports)
