from RdReportsReader.Src.DomModel.Report.Single.Complete.Creation.CreateSingleMissionReport import CreateSingleMissionReport
from ..ReportCollection import ReportCollection


class CreateReportCollection:

    def having(self, rd_reports, report_masterlist):
        
        reports = []

        for report in rd_reports:
            self._try_update_reports_from(reports, report, report_masterlist)

        return ReportCollection(reports)

    def _try_update_reports_from(self, reports, rd_report, report_masterlist):

        try:

            single_report = CreateSingleMissionReport().having(rd_report, report_masterlist)
            reports.append(single_report)

        except:

            print("RD_PARSE_FAILURE: Report: " + rd_report.name())

        else:
            pass
            # print("RD_PARSE_SUCCESS: Report: " + rd_report.name())
