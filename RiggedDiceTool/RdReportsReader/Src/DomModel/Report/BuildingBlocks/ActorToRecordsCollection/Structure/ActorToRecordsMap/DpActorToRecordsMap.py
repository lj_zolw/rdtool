class DpActorToRecordsMap:

    def __init__(self, actor_name):
        self.name = actor_name
        self.dp_records = []

    def all_records(self):
        return self.dp_records

    def actor_name(self):
        return self.name

    def append_record(self, current_record):
        self.dp_records.append(current_record)
