from ..DpActorToRecordsMap import DpActorToRecordsMap


class CreateActorToRecordsMap:

    def with_name(self, current_name):
        return DpActorToRecordsMap(current_name)
