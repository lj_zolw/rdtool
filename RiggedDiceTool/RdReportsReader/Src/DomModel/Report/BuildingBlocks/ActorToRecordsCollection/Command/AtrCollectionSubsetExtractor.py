class AtrCollectionSubsetExtractor:

    def records_map_for_single_actor(self, actor_name, actor_record_collection):

        found = None
        for single_record_map in actor_record_collection:
            if single_record_map.actor_name() == actor_name:
                found = single_record_map
                break

        return found
