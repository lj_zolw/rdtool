class AtrCollectionNameExtractor:

    def all_actor_names_from(self, actor_record_collection):

        names = []
        for single_record_map in actor_record_collection:
            actor_name = single_record_map.actor_name()
            names.append(actor_name)

        return names
