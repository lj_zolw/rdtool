from .Command.AtrCollectionNameExtractor import AtrCollectionNameExtractor
from .Command.AtrCollectionSubsetExtractor import AtrCollectionSubsetExtractor


class DpActorToRecordsCollection:

    actor_record_map_collection = None

    def __init__(self, actor_record_map_collection):
        self.actor_record_map_collection = actor_record_map_collection

    def all_actors(self):
        return self.actor_record_map_collection

    def all_actor_names_list(self):
        return AtrCollectionNameExtractor().all_actor_names_from(self.actor_record_map_collection)

    def get_for_single_actor(self, actor_name):
        return AtrCollectionSubsetExtractor().records_map_for_single_actor(actor_name, self.actor_record_map_collection)
