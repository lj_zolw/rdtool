from ..DpActorToRecordsCollection import DpActorToRecordsCollection
from ..Structure.ActorToRecordsMap.Creation.CreateActorToRecordsMap import CreateActorToRecordsMap


class CreateActorToRecordsCollection:

    def from_dp_records(self, dramatis_personae_records):

        name_sorted_records = sorted(dramatis_personae_records, key=lambda record: record.name())

        actor_records_array = []
        current_name = ""
        current_actor_record_map = None

        for currentRecord in name_sorted_records:

            if current_name != currentRecord.name():
                current_name = currentRecord.name()
                current_actor_record_map = CreateActorToRecordsMap().with_name(current_name)
                actor_records_array.append(current_actor_record_map)

            # this has guaranteed safety - currentName CANNOT be empty
            current_actor_record_map.append_record(currentRecord)

        collection = DpActorToRecordsCollection(actor_records_array)
        return collection
