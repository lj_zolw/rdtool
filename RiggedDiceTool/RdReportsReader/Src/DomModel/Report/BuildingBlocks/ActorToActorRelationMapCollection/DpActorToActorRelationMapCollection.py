from .Command.AtarmCollectionSubsetExtractor import AtarmCollectionSubsetExtractor


class DpActorToActorRelationMapCollection:

    relation_maps = None

    def __init__(self, relation_maps):
        self.relation_maps = relation_maps

    def get_for_single_actor(self, actor_name):
        return AtarmCollectionSubsetExtractor().records_map_for_single_actor(actor_name, self.relation_maps)
