class RelationWithSingleActor:

    char_name = None
    relation_intensity = 0
    records = []

    def __init__(self, char_name):
        self.char_name = char_name
        self.relation_intensity = 0
        self.records = []

    def intensity(self):
        return self.relation_intensity

    def dp_records(self):
        return self.records

    def update_with(self, dp_record):
        self.relation_intensity += 1
        self.records.append(dp_record)
