from .Creation.UpdateActorToActorRelationMap import UpdateActorToActorRelationMap


class DpActorToActorRelationMap:

    name = None
    ata_relations = None

    def __init__(self, actor_name):
        self.name = actor_name
        self.ata_relations = {}

    def actor_name(self):
        return self.name

    def actor_to_actor_relations(self):
        return self.ata_relations

    def update_with_record(self, dp_record):
        UpdateActorToActorRelationMap().update_map_with_record(self.ata_relations, dp_record)
