from ..DpActorToActorRelationMap import DpActorToActorRelationMap


class CreateActorToActorRelationMap:

    def from_actor_records_map(self, single_actor_records_map):

        actor_relation_map = DpActorToActorRelationMap(single_actor_records_map.actor_name())

        for dp_record in single_actor_records_map.all_records():
            dramatis_personae = dp_record.parentDramatisPersonae()
            sibling_records_on_mission = dramatis_personae.getOnlyDpRecordsSiblingTo(dp_record)

            for siblingRecord in sibling_records_on_mission:
                actor_relation_map.update_with_record(siblingRecord)

        return actor_relation_map
