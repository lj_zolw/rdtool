from RdReportsReader.Src.DomModel.Report.BuildingBlocks.ActorToRecordsCollection.Creation.CreateActorToRecordsCollection import CreateActorToRecordsCollection
from ..DpActorToActorRelationMapCollection import DpActorToActorRelationMapCollection
from ..Structure.ActorToActorRelationMap.Creation.CreateActorToActorRelationMap import CreateActorToActorRelationMap


class CreateActorToActorRelationMapCollection:

    def from_dp_records(self, dramatis_personae_records):

        atr_collection = CreateActorToRecordsCollection().from_dp_records(dramatis_personae_records)

        relation_maps = []
        for single_actor_records_map in atr_collection.all_actors():

            actor_to_actor_relation_map = CreateActorToActorRelationMap().from_actor_records_map(single_actor_records_map)
            relation_maps.append(actor_to_actor_relation_map)

        final_collection = DpActorToActorRelationMapCollection(relation_maps)
        return final_collection
