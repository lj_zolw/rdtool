class AtarmCollectionSubsetExtractor:

    def records_map_for_single_actor(self, actor_name, relation_maps):

        found = None
        for singleMap in relation_maps:
            if singleMap.actor_name() == actor_name:
                found = singleMap
                break

        return found
