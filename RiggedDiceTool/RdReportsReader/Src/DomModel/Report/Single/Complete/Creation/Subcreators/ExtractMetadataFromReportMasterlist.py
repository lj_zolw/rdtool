class ExtractMetadataFromReportMasterlist:

    def always_from(self, rd_report, report_masterlist):

        metadata_to_find = rd_report.metadata()
        found_metadata = report_masterlist.report_having_link(metadata_to_find.weblink())

        return found_metadata
