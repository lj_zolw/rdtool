from RdReportsReader.Src.DomModel.Location.Complete.Partial.Generated.Single.Creation.ExtractLocationsInReport import ExtractLocationsInReport
from RdReportsReader.Src.DomModel.Report.Single.Complete.Structure.Consequence.Creation.CreateReportConsequence import CreateReportConsequence
from RdReportsReader.Src.DomModel.Report.Single.Complete.Structure.DramatisPersonae.Creation.ExtractDramatisPersonae import ExtractDramatisPersonae
from .Subcreators.ExtractMetadataFromReportMasterlist import ExtractMetadataFromReportMasterlist
from ..SingleMissionReport import SingleMissionReport


class CreateSingleMissionReport:

    def having(self, rd_report, report_masterlist):

        report_text = rd_report.non_reserved_text_section_block()
        dramatis_personae = ExtractDramatisPersonae().always_from(rd_report)
        report_metadata = ExtractMetadataFromReportMasterlist().always_from(rd_report, report_masterlist)
        locations = ExtractLocationsInReport().optionally_from(rd_report)
        consequences = CreateReportConsequence().optionally_having(rd_report, report_metadata)

        report = SingleMissionReport(report_text, report_metadata, dramatis_personae, locations, consequences)

        dramatis_personae.setParentReport(report)

        return report
