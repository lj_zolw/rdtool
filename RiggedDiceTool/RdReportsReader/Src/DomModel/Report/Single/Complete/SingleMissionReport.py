class SingleMissionReport:

    report_text = None
    report_metadata = None
    dramatis_personae = None
    locations_in_report = None
    consequences_of_mission = None

    def __init__(self, report_text, report_metadata, dramatis_personae, locations_in_report, consequences):
        self.report_text = report_text
        self.report_metadata = report_metadata
        self.dramatis_personae = dramatis_personae
        self.locations_in_report = locations_in_report
        self.consequences_of_mission = consequences

    def getDramatisPersonae(self):
        return self.dramatis_personae

    def getLocationsInReport(self):
        return self.locations_in_report

    def consequences(self):
        return self.consequences_of_mission

    def missionDate(self):
        return self.report_metadata.date()

    def missionName(self):
        return self.report_metadata.name()

    def link(self):
        return self.report_metadata.weblink()

    def sequence_number(self):
        return self.report_metadata.sequence_number()

    def parentCampaign(self):
        return self.report_metadata.parentCampaign()
