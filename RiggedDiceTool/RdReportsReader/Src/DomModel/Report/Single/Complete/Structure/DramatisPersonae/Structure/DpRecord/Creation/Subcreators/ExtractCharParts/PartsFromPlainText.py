class PartsFromPlainText:

    delimiter = None

    def __init__(self, delimiter):
        self.delimiter = delimiter
    
    def extractCharName (self, original_record):
        
        # It is between ': ' and delimiter  
        start_index = original_record.index(": ")
        end_index = original_record.index(self.delimiter)
        actor_name = original_record[start_index + 2: end_index]
        
        return actor_name

    def extractCharReason (self, original_record):

        start_index = original_record.index(self.delimiter) + len(self.delimiter)
        end_index = len(original_record)
        actor_deed = original_record[start_index: end_index]
        
        return actor_deed
