class ConsequenceToRdText:

    def having(self, text, report_metadata):
        header = report_metadata.to_rd_text_no_list_no_comments()
        return header + "\n\n" + text
