from .Command.GetOnlyDpRecordsSubset import GetOnlyDpRecordsSubset
from .Creation.UpdateDpRecordsAtCreation import UpdateDpRecordsAtCreation


class ReportDramatisPersonae:

    dramatis_personae_records = None
    parent_report = None

    def __init__(self, d_personae_records):
        self.dramatis_personae_records = d_personae_records
        UpdateDpRecordsAtCreation().with_records(self, self.dramatis_personae_records)

    def setParentReport(self, parent_report):
        self.parent_report = parent_report

    def getParentReport(self):
        return self.parent_report

    def getDpRecords(self):
        return self.dramatis_personae_records

    def getOnlyDpRecordsSiblingTo(self, original_record):
        siblings = GetOnlyDpRecordsSubset(self.dramatis_personae_records).siblingsTo(original_record)
        return siblings
