class PartsFromZip:

    delimiter = None

    def __init__(self, delimiter):
        self.delimiter = delimiter

    def extractCharName(self, original_record):

        # [[[karty-postaci-inwazja-1411:dracena-diakon|Dracena Diakon]]] jako stereotypowo rozumiana Diakonka
        # It is between | and ]]]

        start_index = original_record.index("|")
        end_index = original_record.index("]]]")
        actor_name = original_record[start_index + 1: end_index]

        return actor_name

    def extractCharReason(self, original_record):

        start_index = original_record.index(self.delimiter) + len(self.delimiter)
        end_index = len(original_record)
        actor_deed = original_record[start_index: end_index]

        return actor_deed
