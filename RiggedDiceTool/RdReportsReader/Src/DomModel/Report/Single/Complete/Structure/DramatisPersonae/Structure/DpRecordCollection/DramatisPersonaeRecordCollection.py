from RdReportsReader.Src.DomModel.Report.BuildingBlocks.ActorToRecordsCollection.Creation.CreateActorToRecordsCollection import CreateActorToRecordsCollection
from RdReportsReader.Src.DomModel.Report.BuildingBlocks.ActorToActorRelationMapCollection.Creation.CreateActorToActorRelationMapCollection import CreateActorToActorRelationMapCollection


class DramatisPersonaeRecordCollection:

    dramatis_personae_records = None

    def __init__(self, dramatis_personae_records):
        self.dramatis_personae_records = dramatis_personae_records

    def createActorToRecordsCollection(self):
        atrCol = CreateActorToRecordsCollection().from_dp_records(self.dramatis_personae_records)
        return atrCol

    def createActorToActorRelationMapCollection(self):
        atarm = CreateActorToActorRelationMapCollection().from_dp_records(self.dramatis_personae_records)
        return atarm
