from .Command.CompareDpRecords import CompareDpRecords


class DramatisPersonaeRecord:

    actor_type = None
    mission_name = None
    mission_deed = None
    parent_d_personae = None

    def __init__(self, actorType, name, missionDeed):

        self.actor_type = actorType
        self.mission_name = name
        self.mission_deed = missionDeed

    def name(self):
        return self.mission_name

    def actorType(self):
        return self.actor_type

    def missionDeed(self):
        return self.mission_deed

    def sequence_number(self):
        return self.parent_d_personae.getParentReport().sequence_number()

    def parentDramatisPersonae(self):
        return self.parent_d_personae

    def parentReport(self):
        return self.parent_d_personae.getParentReport()

    def setParentDramatisPersonae(self, dramatisPersonae):
        self.parent_d_personae = dramatisPersonae

    def isTheSameAs(self, other_record):
        return CompareDpRecords().areTheSameRecordWithinOneMission(self, other_record)
