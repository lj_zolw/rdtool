from .Command.ConsequenceToRdText import ConsequenceToRdText


class ReportConsequence:

    text = None
    report_metadata = None

    def __init__(self, consequence_text, report_metadata):
        self.text = consequence_text
        self.report_metadata = report_metadata

    def to_rd_text(self):
        return ConsequenceToRdText().having(self.text, self.report_metadata)
