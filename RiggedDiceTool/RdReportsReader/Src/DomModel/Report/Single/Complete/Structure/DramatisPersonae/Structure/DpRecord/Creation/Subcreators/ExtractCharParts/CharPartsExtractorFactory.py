from .DelimiterDetector import DelimiterDetector
from .PartsFromZip import PartsFromZip
from .PartsFromPlainText import PartsFromPlainText


class CharPartsExtractorFactory:
    
    def buildSuitableExtractor(self, record):
        
        potential_delimiter = DelimiterDetector().detectDelimeter(record)

        if "[[[inwazja-karty-postaci" in record:
            return PartsFromZip(potential_delimiter)

        else:
            return PartsFromPlainText(potential_delimiter)
