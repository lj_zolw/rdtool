class CompareDpRecords:

    def areTheSameRecordWithinOneMission(self, record1, record2):

        if (record1.name() == record2.name()) and (record1.missionDeed() == record2.missionDeed()):
            return True

        return False
