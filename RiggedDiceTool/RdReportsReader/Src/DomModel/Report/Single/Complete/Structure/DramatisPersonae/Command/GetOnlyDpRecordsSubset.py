class GetOnlyDpRecordsSubset:

    dramatis_personae_records = None

    def __init__(self, dramatis_personae_records):
        self.dramatis_personae_records = dramatis_personae_records

    def siblingsTo(self, original_record):

        siblings = []
        for single_record in self.dramatis_personae_records:

            if not single_record.isTheSameAs(original_record):
                siblings.append(single_record)

        return siblings
