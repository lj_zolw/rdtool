import re

from ..DramatisPersonaeRecord import DramatisPersonaeRecord
from .Subcreators.ExtractCharParts.CharPartsExtractorFactory import CharPartsExtractorFactory

class DramatisPersonaeRecordFactory:

    def from_record_line(self, dp_record_line):

        extractor = CharPartsExtractorFactory().buildSuitableExtractor(dp_record_line)

        character_type = self._extract_char_type_from(dp_record_line)
        mission_name = self._extract_mission_name_from(dp_record_line, extractor)
        mission_deed = self._extract_mission_deed_from(dp_record_line, extractor)

        return DramatisPersonaeRecord(character_type, mission_name, mission_deed)

    def _extract_mission_deed_from(self, dp_record_line, extractor):

        mission_deed = extractor.extractCharReason(dp_record_line).strip()
        return mission_deed

    def _extract_mission_name_from(self, dp_record_line, extractor):

        name = extractor.extractCharName(dp_record_line).strip()
        return name

    def _extract_char_type_from(self, dp_record_line):

        # 3 letters before colon including colon

        regex_match = re.findall("...:", dp_record_line)
        char_type = regex_match[0:2][0]
        return char_type
