class UpdateDpRecordsAtCreation:

    def with_records(self, dramatis_personae, dramatis_personae_records):

        for record in dramatis_personae_records:
            record.setParentDramatisPersonae(dramatis_personae)
