class DelimiterDetector:

    def detectDelimeter(self, operated_line):
      
        index_of_comma = operated_line.find(",")
        index_of_jako = operated_line.find("jako")

        if index_of_comma == -1:
            return "jako"          

        if index_of_jako == -1:
            return ","
        
        if index_of_jako < index_of_comma:
            return "jako"
        else:
            return ","  
