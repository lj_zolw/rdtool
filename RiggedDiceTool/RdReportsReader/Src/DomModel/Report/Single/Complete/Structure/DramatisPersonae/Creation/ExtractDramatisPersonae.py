from ..ReportDramatisPersonae import ReportDramatisPersonae
from ..Structure.DpRecord.Creation.TextIntoDramatisPersonaeRecords import TextIntoDramatisPersonaeRecords


class ExtractDramatisPersonae:

    def always_from(self, rd_report):

        personae_text_section = rd_report.dramatis_personae_text_section()
        d_personae_records = TextIntoDramatisPersonaeRecords().from_section(personae_text_section)
        return ReportDramatisPersonae(d_personae_records)
