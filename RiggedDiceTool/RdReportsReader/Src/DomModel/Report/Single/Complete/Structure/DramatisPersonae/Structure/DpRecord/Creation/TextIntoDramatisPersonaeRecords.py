import re
from .DramatisPersonaeRecordFactory import DramatisPersonaeRecordFactory


class TextIntoDramatisPersonaeRecords:

    def from_section(self, dramatis_personae_text_section):

        personae_section_lines = dramatis_personae_text_section.split("\n")
        record_lines_only = self._extract_d_personae_record_lines_from(personae_section_lines)

        dp_records = []
        for record_line in record_lines_only:
            dp_record = DramatisPersonaeRecordFactory().from_record_line(record_line)
            dp_records.append(dp_record)

        return dp_records

    def _extract_d_personae_record_lines_from(self, d_personae_section_lines):

        d_personae_record_lines = []
        for potential_record in d_personae_section_lines:

            # this magical regexp means: '???: ?' where ? is a single symbol.
            regex_match = re.findall('...:\s.', potential_record)

            if len(regex_match) > 0:
                d_personae_record_lines.append(potential_record)

        return d_personae_record_lines
