from ..SingleReportMetadata import SingleReportMetadata
from RdReportsReader.Src.Configuration.Providers.ProvideInternalTokens import ProvideInternalTokens


class CreateSingleReportMetadata:

    def fromRawRdMetadataRecord(self, record):

        start = record.index(ProvideInternalTokens().one_of_reports_path_prefix())
        finish = record.index(ProvideInternalTokens().link_end())
        comment_start = finish + len(ProvideInternalTokens().link_end())
        comment_finish = len(record)

        agnostic = record[start: finish]
        extra_comments = record[comment_start: comment_finish]

        return self.fromMetadataRecord(agnostic, ProvideInternalTokens().link_display_separator(), extra_comments)

    def fromMetadataRecord(self, record, delimiter, extra_comments):

        separator_offset = record.index(delimiter)
        seq_no_offset = separator_offset + len(delimiter)
        date_offset = separator_offset + len(delimiter) + len("xxxx -")
        name_offset = separator_offset + len(delimiter) + len("xxxx -") + len(" yymmdd -")

        name = record[name_offset: len(record)]
        link = record[0: separator_offset]
        date = record[date_offset: date_offset + len("yymmdd")]
        sequence_number = record[seq_no_offset: seq_no_offset + 3]

        return SingleReportMetadata(name, link, date, sequence_number, extra_comments)
