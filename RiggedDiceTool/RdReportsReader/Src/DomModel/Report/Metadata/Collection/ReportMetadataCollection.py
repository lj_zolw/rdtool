class ReportMetadataCollection:

    metadata_collection = None

    def __init__(self, report_metadata_array):
        self.metadata_collection = report_metadata_array

    def mission_count(self):
        return len(self.metadata_collection)

    def all_metadata(self):
        return self.metadata_collection

    def append_report_metadata(self, metadata):
        self.metadata_collection.append(metadata)

    def single_metadata_at_position(self, position):
        return self.metadata_collection[position - 1]

    def single_metadata_having_link(self, weblink):
        for single_md in self.metadata_collection:
            if single_md.weblink() == weblink:
                return single_md
            
        return None
