from ..ReportMetadataCollection import ReportMetadataCollection


class CreateReportMetadataCollection:

    def empty_collection(self):
        return ReportMetadataCollection([])

    def collection_from_array(self, metadata_array):
        return ReportMetadataCollection(metadata_array)
