class ReportMasterlistCoreMetadataCount:

    def from_collection(self, campaigns_collection):

        mission_count = 0
        for campaign in campaigns_collection:
            mission_count = mission_count + campaign.mission_count()

        return mission_count
