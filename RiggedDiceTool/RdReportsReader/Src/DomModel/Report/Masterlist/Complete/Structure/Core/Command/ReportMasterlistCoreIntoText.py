class ReportMasterlistCoreIntoText:

    def fromCore(self, report_core):

        header = "+++ Inwazja:\n\n"
        campaigns_text = self.campaignsToText(report_core.all_campaigns())

        combined = header + campaigns_text
        return combined

    def campaignsToText(self, campaigns):

        campaigns_text = ""

        for campaign in campaigns:
            campaigns_text.strip()
            campaigns_text = campaigns_text + campaign.to_rd_text() + "\n\n"

        return campaigns_text.strip()
