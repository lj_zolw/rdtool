from .Command.CorrectReportSectionSequenceNumbers import CorrectReportSectionSequenceNumbers
from .Command.MasterListCampaignTraverse import MasterListCampaignTraverse
from .Command.ReportMasterlistCoreIntoText import ReportMasterlistCoreIntoText
from .Command.ReportMasterlistCoreMetadataCount import ReportMasterlistCoreMetadataCount
from .Command.ReportMasterlistCoreMetadataRetrieval import ReportMasterlistCoreMetadataRetrieval


class ReportMasterlistCore:

    campaign_list = None

    def __init__(self, campaign_list):
        self.campaign_list = campaign_list

    def all_campaigns(self):
        return self.campaign_list

    def all_metadata(self):
        return ReportMasterlistCoreMetadataRetrieval().from_campaigns(self.all_campaigns())

    def report_at_position(self, position):
        return MasterListCampaignTraverse().report_at_report_position(self.all_campaigns(), position)

    def report_having_link(self, weblink):
        return MasterListCampaignTraverse().report_having_link(self.all_campaigns(), weblink)

    def to_rd_text(self):
        return ReportMasterlistCoreIntoText().fromCore(self)

    def campaign_count(self):
        return len(self.all_campaigns())

    def metadata_count(self):
        return ReportMasterlistCoreMetadataCount().from_collection(self.all_campaigns())

    def correct_report_seq_numbers(self):
        CorrectReportSectionSequenceNumbers().for_all_campaigns(self.all_campaigns())
