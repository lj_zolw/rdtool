class ReportMasterlistPrelude:
    preludeText = None

    def __init__(self, text):
        self.preludeText = text

    def to_rd_text(self):
        return self.preludeText
