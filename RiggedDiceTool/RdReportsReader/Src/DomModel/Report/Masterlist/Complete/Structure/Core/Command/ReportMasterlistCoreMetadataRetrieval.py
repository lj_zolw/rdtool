class ReportMasterlistCoreMetadataRetrieval:

    def from_campaigns(self, campaigns_collection):

        missions = []
        for campaign in campaigns_collection:
            missions = missions + campaign.all_metadata()

        return missions
