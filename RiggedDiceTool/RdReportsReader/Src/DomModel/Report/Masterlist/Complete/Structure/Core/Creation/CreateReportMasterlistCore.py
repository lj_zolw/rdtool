from RdReportsReader.Src.DomModel.Report.Masterlist.Campaign.Single.Creation.CreateMasterlistCampaign import CreateMasterlistCampaign
from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Structure.Core.ReportMasterlistCore import ReportMasterlistCore


class CreateReportMasterlistCore:

    def from_text(self, report_masterlist_text):

        core_text_section = self._extract_core_text_section_from(report_masterlist_text)  #.split("\n")
        campaign_text_sections = self._split_core_text_into_campaign_texts(core_text_section)
        campaigns = self._build_campaigns_from_text_sections(campaign_text_sections)
        masterlist_core = ReportMasterlistCore(campaigns)
        return masterlist_core

    def _extract_core_text_section_from(self, report_masterlist_text):

        start_index = report_masterlist_text.index("+++ Inwazja")
        end_index = report_masterlist_text.index("++++ Koniec znanej historii Inwazji")
        masterlist_core_text = report_masterlist_text[start_index: end_index]
        return masterlist_core_text

    def _split_core_text_into_campaign_texts(self, core_text_section):

        split_core = core_text_section.split("++++")
        only_relevant = split_core[1:]

        campaign_text_sections = []

        for section in only_relevant:
            campaign_text_sections.append("++++" + section)

        return campaign_text_sections

    def _build_campaigns_from_text_sections(self, text_sections):

        campaigns = []

        for section in text_sections:
            campaign = CreateMasterlistCampaign().from_campaign_text_section(section)
            campaigns.append(campaign)

        return campaigns
