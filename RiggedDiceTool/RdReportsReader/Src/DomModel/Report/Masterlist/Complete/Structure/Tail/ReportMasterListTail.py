class ReportMasterlistTail:

    tailText = None

    def __init__(self, tail_text):
        self.tailText = tail_text

    def to_rd_text(self):
        return self.tailText
