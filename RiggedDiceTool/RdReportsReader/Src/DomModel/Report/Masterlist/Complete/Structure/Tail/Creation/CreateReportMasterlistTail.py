from ..ReportMasterListTail import ReportMasterlistTail


class CreateReportMasterlistTail:

    def from_text(self, reportMasterlistText):

        startOfTailIndex = reportMasterlistText.index("++++ Koniec znanej historii Inwazji")

        tailText = reportMasterlistText[startOfTailIndex: len(reportMasterlistText)]
        tail = ReportMasterlistTail(tailText.strip())

        return tail
