

class CorrectReportSectionSequenceNumbers:

    def for_all_campaigns(self, campaigns_collection):

        counter = int(campaigns_collection[0].metadata_at_position(1).sequence_number())

        for campaign in campaigns_collection:
            campaign.correct_sequence_numbers_starting_at(counter)
            counter = counter + campaign.mission_count()
