from ..ReportMasterlistPrelude import ReportMasterlistPrelude


class CreateReportMasterlistPrelude:

    def from_text(self, reportMasterlistText):

        startOfReportsIndex = reportMasterlistText.index("+++ Inwazja")

        preludeText = reportMasterlistText[0: 0 + startOfReportsIndex]
        prelude = ReportMasterlistPrelude(preludeText.strip())

        return prelude
