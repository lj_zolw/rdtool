class ReportMasterListToText:

    def fromComponents(self, prelude, core, tail):

        prelude_text = prelude.to_rd_text()
        core_text = core.to_rd_text()
        tail_text = tail.to_rd_text()

        result = ""

        if prelude_text != "":
            result += prelude_text + "\n\n"

        if core_text != "":
            result += core_text

        if tail_text != "":
            result += "\n\n" + tail_text

        return result
