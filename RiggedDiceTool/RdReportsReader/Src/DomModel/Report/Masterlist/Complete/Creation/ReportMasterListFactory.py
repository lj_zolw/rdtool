from ..Structure.Core.Creation.CreateReportMasterlistCore import CreateReportMasterlistCore
from ..Structure.Prelude.Creation.CreateReportMasterlistPrelude import CreateReportMasterlistPrelude
from ..Structure.Tail.Creation.CreateReportMasterlistTail import CreateReportMasterlistTail
from ..ReportMasterlist import ReportMasterlist


class ReportMasterlistFactory:

    def createFrom(self, prelude, core, tail):

        masterlist = ReportMasterlist(prelude, core, tail)
        masterlist.correct_report_seq_numbers()
        return masterlist

    def createFromText(self, report_masterlist_text):

        prelude = CreateReportMasterlistPrelude().from_text(report_masterlist_text)
        core = CreateReportMasterlistCore().from_text(report_masterlist_text)
        tail = CreateReportMasterlistTail().from_text(report_masterlist_text)

        masterlist = self.createFrom(prelude, core, tail)
        return masterlist
