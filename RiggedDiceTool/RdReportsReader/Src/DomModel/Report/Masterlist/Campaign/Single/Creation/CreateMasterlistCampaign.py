from ..MasterlistCampaign import MasterlistCampaign
from RdReportsReader.Src.DomModel.Report.Metadata.Single.Creation.CreateSingleReportMetadata import CreateSingleReportMetadata
from RdReportsReader.Src.Configuration.Providers.ProvideInternalTokens import ProvideInternalTokens


class CreateMasterlistCampaign:

    def empty_campaign_having_name_line(self, campaign_name_line):

        campaign_name = campaign_name_line[4: len(campaign_name_line)].strip()
        return MasterlistCampaign(campaign_name)

    def from_campaign_text_section(self, campaign_text_section):

        campaign = None

        for record in campaign_text_section.split('\n'):

            if ProvideInternalTokens().masterlist_campaign_header() in record:

                campaign = self.empty_campaign_having_name_line(record)

            elif ProvideInternalTokens().one_of_reports_path_prefix() in record:

                metadata = self._form_metadata_from_line(record)
                campaign.append_report_metadata(metadata)

            else:
                # ignore every other type of line
                pass

        return campaign

    def _form_metadata_from_line(self, metadata_record):
        return CreateSingleReportMetadata().fromRawRdMetadataRecord(metadata_record)
