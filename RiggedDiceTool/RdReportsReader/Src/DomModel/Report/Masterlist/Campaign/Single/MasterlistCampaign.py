from .Command.MasterListCampaignIntoText import MasterListCampaignIntoText
from .Command.CorrectCampaignSequenceNumbers import CorrectCampaignSequenceNumbers
from RdReportsReader.Src.DomModel.Report.Metadata.Collection.Creation.CreateReportMetadataCollection import CreateReportMetadataCollection


class MasterlistCampaign:

    campaign_name = ""
    metadata_collection = None

    def __init__(self, name):
        self.campaign_name = name
        self.metadata_collection = CreateReportMetadataCollection().empty_collection()

    def name(self):
        return self.campaign_name

    def mission_count(self):
        return self.metadata_collection.mission_count()

    def all_metadata(self):
        return self.metadata_collection.all_metadata()

    def append_report_metadata(self, metadata):
        metadata.updateWithCampaign(self)
        self.metadata_collection.append_report_metadata(metadata)

    def metadata_at_position(self, position):
        return self.metadata_collection.single_metadata_at_position(position)

    def metadata_having_link(self, weblink):
        return self.metadata_collection.single_metadata_having_link(weblink)

    def correct_sequence_numbers_starting_at(self, start_value):
        self.metadata_collection = CorrectCampaignSequenceNumbers().for_all_metadata_with_start_value_of(self.metadata_collection, start_value)

    def to_rd_text(self):
        return MasterListCampaignIntoText().to_rd_format(self)
