from RdReportsReader.Src.DomModel.Report.Metadata.Collection.Creation.CreateReportMetadataCollection import CreateReportMetadataCollection


class CorrectCampaignSequenceNumbers:

    def for_all_metadata_with_start_value_of(self, metadata_collection, start_value):

        all_metadata = metadata_collection.all_metadata()

        corrected_metadata = []
        for single_metadata in all_metadata:

            if single_metadata.sequence_number().casefold() != "xxx".casefold():

                string_seq_no = self._pad_to_3_digits_string(start_value)
                single_metadata.set_sequence_number(string_seq_no)

                corrected_metadata.append(single_metadata)

                start_value += 1

            else:
                corrected_metadata.append(single_metadata)

        return CreateReportMetadataCollection().collection_from_array(corrected_metadata)

    def _pad_to_3_digits_string(self, value):
        return "%03d" % value
