from RdReportsReader.Src.CommonTools.TextTransform.SpecialSymbolsCorrector import SpecialSymbolsCorrector


class MasterListCampaignIntoText:

    def to_rd_format(self, campaign):

        name_text = "++++ " + campaign.name() + "\n"
        campaign_mission_count_text = "Ilość misji: " + self._pad_to_3_digit_string(campaign.mission_count()) + "\n"
        campaign_summary_link = self._campaign_summary_link(campaign.name()) + "\n"
        separator = "\n"
        metadata_text = self._metadata_to_text(campaign.all_metadata())

        if metadata_text == "":
            combined = name_text
        else:
            combined = name_text + campaign_mission_count_text + campaign_summary_link + separator + metadata_text

        return combined

    def _campaign_summary_link(self, campaign_name):
        link_formed_campaign_name = SpecialSymbolsCorrector().strip_special_symbols(campaign_name.lower().replace(" ", "-"))[:25]
        if link_formed_campaign_name[-1] == "-":
            link_formed_campaign_name = link_formed_campaign_name[:-1]

        link = "[[[inwazja-konspekty-kampanie:" + link_formed_campaign_name + "|Streszczenie kampanii]]]"
        return link

    def _metadata_to_text(self, all_metadata):

        all_metadata_combined_text = ""

        for metadata in all_metadata:
            all_metadata_combined_text = all_metadata_combined_text + metadata.to_rd_text() + "\n"

        return all_metadata_combined_text.strip()

    def _pad_to_3_digit_string(self, value):
        return '%03d' % value
