class CampaignSummaryToRdText:

    def having(self, campaign, report_consequences):

        campaign_name = campaign.name()
        delimiter = "\n\n----\n"

        consequence_text = ""
        for consequence in report_consequences:
            consequence_text += delimiter
            consequence_text += consequence.to_rd_text()

        rd_text = "+++ " + campaign_name + consequence_text
        return rd_text
