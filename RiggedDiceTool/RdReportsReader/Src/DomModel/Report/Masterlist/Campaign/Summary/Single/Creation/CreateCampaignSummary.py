from ..CampaignSummary import CampaignSummary


class CreateCampaignSummary:

    def having(self, campaign, reports_in_campaign):

        report_consequences = []

        for report in reports_in_campaign:
            consequence = report.consequences()
            report_consequences.append(consequence)

        return CampaignSummary(campaign, report_consequences)
