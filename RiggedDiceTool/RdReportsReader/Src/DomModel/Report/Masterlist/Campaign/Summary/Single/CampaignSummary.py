from .Command.CampaignSummaryToRdText import CampaignSummaryToRdText


class CampaignSummary:

    campaign = None
    report_consequences = None

    def __init__(self, campaign, report_consequences):
        self.campaign = campaign
        self.report_consequences = report_consequences

    def campaign_name(self):
        return self.campaign.name()

    def to_rd_text(self):
        return CampaignSummaryToRdText().having(self.campaign, self.report_consequences)
