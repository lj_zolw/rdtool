class CampaignSummaryCollection:

    campaign_summaries = None

    def __init__(self, campaign_summaries):
        self.campaign_summaries = campaign_summaries

    def summaries(self):
        return self.campaign_summaries
