class CompleteProfileCollection:

    profiles = None

    def __init__(self, complete_profiles):
        self.profiles = complete_profiles

    def count(self):
        return len(self.profiles)

    def profileAtIndex(self, index):
        return self.profiles[index]

    def profileHavingName(self, character_name):
        profile = [single for single in self.profiles if single.characterName() == character_name][0]
        return profile

    def allProfiles(self):
        return self.profiles
