from ..CompleteProfileCollection import CompleteProfileCollection
from ...Single.Creation.CreateSingleCompleteProfile import CreateSingleCompleteProfile

class CreateCompleteProfileCollection:

    def mergeReadAndGenerated(self, read_profiles, generated_profiles, profile_masterlist):

        all_unique_names = self._sorted_unique_list_of_names(read_profiles, generated_profiles)

        complete_profiles = []
        for single_name in all_unique_names:
            single_read_profile = read_profiles.getProfileByName(single_name)
            single_generated_profile = generated_profiles.getProfileByName(single_name)

            single_complete_profile = CreateSingleCompleteProfile().having(single_name, single_read_profile, single_generated_profile, profile_masterlist)
            complete_profiles.append(single_complete_profile)

        collection = CompleteProfileCollection(complete_profiles)
        return collection


    def _sorted_unique_list_of_names(self, read_profiles, generated_profiles):

        read_profile_names = read_profiles.allNames()
        generated_profile_names = generated_profiles.allNames()

        read_profile_names.extend(generated_profile_names)
        merged_names = list(set(read_profile_names))
        merged_names.sort()
        return merged_names

