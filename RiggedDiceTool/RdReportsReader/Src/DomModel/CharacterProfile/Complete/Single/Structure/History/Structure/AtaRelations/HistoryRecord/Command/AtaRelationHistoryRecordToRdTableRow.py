from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Single.Structure.History.Helper.RdTextFormatter import RdTextFormatter


class AtaRelationHistoryRecordToRdTableRow:

    def having(self, related_actor_name, intensity, mission_records, profile_masterlist):

        mission_identifier_composite_string = ""
        for single_record in mission_records:
            mission_identifier_composite_string = mission_identifier_composite_string + self._form_single_identifier_from(single_record)

        mission_identifier_string = mission_identifier_composite_string[:-2]
        return "||" + self._potential_name_link(related_actor_name, profile_masterlist) + "||" + str(intensity) + "||" + mission_identifier_string + "||"

    def _potential_name_link(self, related_actor_name, profile_masterlist):

        if profile_masterlist.contains_profile_name(related_actor_name):
            actor_metadata = profile_masterlist.retrieve_metadata_having_name(related_actor_name)
            actual_actor_identifier = actor_metadata.to_rd_text()
        else:
            actual_actor_identifier = related_actor_name

        return actual_actor_identifier

    def _form_single_identifier_from(self, mission_record):

        mission_date = str(mission_record.parentReport().missionDate())
        mission_link = mission_record.parentReport().link()

        return RdTextFormatter().textToBeHyperlink(mission_link, mission_date) + ", "
