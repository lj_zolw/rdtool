class CharacterMechanics:

    section_collection = None

    def __init__(self, section_collection):
        self.section_collection = section_collection

    def deleteGeneratedSections(self, generated_profile_section_collection):
        self.section_collection.deleteDuplicateSections(generated_profile_section_collection)
        
    def to_rd_text(self):
        return self.section_collection.to_rd_text()
