class PresenceHistoryAggregateReport:

    def aggregate_report(self, mission_presence_records):

        overall_presence_on_missions = "+++++ Postać wystąpiła na: " + str(len(mission_presence_records)) + " misjach."
        aggregate_report = overall_presence_on_missions + "\n" + self._overall_presence_on_campaigns(mission_presence_records) + "\n"
        return aggregate_report

    def _overall_presence_on_campaigns(self, mission_presence_records):

        all_campaign_names = []
        for record in mission_presence_records:
            all_campaign_names.append(record.parent_campaign_name())

        unique_names = list(set(all_campaign_names))

        campaign_string = ""

        for name in unique_names:
            campaign_string += name + ", "

        campaign_string = campaign_string[:-2]

        overall_presence = "+++++ Postać wystąpiła w: " + str(len(unique_names)) + " kampaniach: " + campaign_string
        return overall_presence
