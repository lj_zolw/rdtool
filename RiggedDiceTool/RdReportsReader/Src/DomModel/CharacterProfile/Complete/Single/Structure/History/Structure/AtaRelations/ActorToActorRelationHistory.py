from ...Helper.SortDpRecordList import SortDpRecordList
from .Command.AtaRelationHistoryToProfileSection import AtaRelationHistoryToProfileSection


class ActorToActorRelationHistory:

    ata_relation_history_records = None

    def __init__(self, ata_relation_history_records):
        self.ata_relation_history_records = ata_relation_history_records

    def to_profile_section(self, profile_masterlist):
        self.sort_for_display()
        return AtaRelationHistoryToProfileSection().having(self.ata_relation_history_records, profile_masterlist)

    def sort_for_display(self):
        self.ata_relation_history_records = SortDpRecordList().byIntensityDescendingActorNameAscending(self.ata_relation_history_records)
