from ..AtaRelationHistoryRecord import AtaRelationHistoryRecord


class CreateAtaRelationHistoryRecord:

    def having(self, actor_name, intensity, dp_records):
        history_record = AtaRelationHistoryRecord(actor_name, intensity, dp_records)
        return history_record
