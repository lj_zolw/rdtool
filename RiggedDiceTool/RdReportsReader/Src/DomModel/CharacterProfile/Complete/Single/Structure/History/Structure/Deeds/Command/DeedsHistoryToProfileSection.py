from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Section.Single.Creation.CreateSingleProfileSection import CreateSingleProfileSection


class DeedsHistoryToProfileSection:

    def from_records(self, mission_deed_records):

        headline = "Dokonania:"

        combined_section_text = ""
        for single_record in mission_deed_records:
            combined_section_text = combined_section_text + single_record.to_rd_string() + "\n"

        cleaned_section_text = combined_section_text.strip()

        profile_section = CreateSingleProfileSection().fromHeadlineAndText(headline, cleaned_section_text)
        return profile_section
