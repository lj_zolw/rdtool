from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Section.Single.Creation.CreateSingleProfileSection import CreateSingleProfileSection


class AtaRelationHistoryToProfileSection:

    def having(self, ata_relation_history_records, profile_masterlist):

        headline = "Relacje z postaciami:"

        combined_section_text = "||~ Z kim ||~ Intensywność ||~ Na misjach ||\n"
        for single_record in ata_relation_history_records:
            combined_section_text = combined_section_text + single_record.to_rd_table_row(profile_masterlist) + "\n"

        cleaned_section_text = combined_section_text.strip()

        profile_section = CreateSingleProfileSection().fromHeadlineAndText(headline, cleaned_section_text)
        return profile_section
