from ..MissionPresenceHistoryRecord import MissionPresenceHistoryRecord


class CreateMissionPresenceHistoryRecord:

    def having(self, single_dp_record):

        report = single_dp_record.parentReport()

        mission_date = report.missionDate()
        mission_name = report.missionName()
        mission_seq_no = report.sequence_number()
        link = report.link()
        campaign_name = report.parentCampaign().name()

        record = MissionPresenceHistoryRecord(mission_date, mission_name, mission_seq_no, link, campaign_name)
        return record
