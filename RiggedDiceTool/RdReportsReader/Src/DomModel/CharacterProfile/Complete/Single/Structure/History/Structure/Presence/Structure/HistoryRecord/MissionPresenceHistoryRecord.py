from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Single.Structure.History.Helper.RdTextFormatter import RdTextFormatter


class MissionPresenceHistoryRecord:

    mission_date = None
    mission_name = None
    mission_sequence_number = None
    link = None
    campaign_name = None

    def __init__(self, mission_date, mission_name, mission_seq_number, link, campaign_name):
        self.mission_date = mission_date
        self.mission_name = mission_name
        self.mission_sequence_number = mission_seq_number
        self.link = link
        self.campaign_name = campaign_name

    def sequence_number(self):
        return self.mission_sequence_number

    def parent_campaign_name(self):
        return self.campaign_name

    def to_rd_string(self):
        return "|| " + str(self.mission_date) + " || " + self.mission_sequence_number + " || " + \
               RdTextFormatter().textToBeHyperlink(self.link, self.mission_name) + "||" + self.campaign_name + "||"
