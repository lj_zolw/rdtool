class PresenceHistoryRecordsReport:

    def single_records_report(self, mission_presence_records):

        combined_section_text = "||~ Data ||~ Numer ||~ Misja ||~ W kampanii ||\n"

        for single_record in mission_presence_records:
            combined_section_text = combined_section_text + single_record.to_rd_string() + "\n"

        return combined_section_text
