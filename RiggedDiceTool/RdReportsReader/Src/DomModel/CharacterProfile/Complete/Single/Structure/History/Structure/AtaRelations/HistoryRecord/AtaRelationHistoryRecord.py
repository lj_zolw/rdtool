from ....Helper.SortDpRecordList import SortDpRecordList
from .Command.AtaRelationHistoryRecordToRdTableRow import AtaRelationHistoryRecordToRdTableRow


class AtaRelationHistoryRecord:

    related_actor_name_1 = None
    relation_intensity = None
    dp_mission_records = None

    def __init__(self, related_actor_name, intensity, dp_mission_records):
        self.related_actor_name_1 = related_actor_name
        self.relation_intensity = intensity
        self.dp_mission_records = dp_mission_records

    def intensity(self):
        return self.relation_intensity

    def related_actor_name(self):
        return self.related_actor_name_1

    def to_rd_table_row(self, profile_masterlist):
        self.sort_for_display()
        return AtaRelationHistoryRecordToRdTableRow().having(self.related_actor_name_1, self.relation_intensity, self.dp_mission_records, profile_masterlist)

    def sort_for_display(self):
        self.dp_mission_records = SortDpRecordList().bySequenceNumber(self.dp_mission_records)
