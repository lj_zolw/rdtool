from .Command.DeedsHistoryToProfileSection import DeedsHistoryToProfileSection
from ...Helper.SortDpRecordList import SortDpRecordList


class MissionDeedsHistory:

    mission_deed_records = None

    def __init__(self, mission_deeds):
        self.mission_deed_records = mission_deeds

    def to_profile_section(self):
        self.sort_for_display()
        return DeedsHistoryToProfileSection().from_records(self.mission_deed_records)

    def sort_for_display(self):
        self.mission_deed_records = SortDpRecordList().bySequenceNumber(self.mission_deed_records)
