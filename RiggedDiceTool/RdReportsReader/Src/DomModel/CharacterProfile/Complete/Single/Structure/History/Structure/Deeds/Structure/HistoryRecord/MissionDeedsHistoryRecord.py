from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Single.Structure.History.Helper.RdTextFormatter import RdTextFormatter


class MissionDeedsHistoryRecord:

    mission_deed = None
    mission_sequence_number = None
    link = None
    mission_name = None

    def __init__(self, mission_deed, mission_sequence_number, link, mission_name):
        self.mission_deed = mission_deed
        self.mission_sequence_number = mission_sequence_number
        self.link = link
        self.mission_name = mission_name

    def sequence_number(self):
        return self.mission_sequence_number

    def to_rd_string(self):
        return "|| " + RdTextFormatter().textToBeHyperlink(self.link, self.mission_name) + " || " + self.mission_deed + " ||"
