from ..ActorToActorRelationHistory import ActorToActorRelationHistory
from ..HistoryRecord.Creation.CreateAtaRelationHistoryRecord import CreateAtaRelationHistoryRecord


class CreateAtaRelationHistory:

    def having(self, generated_profile):

        relation_map = generated_profile.allActorToActorRelations()
        names = relation_map.keys()

        ata_relation_history_records = []
        for singleName in names:
            relation_with_actor = relation_map[singleName]

            intensity = relation_with_actor.intensity()
            dp_records = relation_with_actor.dp_records()

            single_historical_relation_record = CreateAtaRelationHistoryRecord().having(singleName, intensity, dp_records)
            ata_relation_history_records.append(single_historical_relation_record)

        return ActorToActorRelationHistory(ata_relation_history_records)

    def empty(self):
        return ActorToActorRelationHistory([])
