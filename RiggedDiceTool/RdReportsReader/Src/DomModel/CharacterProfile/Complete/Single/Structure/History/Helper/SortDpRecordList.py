class SortDpRecordList:

    def bySequenceNumber(self, record_list):
        sorted_list = sorted(record_list, key=lambda record: record.sequence_number())
        return sorted_list

    def byIntensityDescendingActorNameAscending(self, record_list):
        sorted_list = sorted(record_list, key=lambda record: (-record.intensity(), record.related_actor_name()))
        return sorted_list
