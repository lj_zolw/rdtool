from ..CharacterMechanics import CharacterMechanics
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Section.Collection.Creation.CreateProfileSectionCollection import CreateProfileSectionCollection


class CreateCharacterMechanics:

    def empty_mechanics(self):
        section_collection = CreateProfileSectionCollection().empty()
        return CharacterMechanics(section_collection)

    def from_profile(self, read_profile):
        return CharacterMechanics(read_profile.sectionCollection())
