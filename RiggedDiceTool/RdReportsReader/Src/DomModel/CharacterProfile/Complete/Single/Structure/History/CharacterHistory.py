from .Command.CharHistoryToProfileSections import CharHistoryToProfileSections


class CharacterHistory:

    mission_presence = None
    mission_deeds = None
    ata_relation_history = None

    def __init__(self, mission_presence, mission_deeds, actor_to_actor_relation_history):
        self.mission_presence = mission_presence
        self.mission_deeds = mission_deeds
        self.ata_relation_history = actor_to_actor_relation_history

    def to_rd_text(self, profile_masterlist):
        return self.to_profile_sections(profile_masterlist).to_rd_text()

    def to_profile_sections(self, profile_masterlist):
        return CharHistoryToProfileSections().having(self.mission_presence, self.mission_deeds, self.ata_relation_history, profile_masterlist)
