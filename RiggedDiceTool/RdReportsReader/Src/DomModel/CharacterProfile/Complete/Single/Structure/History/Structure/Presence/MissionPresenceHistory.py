from ...Helper.SortDpRecordList import SortDpRecordList
from .Command.PresenceHistoryToProfileSection import PresenceHistoryToProfileSection


class MissionPresenceHistory:

    mission_presence_records = None

    def __init__(self, mission_presences):
        self.mission_presence_records = mission_presences

    def to_profile_section(self):
        self.sort_for_display()
        return PresenceHistoryToProfileSection().having(self.mission_presence_records)

    def sort_for_display(self):
        self.mission_presence_records = SortDpRecordList().bySequenceNumber(self.mission_presence_records)
