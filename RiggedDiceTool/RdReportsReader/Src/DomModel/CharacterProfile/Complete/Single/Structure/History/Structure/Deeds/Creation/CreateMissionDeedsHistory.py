from ..MissionDeedsHistory import MissionDeedsHistory
from ..Structure.HistoryRecord.Creation.CreateMissionDeedsHistoryRecord import CreateMissionDeedsHistoryRecord


class CreateMissionDeedsHistory:

    def having(self, generated_profile):

        mission_records = generated_profile.allDpMissionRecords()

        mission_deeds = []
        for singleRecord in mission_records:
            mission_deed_record = CreateMissionDeedsHistoryRecord().having(singleRecord)
            mission_deeds.append(mission_deed_record)

        presence_history = MissionDeedsHistory(mission_deeds)
        return presence_history

    def empty(self):
        return MissionDeedsHistory([])
