from ..MissionDeedsHistoryRecord import MissionDeedsHistoryRecord


class CreateMissionDeedsHistoryRecord:

    def having(self, single_dp_record):

        report = single_dp_record.parentReport()

        mission_deed = single_dp_record.missionDeed()
        mission_name = report.missionName()
        mission_sequence_number = report.sequence_number()
        link = report.link()

        record = MissionDeedsHistoryRecord(mission_deed, mission_sequence_number, link, mission_name)
        return record
