from ..MissionPresenceHistory import MissionPresenceHistory
from ..Structure.HistoryRecord.Creation.CreateMissionPresenceHistoryRecord import CreateMissionPresenceHistoryRecord


class CreateMissionPresenceHistory:

    def having(self, generated_profile):

        mission_records = generated_profile.allDpMissionRecords()

        mission_presences = []
        for single_record in mission_records:
            single_presence = CreateMissionPresenceHistoryRecord().having(single_record)
            mission_presences.append(single_presence)

        return MissionPresenceHistory(mission_presences)

    def empty(self):
        return MissionPresenceHistory([])
