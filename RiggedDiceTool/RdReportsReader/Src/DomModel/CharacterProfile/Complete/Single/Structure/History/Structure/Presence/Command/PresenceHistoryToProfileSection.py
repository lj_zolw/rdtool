from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Section.Single.Creation.CreateSingleProfileSection import CreateSingleProfileSection
from .Subcommand_HistoryToProfileSection.PresenceHistoryRecordsReport import PresenceHistoryRecordsReport
from .Subcommand_HistoryToProfileSection.PresenceHistoryAggregateReport import PresenceHistoryAggregateReport


class PresenceHistoryToProfileSection:

    def having(self, mission_presence_records):

        headline = "Na misjach:"

        overall_report_text = PresenceHistoryAggregateReport().aggregate_report(mission_presence_records)
        presence_report_text = PresenceHistoryRecordsReport().single_records_report(mission_presence_records)
        cleaned_presence_section_text = presence_report_text.strip()

        cleaned_section_text = overall_report_text + "\n" + cleaned_presence_section_text

        profile_section = CreateSingleProfileSection().fromHeadlineAndText(headline, cleaned_section_text)
        return profile_section
