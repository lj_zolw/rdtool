from ..CharacterHistory import CharacterHistory
from ..Structure.Deeds.Creation.CreateMissionDeedsHistory import CreateMissionDeedsHistory
from ..Structure.AtaRelations.Creation.CreateAtaRelationHistory import CreateAtaRelationHistory
from ..Structure.Presence.Creation.CreateMissionPresenceHistory import CreateMissionPresenceHistory


class CreateCharacterHistory:

    def empty_history(self):

        mission_presence = CreateMissionPresenceHistory().empty()
        mission_deeds = CreateMissionDeedsHistory().empty()
        actor_to_actor_relation_map = CreateAtaRelationHistory().empty()

        history = CharacterHistory(mission_presence, mission_deeds, actor_to_actor_relation_map)
        return history

    def from_profile(self, generated_profile):

        mission_presence = CreateMissionPresenceHistory().having(generated_profile)
        mission_deeds = CreateMissionDeedsHistory().having(generated_profile)
        actor_to_actor_relation_map = CreateAtaRelationHistory().having(generated_profile)

        history = CharacterHistory(mission_presence, mission_deeds, actor_to_actor_relation_map)
        return history
