from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Section.Collection.Creation.CreateProfileSectionCollection import CreateProfileSectionCollection


class CharHistoryToProfileSections:

    def having(self, mission_presence, mission_deeds, ata_relation_history, profile_masterlist):

        presence_section = mission_presence.to_profile_section()
        deeds_section = mission_deeds.to_profile_section()
        ata_relation_section = ata_relation_history.to_profile_section(profile_masterlist)

        history_section_list = [presence_section, deeds_section, ata_relation_section]

        section_collection = CreateProfileSectionCollection().from_section_list(history_section_list)
        return section_collection
