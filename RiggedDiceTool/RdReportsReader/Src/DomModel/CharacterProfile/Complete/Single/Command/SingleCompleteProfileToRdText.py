class SingleCompleteProfileToRdText:

    def having(self, character_name, mechanics, history, profile_masterlist):

        name_rd_text = "+ " + character_name
        mechanics_rd_text = mechanics.to_rd_text().strip()
        history_rd_text = history.to_rd_text(profile_masterlist).strip()

        complete_profile_rd_text = name_rd_text + "\n\n" + mechanics_rd_text + "\n\n" + history_rd_text
        return complete_profile_rd_text
