from .Command.SingleCompleteProfileToRdText import SingleCompleteProfileToRdText


class SingleCompleteProfile:

    character_name = None
    mechanics = None
    history = None
    profile_masterlist = None

    def __init__(self, character_name, character_mechanics, character_history, profile_masterlist):
        self.character_name = character_name
        self.mechanics = character_mechanics
        self.history = character_history
        self.profile_masterlist = profile_masterlist

    def characterName(self):
        return self.character_name

    def to_rd_text(self):
        return SingleCompleteProfileToRdText().having(self.character_name, self.mechanics, self.history, self.profile_masterlist)
