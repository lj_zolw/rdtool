from ...Structure.History.Creation.CreateCharacterHistory import CreateCharacterHistory


class CreateCharacterHistorySubfactory:

    def fromNullable(self, generated_profile):

        if generated_profile is None:
            characterHistory = CreateCharacterHistory().empty_history()
        else:
            characterHistory = CreateCharacterHistory().from_profile(generated_profile)

        return characterHistory
