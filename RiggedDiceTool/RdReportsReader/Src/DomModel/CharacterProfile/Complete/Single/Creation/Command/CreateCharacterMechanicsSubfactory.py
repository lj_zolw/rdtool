from ...Structure.Mechanics.Creation.CreateCharacterMechanics import CreateCharacterMechanics


class CreateCharacterMechanicsSubfactory:

    def fromNullable(self, read_profile):

        if read_profile is None:
            character_mechanics = CreateCharacterMechanics().empty_mechanics()
        else:
            character_mechanics = CreateCharacterMechanics().from_profile(read_profile)

        return character_mechanics
