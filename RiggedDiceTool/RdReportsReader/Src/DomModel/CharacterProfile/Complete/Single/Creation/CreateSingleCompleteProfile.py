from .Command.CreateCharacterHistorySubfactory import CreateCharacterHistorySubfactory
from .Command.CreateCharacterMechanicsSubfactory import CreateCharacterMechanicsSubfactory
from ..SingleCompleteProfile import SingleCompleteProfile


class CreateSingleCompleteProfile:

    def having(self, character_name, read_profile, generated_profile, profile_masterlist):

        character_history = CreateCharacterHistorySubfactory().fromNullable(generated_profile)
        character_mechanics = CreateCharacterMechanicsSubfactory().fromNullable(read_profile)

        generated_profile_sections = character_history.to_profile_sections(profile_masterlist)
        character_mechanics.deleteGeneratedSections(generated_profile_sections)

        complete_profile = SingleCompleteProfile(character_name, character_mechanics, character_history, profile_masterlist)
        return complete_profile
