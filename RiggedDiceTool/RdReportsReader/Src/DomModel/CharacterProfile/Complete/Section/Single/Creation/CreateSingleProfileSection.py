import re

from ..SingleProfileSection import SingleProfileSection


class CreateSingleProfileSection:

    def from_section_text(self, section_text_candidate):

        # This magical regex means 'first line'
        headline_list = re.findall('^.+', section_text_candidate)

        if len(headline_list) > 0:
            headline = headline_list[0]
        else:
            headline = ""

        section_text = section_text_candidate.replace(headline, '').strip()

        single_profile_section = self.fromHeadlineAndText(headline, section_text)
        return single_profile_section

    def fromHeadlineAndText(self, headline, section_text):

        single_profile_section = SingleProfileSection(headline, section_text)
        return single_profile_section
