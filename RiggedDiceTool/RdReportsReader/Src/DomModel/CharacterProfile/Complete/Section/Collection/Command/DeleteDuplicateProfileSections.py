class DeleteDuplicateProfileSections:

    def returnDeduplicated(self, left_list, right_list):

        new_list = left_list[:]
        for potentially_duplicate_section in right_list:
            for core_section in left_list:
                if potentially_duplicate_section.headline() == core_section.headline():
                    new_list.remove(core_section)
                    break

        return new_list
