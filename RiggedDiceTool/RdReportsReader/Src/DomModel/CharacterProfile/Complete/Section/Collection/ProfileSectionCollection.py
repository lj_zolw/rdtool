from .Command.DeleteDuplicateProfileSections import DeleteDuplicateProfileSections
from .Command.ProfileSectionCollectionToRdText import ProfileSectionCollectionToRdText


class ProfileSectionCollection:

    profile_sections = None

    def __init__(self, profile_sections):
        self.profile_sections = profile_sections

    def profileSectionCollection(self):
        return self.profile_sections

    def deleteDuplicateSections(self, other_profile_section_collection):
        self.profile_sections = DeleteDuplicateProfileSections().returnDeduplicated(self.profileSectionCollection(), other_profile_section_collection.profileSectionCollection())

    def to_rd_text(self):
        return ProfileSectionCollectionToRdText().fromSections(self.profile_sections)
