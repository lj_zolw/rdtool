class ProfileSectionCollectionToRdText:

    def fromSections(self, profile_sections):

        plain_text = ""
        for singleSection in profile_sections:
            plain_text = plain_text + singleSection.to_rd_text()

        return plain_text
