import re

from ..ProfileSectionCollection import ProfileSectionCollection
from ...Single.Creation.CreateSingleProfileSection import CreateSingleProfileSection


class CreateProfileSectionCollection:

    def from_text(self, rd_report_text_without_name):

        profile_sections = []
        # text_section_prototypes = rd_report_text_without_name.split('^\+\+\+\s')
        text_section_prototypes = re.split('\n\+\+\+\s', rd_report_text_without_name)

        for sectionTextCandidate in text_section_prototypes:
            if sectionTextCandidate.strip():
                single_profile_section = CreateSingleProfileSection().from_section_text(sectionTextCandidate)

                if not single_profile_section.isEmpty():
                    profile_sections.append(single_profile_section)

        section_collection = self.from_section_list(profile_sections)
        return section_collection

    def empty(self):
        profile_sections = []
        section_collection = self.from_section_list(profile_sections)
        return section_collection

    def from_section_list(self, section_list):
        section_collection = ProfileSectionCollection(section_list)
        return section_collection
