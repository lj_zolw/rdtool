from ..SingleReadProfile import SingleReadProfile
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Section.Collection.Creation.CreateProfileSectionCollection import CreateProfileSectionCollection


class CreateSingleReadProfile:

    def createFromRdProfileMasterlist(self, rd_profile, masterlist):

        character_name = rd_profile.characterName()
        nameless_rd_profile_text = self._profile_text_without_name(rd_profile, character_name)
        profile_sections = CreateProfileSectionCollection().from_text(nameless_rd_profile_text)

        single_profile = SingleReadProfile(character_name, profile_sections)
        return single_profile

    def _profile_text_without_name(self, read_rd_profile, character_name):
        text = read_rd_profile.text()
        result = text.replace("+ " + character_name, '')
        return result
