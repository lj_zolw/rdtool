from RdReportsReader.Src.DomModel.CharacterProfile.CommonTools.ProfileListDataExtractor import ProfileListDataExtractor

class ReadProfileCollection:

    def __init__(self, profiles):
        self.profiles = profiles

    def count(self):
        return len(self.profiles)

    def allNames(self):
        return ProfileListDataExtractor().extractAllNamesFromCollection(self.profiles)
        
    def getProfileByName(self, single_name):
        return ProfileListDataExtractor().extractSelectedProfileFromCollectionBy(single_name, self.profiles)
