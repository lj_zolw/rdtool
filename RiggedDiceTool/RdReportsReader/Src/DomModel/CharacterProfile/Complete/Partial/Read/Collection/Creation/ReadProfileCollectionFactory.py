from ..ReadProfileCollection import ReadProfileCollection
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Partial.Read.Single.Creation.CreateSingleReadProfile import CreateSingleReadProfile

class ReadProfileCollectionFactory:

    def createFrom(self, read_profiles_subset):

        single_read_profile_factory = CreateSingleReadProfile()
        profiles = []

        for rd_profile in read_profiles_subset:
            single_read_profile = single_read_profile_factory.createFromRdProfileMasterlist(rd_profile)
            profiles.append(single_read_profile)

        return ReadProfileCollection(profiles)

    def createFromRdProfilesMasterlist(self, rd_profiles, masterlist):

        single_read_profile_factory = CreateSingleReadProfile()
        profiles = []

        for rd_profile in rd_profiles:
            single_profile = single_read_profile_factory.createFromRdProfileMasterlist(rd_profile, masterlist)
            profiles.append(single_profile)

        return ReadProfileCollection(profiles)
