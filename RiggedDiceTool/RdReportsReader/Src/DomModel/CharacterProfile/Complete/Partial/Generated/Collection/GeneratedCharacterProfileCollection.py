from RdReportsReader.Src.DomModel.CharacterProfile.CommonTools.ProfileListDataExtractor import ProfileListDataExtractor


class GeneratedCharacterProfileCollection:

    generated_profiles = None

    def __init__(self, generated_profiles):
        self.generated_profiles = generated_profiles

    def count(self):
        return len(self.generated_profiles)

    def allNames(self):
        return ProfileListDataExtractor().extractAllNamesFromCollection(self.generated_profiles)

    def getProfileByName(self, single_name):
        return ProfileListDataExtractor().extractSelectedProfileFromCollectionBy(single_name, self.generated_profiles)
