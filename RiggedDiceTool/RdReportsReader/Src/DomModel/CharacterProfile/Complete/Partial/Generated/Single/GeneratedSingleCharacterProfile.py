class GeneratedSingleCharacterProfile:

    actor_name = None
    dp_records_collection = None
    ata_relations_map = None

    def __init__(self, actorName, dpRecordsCollection, ataRelationsMap):
        self.actor_name = actorName
        self.dp_records_collection = dpRecordsCollection
        self.ata_relations_map = ataRelationsMap

    def name(self):
        return self.actor_name

    def allDpMissionRecords(self):
        return self.dp_records_collection.all_records()

    def allActorToActorRelations(self):
        return self.ata_relations_map.actor_to_actor_relations()
