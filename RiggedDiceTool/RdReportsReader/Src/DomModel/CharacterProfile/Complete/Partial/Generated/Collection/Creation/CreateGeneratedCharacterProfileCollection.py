from ...Single.Creation.CreateSingleGeneratedCharacterProfile import CreateSingleGeneratedCharacterProfile
from ..GeneratedCharacterProfileCollection import GeneratedCharacterProfileCollection


class CreateGeneratedCharacterProfileCollection:

    def forActorsFromSources(self, actor_names, actor_to_dp_records_collection, actor_to_actor_relation_map_collection):

        create_single_generated_profile = CreateSingleGeneratedCharacterProfile()

        generated_profiles = []
        for actorName in actor_names:

            their_dp_records_collection = actor_to_dp_records_collection.get_for_single_actor(actorName)
            their_ata_relations_map = actor_to_actor_relation_map_collection.get_for_single_actor(actorName)

            single_profile = create_single_generated_profile.knowing(actorName, their_dp_records_collection, their_ata_relations_map)
            generated_profiles.append(single_profile)

        generated_profile_collection = GeneratedCharacterProfileCollection(generated_profiles)
        return generated_profile_collection
