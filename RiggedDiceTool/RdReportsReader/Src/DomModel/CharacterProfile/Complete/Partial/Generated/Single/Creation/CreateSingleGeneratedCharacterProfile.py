from ..GeneratedSingleCharacterProfile import GeneratedSingleCharacterProfile


class CreateSingleGeneratedCharacterProfile:

    def knowing(self, actor_name, dp_records_collection, ata_relations_map):
        profile = GeneratedSingleCharacterProfile(actor_name, dp_records_collection, ata_relations_map)
        return profile