class SingleProfileMetadata:

    name = ""
    weblink = ""
    category = ""
    trivia = ""

    def __init__(self, name, weblink, category, trivia):
        self.name = name
        self.weblink = weblink
        self.category = category
        self.trivia = trivia

    def ziplink(self):
        return self.weblink.replace(":", "_")

    def characterName(self):
        return self.name

    def to_rd_text(self):
        return "[[[" + self.weblink + "|" + self.name + "]]]"
