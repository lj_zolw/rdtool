import re


class IsProfileMetadataRecordValid:

    def checkCandidate(self, metadata_candidate):

        pattern = "...:\s\[\[\[.+:.+\]\]\]"
        valid = re.search(pattern, metadata_candidate)

        if valid is None:
            return False
        else:
            return True
