import re

from RdReportsReader.Src.DomModel.CharacterProfile.Metadata.Single.SingleProfileMetadata import SingleProfileMetadata


class CreateSingleProfileMetadata:

    def fromRawRdMetadataRecord(self, metadata_candidate):

        stripped = metadata_candidate.strip()

        pattern = '(\[\[\[)(.+)(\|)(.+)(\]\]\])(.*)'
        regexSplit = re.search(pattern, stripped)

        category = stripped[0:3]
        weblink = regexSplit.group(2)
        name = regexSplit.group(4)
        trivia = regexSplit.group(6)

        return SingleProfileMetadata(name, weblink, category, trivia)
