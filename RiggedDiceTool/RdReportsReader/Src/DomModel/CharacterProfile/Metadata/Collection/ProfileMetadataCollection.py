class ProfileMetadataCollection:

    metadata_collection = None

    def __init__(self, metadata_collection):
        self.metadata_collection = metadata_collection

    def profiles_count(self):
        return len(self.metadata_collection)

    def all_metadata(self):
        return self.metadata_collection

    def contains_profile_name(self, profile_name):
        return any(single for single in self.metadata_collection if single.characterName() == profile_name)

    def retrieve_single_metadata_having_name(self, profile_name):
        return [single for single in self.metadata_collection if single.characterName() == profile_name][0]
