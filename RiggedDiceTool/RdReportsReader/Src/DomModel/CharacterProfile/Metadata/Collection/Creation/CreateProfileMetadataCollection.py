from ...Single.Creation.CreateSingleProfileMetadata import CreateSingleProfileMetadata
from ...Single.Command.IsProfileMetadataRecordValid import IsProfileMetadataRecordValid
from ..ProfileMetadataCollection import ProfileMetadataCollection


class CreateProfileMetadataCollection:

    def from_text_block(self, metadata_text_block):

        metadata_collection = []
        split_text_block = metadata_text_block.split("\n")
        for metadata_candidate in split_text_block:

            if IsProfileMetadataRecordValid().checkCandidate(metadata_candidate):
                profile_metadata = CreateSingleProfileMetadata().fromRawRdMetadataRecord(metadata_candidate)
                metadata_collection.append(profile_metadata)

        return ProfileMetadataCollection(metadata_collection)
