from ..ProfileMasterlistTail import ProfileMasterlistTail


class CreateProfileMasterlistTail:

    def from_text(self, profile_masterlist_text):

        start_of_tail_index = profile_masterlist_text.index("++++ Koniec kart postaci Inwazji")

        tail_text = profile_masterlist_text[start_of_tail_index: len(profile_masterlist_text)]
        tail = ProfileMasterlistTail(tail_text.strip())

        return tail
