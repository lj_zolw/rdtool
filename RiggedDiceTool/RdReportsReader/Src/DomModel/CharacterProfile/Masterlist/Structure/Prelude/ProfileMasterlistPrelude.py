class ProfileMasterlistPrelude:

    prelude_text = ""

    def __init__(self, prelude_text):
        self.prelude_text = prelude_text

    def to_rd_text(self):
        return self.prelude_text
