from ..ProfileMasterlistPrelude import ProfileMasterlistPrelude


class CreateProfileMasterlistPrelude:

    def from_text(self, profile_masterlist_text):

        prelude_core_delimiter = "+ Karty postaci Inwazji:"
        start_of_core_index = profile_masterlist_text.index(prelude_core_delimiter)

        prelude_text = profile_masterlist_text[0: start_of_core_index]
        prelude = ProfileMasterlistPrelude(prelude_text.strip())

        return prelude
