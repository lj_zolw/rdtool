from .Command.CountProfileMasterlistFactionMetadata import CountProfileMasterlistFactionMetadata
from .Command.ProfileMasterlistFactionContainsMetadata import ProfileMasterlistFactionContainsMetadata
from .Command.RetrieveProfileMasterlistFactionMetadata import RetrieveProfileMasterlistFactionMetadata


class ProfileMasterlistFaction:

    name = None
    metadata = None
    subfactions = None

    def __init__(self, name, metadata, subfactions):
        self.name = name
        self.metadata_collection = metadata
        self.subfactions = subfactions

    def metadata_count(self):
        return CountProfileMasterlistFactionMetadata().including_subfactions(self.metadata_collection, self.subfactions)

    def all_metadata(self):
        return RetrieveProfileMasterlistFactionMetadata().including_subfactions(self.metadata_collection, self.subfactions)

    def containsMetadataWithName(self, profile_name):
        return ProfileMasterlistFactionContainsMetadata().having_name(self.metadata_collection, self.subfactions, profile_name)

    def retrieve_metadata_having_name(self, profile_name):
        return RetrieveProfileMasterlistFactionMetadata().metadata_having_name(self.metadata_collection, self.subfactions, profile_name)
