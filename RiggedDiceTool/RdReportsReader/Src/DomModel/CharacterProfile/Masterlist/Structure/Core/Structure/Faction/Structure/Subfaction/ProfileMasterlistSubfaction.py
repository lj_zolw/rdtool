class ProfileMasterlistSubfaction:

    name = None
    metadata_collection = None

    def __init__(self, name, metadata_collection):
        self.name = name
        self.metadataCollection = metadata_collection

    def metadata_count(self):
        return self.metadataCollection.profiles_count()

    def all_metadata(self):
        return self.metadataCollection.all_metadata()

    def containsMetadataWithName(self, profile_name):
        return self.metadataCollection.contains_profile_name(profile_name)

    def retrieve_metadata_having_name(self, profile_name):
        return self.metadataCollection.retrieve_single_metadata_having_name(profile_name)
