import re

from ..Structure.Faction.Creation.CreateProfileMasterlistFaction import CreateProfileMasterlistFaction
from ..ProfileMasterlistCore import ProfileMasterlistCore


class CreateProfileMasterlistCore:

    def from_text(self, profile_masterlist_text):

        core_section_text = self._extract_relevant_text_from(profile_masterlist_text)
        factions_texts = self._split_into_faction_text(core_section_text)
        factions = self._create_factions(factions_texts)
        core = ProfileMasterlistCore(factions)
        return core

    def _create_factions(self, factions_texts):

        factions = []
        for factionText in factions_texts:
            name = factionText.split("\n")[0].strip()
            remainder = factionText.replace(name, "").strip()
            factions.append(CreateProfileMasterlistFaction().fromNameAndText(name, remainder))

        return factions

    def _split_into_faction_text(self, core_section_text):

        faction_header_pattern = '[^\+]\+\+\+\s'
        faction_first_header_pattern = '^\+\+\+\s'
        faction_text_blocks = re.split(faction_header_pattern, core_section_text)
        non_empty_faction_texts = [ft for ft in faction_text_blocks if ft is not False]

        cleared_faction_text_blocks = self._clear_factions_from_header(faction_first_header_pattern, faction_header_pattern, non_empty_faction_texts)
        return cleared_faction_text_blocks

    def _extract_relevant_text_from(self, masterlist_wikidot_text):

        beginning_index = masterlist_wikidot_text.index("+ Karty postaci Inwazji:") + len("+ Karty postaci Inwazji:")
        ending_index = masterlist_wikidot_text.index("++++ Koniec kart postaci Inwazji")
        core_text = masterlist_wikidot_text[beginning_index: ending_index]
        return core_text.strip()

    def _clear_factions_from_header(self, faction_first_header_pattern, faction_general_header_pattern, non_empty_faction_texts):
        cleared_faction_text_blocks = []
        for block in non_empty_faction_texts:
            first_sub = re.sub(faction_general_header_pattern, "", block)
            second_sub = re.sub(faction_first_header_pattern, "", first_sub)
            cleared_faction_text_blocks.append(second_sub)
        return cleared_faction_text_blocks