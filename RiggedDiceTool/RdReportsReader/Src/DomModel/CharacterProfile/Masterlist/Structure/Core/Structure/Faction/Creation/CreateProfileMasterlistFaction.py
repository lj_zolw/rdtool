from ..ProfileMasterlistFaction import ProfileMasterlistFaction
from ..Structure.Subfaction.Creation.CreateProfileMasterlistSubfaction import CreateProfileMasterlistSubfaction
from RdReportsReader.Src.DomModel.CharacterProfile.Metadata.Collection.Creation.CreateProfileMetadataCollection import CreateProfileMetadataCollection
from RdReportsReader.Src.DomModel.CharacterProfile.Metadata.Single.Command.IsProfileMetadataRecordValid import IsProfileMetadataRecordValid

class CreateProfileMasterlistFaction:

    def fromNameAndText(self, name, text):

        subfactionless_text = self.extractSubfactionlessText(text)
        subfactions_texts = self.extractSubfactionTexts(text)

        subfactions = self.createSubfactionsFrom(subfactions_texts)
        direct_metadata = self.createMetadataFrom(subfactionless_text)

        faction = ProfileMasterlistFaction(name, direct_metadata, subfactions)
        return faction

    def createMetadataFrom(self, metadata_text_block):
        return CreateProfileMetadataCollection().from_text_block(metadata_text_block)

    def createSubfactionsFrom(self, subfactions_texts):

        subfactions = []
        for subfaction_text in subfactions_texts:
            if subfaction_text:
                name = subfaction_text.split("\n")[0].strip()
                metadata_text_block = subfaction_text.replace(name, "").strip()
                subfactions.append(CreateProfileMasterlistSubfaction().fromNameAndText(name, metadata_text_block))

        return subfactions

    def extractSubfactionTexts(self, text):

        relevant_texts = []
        if "+++++" in text:
            split_text = text.split("+++++")
            if not IsProfileMetadataRecordValid().checkCandidate(split_text[0]):
                relevant_texts = split_text
            else:
                relevant_texts = split_text[1:]

        return relevant_texts

    def extractSubfactionlessText(self, text):

        relevant_text = ""

        if "+++++" in text:
            split_text = text.split("+++++")
            relevant_text = split_text[0].strip()
        else:
            relevant_text = text.strip()

        return relevant_text
