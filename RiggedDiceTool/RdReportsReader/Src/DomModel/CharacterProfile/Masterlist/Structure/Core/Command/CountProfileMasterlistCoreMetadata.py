class CountProfileMasterlistCoreMetadata:

    def in_factions(self, factions):

        metadata_count = 0
        for faction in factions:
            metadata_count = metadata_count + faction.metadata_count()

        return metadata_count
