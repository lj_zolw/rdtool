class CountProfileMasterlistFactionMetadata:

    def including_subfactions(self, metadata_collection, subfactions):

        metadata_count = metadata_collection.profiles_count()

        for subfaction in subfactions:
            metadata_count = metadata_count + subfaction.metadata_count()

        return metadata_count
