from ..ProfileMasterlistSubfaction import ProfileMasterlistSubfaction
from RdReportsReader.Src.DomModel.CharacterProfile.Metadata.Collection.Creation.CreateProfileMetadataCollection import CreateProfileMetadataCollection


class CreateProfileMasterlistSubfaction:

    def fromNameAndText(self, name, metadata_text_block):
        metadata_collection = CreateProfileMetadataCollection().from_text_block(metadata_text_block)
        subfaction = ProfileMasterlistSubfaction(name, metadata_collection)
        return subfaction
