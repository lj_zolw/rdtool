class ProfileMasterlistFactionContainsMetadata:

    def having_name(self, metadata_collection, subfactions, profile_name):

        if metadata_collection.contains_profile_name(profile_name):
            return True

        for single_subfaction in subfactions:
            if single_subfaction.containsMetadataWithName(profile_name):
                return True

        return False
