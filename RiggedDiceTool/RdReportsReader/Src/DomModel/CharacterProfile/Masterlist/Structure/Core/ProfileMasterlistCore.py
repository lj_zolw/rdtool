from .Command.CountProfileMasterlistCoreMetadata import CountProfileMasterlistCoreMetadata
from .Command.ProfileMasterlistCoreContainsMetadata import ProfileMasterlistCoreContainsMetadata
from .Command.RetrieveProfileMasterlistCoreMetadata import RetrieveProfileMasterlistCoreMetadata


class ProfileMasterlistCore:

    factions = None

    def __init__(self, factions):
        self.factions = factions

    def metadata_count(self):
        return CountProfileMasterlistCoreMetadata().in_factions(self.factions)

    def contains_profile_name(self, profile_name):
        return ProfileMasterlistCoreContainsMetadata().with_name(self.factions, profile_name)

    def all_metadata(self):
        return RetrieveProfileMasterlistCoreMetadata().all_from_factions(self.factions)

    def retrieve_metadata_having_name(self, profile_name):
        return RetrieveProfileMasterlistCoreMetadata().having_name(self.factions, profile_name)
