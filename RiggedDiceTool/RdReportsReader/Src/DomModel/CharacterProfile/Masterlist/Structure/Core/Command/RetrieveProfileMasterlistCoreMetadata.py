class RetrieveProfileMasterlistCoreMetadata:

    def all_from_factions(self, faction_collection):

        metadata = []
        for faction in faction_collection:
            metadata = metadata + faction.all_metadata()

        return metadata

    def having_name(self, faction_collection, profile_name):

        for faction in faction_collection:
            if faction.containsMetadataWithName(profile_name):
                return faction.retrieve_metadata_having_name(profile_name)

        return None
