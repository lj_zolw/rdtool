class RetrieveProfileMasterlistFactionMetadata:

    def including_subfactions(self, metadata_collection, subfactions):

        metadata = metadata_collection.all_metadata()
        for subfaction in subfactions:
            metadata = metadata + subfaction.all_metadata()

        return metadata

    def metadata_having_name(self, metadataCollection, subfactions, profileName):

        if metadataCollection.contains_profile_name(profileName):
           return metadataCollection.retrieve_single_metadata_having_name(profileName)

        for single_subfaction in subfactions:
            if single_subfaction.containsMetadataWithName(profileName):
                return single_subfaction.retrieve_metadata_having_name(profileName)

        raise "No metadata with name " + profileName + "present."
