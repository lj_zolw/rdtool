class ProfileMasterlistCoreContainsMetadata:

    def with_name(self, factions, profileName):

        contains = False
        for faction in factions:
            if faction.containsMetadataWithName(profileName):
                contains = True
                break

        return contains
