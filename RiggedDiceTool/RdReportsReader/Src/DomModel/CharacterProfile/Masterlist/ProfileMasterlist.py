class ProfileMasterlist:

    def __init__(self, prelude, core, tail):
        self.prelude = prelude
        self.core = core
        self.tail = tail

    def metadata_count(self):
        return self.core.metadata_count()

    def all_metadata(self):
        return self.core.all_metadata()

    def contains_profile_name(self, profile_name):
        return self.core.contains_profile_name(profile_name)

    def retrieve_metadata_having_name(self, profile_name):
        return self.core.retrieve_metadata_having_name(profile_name)
