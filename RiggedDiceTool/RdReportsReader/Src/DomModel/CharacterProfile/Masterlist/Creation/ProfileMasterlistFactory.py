from ..ProfileMasterlist import ProfileMasterlist
from ..Structure.Prelude.Creation.CreateProfileMasterlistPrelude import CreateProfileMasterlistPrelude
from ..Structure.Core.Creation.CreateProfileMasterlistCore import CreateProfileMasterlistCore
from ..Structure.Tail.Creation.CreateProfileMasterlistTail import CreateProfileMasterlistTail


class ProfileMasterlistFactory:

    def createFromText(self, profile_masterlist_text):

        prelude = CreateProfileMasterlistPrelude().from_text(profile_masterlist_text)
        core = CreateProfileMasterlistCore().from_text(profile_masterlist_text)
        tail = CreateProfileMasterlistTail().from_text(profile_masterlist_text)

        masterlist = ProfileMasterlist(prelude, core, tail)
        return masterlist
