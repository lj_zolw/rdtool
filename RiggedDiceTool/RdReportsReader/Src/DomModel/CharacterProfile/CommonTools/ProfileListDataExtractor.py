class ProfileListDataExtractor:

    def extractAllNamesFromCollection(self, profile_list):

        names = []
        for singleProfile in profile_list:
            names.append(singleProfile.name())

        return names

    def extractSelectedProfileFromCollectionBy(self, actor_name, profile_list):

        for singleProfile in profile_list:
            if singleProfile.name() == actor_name:
                return singleProfile

        return None
