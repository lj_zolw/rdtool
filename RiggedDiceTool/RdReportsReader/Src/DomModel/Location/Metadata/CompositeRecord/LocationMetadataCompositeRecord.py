import uuid

from .Command.CompositeLocationMetadataToRdText import CompositeLocationMetadataToRdText
from .Command.MergeCompositeLocationMetadata import MergeCompositeLocationMetadata
from .Command.RetrieveRootOfCompositeLocationMetadata import RetrieveRootOfCompositeLocationMetadata


class LocationMetadataCompositeRecord:

    single_location_metadata_value = None
    indentation_level = None
    location_children = []
    parent = None
    unique_id = None

    def __init__(self, single_location_metadata_value, depth):
        self.single_location_metadata_value = single_location_metadata_value
        self.indentation_level = depth
        self.location_children = []
        self.parent = None
        self.unique_id = uuid.uuid4()

    def guid(self):
        return self.unique_id

    def value(self):
        return self.single_location_metadata_value

    def mergeWithMetadataCollection(self, another_one):
        return MergeCompositeLocationMetadata().twoIntoNewOne(self.root(), another_one.root())

    def to_rd_text(self):
        return CompositeLocationMetadataToRdText().fromComposite(self)

    def name(self):
        return self.value().name()

    # Composite collection methods

    def root(self):
        return RetrieveRootOfCompositeLocationMetadata().from_composite(self)

    def intendationLevel(self):
        return self.indentation_level

    def addChild(self, location_metadata):
        self.location_children.append(location_metadata)
        location_metadata.setParent(self)

    def removeChild(self, location_metadata):
        new_children_list = []
        for child in self.location_children:
            if child.guid() != location_metadata.guid():
                new_children_list.append(child)
        self.location_children = new_children_list

    def children(self):
        return self.location_children

    def hasChildren(self):
        return len(self.location_children) != 0

    def setParent(self, location_metadata):
        self.parent = location_metadata

    def getParent(self):
        return self.parent

    def addSibling(self, location_metadata):
        self.getParent().addChild(location_metadata)

