class RetrieveRootOfCompositeLocationMetadata:

    def from_composite(self, composite_location):

        current = composite_location
        parent = composite_location.getParent()

        while parent is not None:
            current = parent
            parent = parent.getParent()

        return current
