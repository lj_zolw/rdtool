class CompositeLocationMetadataToRdText:

    def fromComposite(self, composite):

        header = ""
        body = self.compositeToTextRecursively(composite.root())
        footer = "\n"
        return header + body + footer

    def compositeToTextRecursively(self, node):

        result = ""

        for child in node.children():

            indentation_level = child.intendationLevel()
            value = child.value()

            result = result + "\n" + value.to_rd_text(indentation_level)

            if node.hasChildren():
                result = result + self.compositeToTextRecursively(child)

        return result
