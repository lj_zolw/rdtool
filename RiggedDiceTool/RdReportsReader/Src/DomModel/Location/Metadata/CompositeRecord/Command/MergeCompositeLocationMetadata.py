from ..Creation.CreateLocationMetadataCompositeRecord import CreateLocationMetadataCompositeRecord


class MergeCompositeLocationMetadata:

    def twoIntoNewOne(self, left, right):

        merged = self.getThemToCommonRoot(left, right)
        self.purgeDuplicatesRelinkingChildrenRecursively(merged.root())
        return merged

    def purgeDuplicatesRelinkingChildrenRecursively(self, node):

        already_present = {}
        children_to_delete = []

        for singleChild in node.children():

            if singleChild.name().lower().strip() in already_present:

                child_to_link_grandchildren_to = already_present[singleChild.name().lower().strip()]
                duplicate_child = singleChild

                children_to_delete.append(duplicate_child)

                for grandchildToLink in duplicate_child.children():
                    child_to_link_grandchildren_to.addChild(grandchildToLink)
                    grandchildToLink.setParent(child_to_link_grandchildren_to)

            else:
                already_present[singleChild.name().lower().strip()] = singleChild

        for childToDelete in children_to_delete:
            node.removeChild(childToDelete)

        for singleChild in node.children():
            self.purgeDuplicatesRelinkingChildrenRecursively(singleChild)

        node.children().sort(key=lambda value: value.name().lower().strip())

    def getThemToCommonRoot(self, left, right):

        merged = CreateLocationMetadataCompositeRecord().rootLocationMetadata()

        for singleChild in left.children():
            singleChild.setParent(merged)
            merged.addChild(singleChild)

        for singleChild in right.children():
            singleChild.setParent(merged)
            merged.addChild(singleChild)

        return merged
