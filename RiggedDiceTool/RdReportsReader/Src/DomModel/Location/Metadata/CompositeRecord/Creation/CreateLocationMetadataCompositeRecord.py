import re

from ...Value.Creation.CreateLocationMetadataValue import CreateLocationMetadataValue
from ...Value.Command.CalculateDepthLevel import CalculateDepthLevel


class CreateLocationMetadataCompositeRecord:

    def rootLocationMetadata(self):
        root = CreateLocationMetadataValue().rootLocationMetadata()
        return LocationMetadataCompositeRecord(root, -1)

    def empty_collection(self):
        return self.rootLocationMetadata()

    def wrapLocationMetadataValueWithFloatingComposite(self, location_metadata_value, depth):
        return LocationMetadataCompositeRecord(location_metadata_value, depth)

    def fromSingleLine(self, single_line, indentation_depth_symbol):
        value = CreateLocationMetadataValue().fromLocationLine(single_line)
        depth = CalculateDepthLevel().from_valid_string(single_line, indentation_depth_symbol)
        composite = self.wrapLocationMetadataValueWithFloatingComposite(value, depth)
        return composite

    def fromTextLineArray(self, location_lines):

        indentation_depth_symbol = " "

        root = CreateLocationMetadataCompositeRecord().rootLocationMetadata()
        previously_analyzed_location = root

        for single_line in location_lines:

            current_depth = CalculateDepthLevel().from_valid_string(single_line, indentation_depth_symbol)
            if current_depth > previously_analyzed_location.intendationLevel():

                location_metadata_composite = self.fromSingleLine(single_line, indentation_depth_symbol)
                previously_analyzed_location.addChild(location_metadata_composite)
                previously_analyzed_location = location_metadata_composite

            elif current_depth == previously_analyzed_location.intendationLevel():

                location_metadata_composite = self.fromSingleLine(single_line, indentation_depth_symbol)
                previously_analyzed_location.addSibling(location_metadata_composite)
                previously_analyzed_location = location_metadata_composite

            else:

                potential_parent_indentation = previously_analyzed_location.intendationLevel()
                potential_parent = previously_analyzed_location

                while current_depth <= potential_parent_indentation:
                    potential_parent = potential_parent.getParent()
                    potential_parent_indentation = potential_parent.intendationLevel()

                location_metadata_composite = self.fromSingleLine(single_line, indentation_depth_symbol)
                potential_parent.addChild(location_metadata_composite)
                previously_analyzed_location = location_metadata_composite

        return root

from ..LocationMetadataCompositeRecord import LocationMetadataCompositeRecord
