import re


class CalculateDepthLevel:

    def from_valid_string(self, location_line, indentation_depth_symbol):
        depth_section = re.findall("\s*#", location_line)[0]
        depth = depth_section.count(indentation_depth_symbol)
        return depth
