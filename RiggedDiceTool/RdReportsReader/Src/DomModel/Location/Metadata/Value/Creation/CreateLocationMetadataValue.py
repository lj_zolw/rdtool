import re

from ..LocationMetadataValue import LocationMetadataValue


class CreateLocationMetadataValue:

    def rootLocationMetadata(self):
        return LocationMetadataValue("*", "")

    def fromLocationLine(self, single_line):

        delimiter = ","

        record = re.findall("(#\s*)(.+)", single_line)[0][1]
        stripped = record.strip()

        first_delimiter_index = stripped.find(delimiter)

        if first_delimiter_index == -1:
            name = stripped
            deed = ""
        else:
            name = stripped[0: first_delimiter_index]
            deed = stripped[first_delimiter_index + len(delimiter): len(stripped)].strip()

        return LocationMetadataValue(name, deed)

