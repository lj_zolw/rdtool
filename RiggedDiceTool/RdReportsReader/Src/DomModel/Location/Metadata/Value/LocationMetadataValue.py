class LocationMetadataValue:

    location_name = None
    location_deed = None

    def __init__(self, name, deed):
        self.location_name = name
        self.location_deed = deed

    # Domain methods

    def name(self):
        return self.location_name

    def deed(self):
        return self.location_deed

    def to_rd_text(self, indentation_level):
        result = " " * indentation_level + "# " + self.location_name
        return result
