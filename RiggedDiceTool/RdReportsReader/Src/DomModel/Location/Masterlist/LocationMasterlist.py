from .Command.LocationMasterlistToRdText import LocationMasterlistToRdText


class LocationMasterlist:

    composite_metadata = None

    def __init__(self, composite_metadata):
        self.composite_metadata = composite_metadata

    def to_rd_text(self):
        return LocationMasterlistToRdText().fromComposite(self.composite_metadata)
