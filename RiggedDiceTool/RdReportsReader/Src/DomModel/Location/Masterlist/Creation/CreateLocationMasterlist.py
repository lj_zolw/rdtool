from ..LocationMasterlist import LocationMasterlist
from ...Complete.Partial.Generated.Single.Creation.CreateLocationsInReport import CreateLocationsInReport


class CreateLocationMasterlist:

    def having_composite_metadata(self, composite_metadata):
        return LocationMasterlist(composite_metadata)

    def having_report_collection(self, report_collection):

        all_locations_on_missions = report_collection.all_locations_on_missions()

        combined_locations_in_pseudo_single_report = self._merge_locations_on_missions(all_locations_on_missions)
        masterlist = self.having_composite_metadata(combined_locations_in_pseudo_single_report.compositeMetadataCollection())
        return masterlist

    def _merge_locations_on_missions(self, all_locations_on_missions):

        operated = CreateLocationsInReport().emptyLocationsInReport()

        for single in all_locations_on_missions:
            operated = operated.mergeWith(single)

        return operated
