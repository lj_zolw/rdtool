from RdReportsReader.Src.DomModel.Location.Complete.Partial.Generated.Single.Creation.CreateLocationsInReport import CreateLocationsInReport


class MergeLocationsInReport:

    def from_two(self, left, right):
        merged_collection = left.compositeMetadataCollection().mergeWithMetadataCollection(right.compositeMetadataCollection())
        return CreateLocationsInReport().fromLocationMetadataCollection(merged_collection)

