from .CreateLocationsInReport import CreateLocationsInReport


class ExtractLocationsInReport:

    # If possible, will return locations.
    # If impossible, will return empty locations (work as NullObject here).
    def optionally_from(self, rd_report):

        location_text_section = rd_report.location_text_section()

        if not location_text_section:
            locations_in_report = CreateLocationsInReport().emptyLocationsInReport()
        else:
            try:
                locations_in_report = CreateLocationsInReport().fromRdTextSection(location_text_section)

            except:
                locations_in_report = CreateLocationsInReport().emptyLocationsInReport()
                print("RD_PARSE_FAILURE_LOCATION: Report: " + rd_report.name())

        return locations_in_report
