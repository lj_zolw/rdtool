from .Parsing.SplitLocationTextSectionIntoValidLocationLines import SplitLocationTextSectionIntoValidLocationLines
from RdReportsReader.Src.DomModel.Location.Metadata.CompositeRecord.Creation.CreateLocationMetadataCompositeRecord import CreateLocationMetadataCompositeRecord


class CreateLocationsInReport:

    def fromLocationMetadataCollection(self, location_metadata_collection):
        return LocationsInReport(location_metadata_collection)

    def emptyLocationsInReport(self):
        location_metadata_empty_collection = CreateLocationMetadataCompositeRecord().empty_collection()
        return self.fromLocationMetadataCollection(location_metadata_empty_collection)

    def fromRdTextSection(self, location_text_section):
        location_lines = SplitLocationTextSectionIntoValidLocationLines().fromTextSection(location_text_section)
        location_metadata_collection = CreateLocationMetadataCompositeRecord().fromTextLineArray(location_lines)
        return self.fromLocationMetadataCollection(location_metadata_collection)

from ..LocationsInReport import LocationsInReport
