import re


class SplitLocationTextSectionIntoValidLocationLines:

    def fromTextSection(self, location_text_section):

        location_lines = re.findall("\s*#.+", location_text_section)
        return location_lines
