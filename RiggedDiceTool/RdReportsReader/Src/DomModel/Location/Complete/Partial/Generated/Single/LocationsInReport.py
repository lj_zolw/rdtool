from .Command.MergeLocationsInReport import MergeLocationsInReport


class LocationsInReport:

    collection = None

    def __init__(self, location_metadata_collection):
        self.collection = location_metadata_collection

    def compositeMetadataCollection(self):
        return self.collection

    def mergeWith(self, another_one):
        return MergeLocationsInReport().from_two(self, another_one)
