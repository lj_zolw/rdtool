import json

from RdReportsReader.Src.CommonTools.FileOps.AbsoluteFileLocation import AbsoluteFileLocation
from RdReportsReader.Src.CommonTools.FileOps.ReadFile import ReadFile


class ReadConfigJson:

    read_config = None

    # Less file reading == good stuff.

    def __init__(self):
        if ReadConfigJson.read_config is None:
            ReadConfigJson.read_config = self._read_json_config()
        else:
            pass

    # Superclasses of configuration go here

    def general_files_area(self):
        return ReadConfigJson.read_config['general_files']

    def zip_source_area(self):
        return ReadConfigJson.read_config['zip_source']

    # Private method. For pseudo-singleton approach.

    def _read_json_config(self):
        config_path = AbsoluteFileLocation().of_file(__file__, "../rd_tool_config.json")
        read_json = ReadFile().from_path(config_path)
        parsed_config = json.loads(read_json)
        return parsed_config
