from RdReportsReader.Src.AppServices.Command.Strategy.Selection.HighLvCommandSelector import HighLvCommandSelector


class ProvideSelectedCommandsConfig:

    def __init__(self):
        self.commands = 0
        self.commands |= HighLvCommandSelector.FormCharacterProfilesFromRd.value
        self.commands |= HighLvCommandSelector.CorrectReportMasterlistSeqNo.value
        self.commands |= HighLvCommandSelector.GenerateLocationMasterlist.value
        self.commands |= HighLvCommandSelector.GenerateCampaignSummaries.value

    def selected_commands(self):
        return self.commands

