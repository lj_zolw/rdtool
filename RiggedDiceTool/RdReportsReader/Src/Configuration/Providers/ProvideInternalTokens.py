import configparser

from RdReportsReader.Src.CommonTools.FileOps.AbsoluteFileLocation import AbsoluteFileLocation


class ProvideInternalTokens:

    config = None

    def __init__(self):
        if not ProvideInternalTokens.config:
            self._initialize_configparser()

    # Internal links

    def one_of_reports_path_prefix(self):
        return ProvideInternalTokens.config['Internal_Links']['one_of_reports_path_prefix']

    # Internal grammar

    def link_start(self):
        return ProvideInternalTokens.config['Internal_Grammar']['link_start']

    def link_end(self):
        return ProvideInternalTokens.config['Internal_Grammar']['link_end']

    def link_display_separator(self):
        return ProvideInternalTokens.config['Internal_Grammar']['link_display_separator']

    # Headers

    def masterlist_campaign_header(self):
        return ProvideInternalTokens.config['Headers']['masterlist_campaign_header']

    # Private

    def _initialize_configparser(self):
        ProvideInternalTokens.config = configparser.ConfigParser()
        path = AbsoluteFileLocation().of_file(__file__, "../rd_internal_tokens.properties")
        ProvideInternalTokens.config.read(path)
