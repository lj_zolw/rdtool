from RdReportsReader.Src.Configuration.Providers.ProvideSelectedCommandsConfig import ProvideSelectedCommandsConfig
from .Command.HighLvCommandFactory import HighLvCommandFactory


class RdTool:

    def start(self):

        selected = ProvideSelectedCommandsConfig().selected_commands()
        constructed_commands = HighLvCommandFactory().create_command_from(selected)

        for single_command in constructed_commands:
            print(single_command.announce_start())
            single_command.perform_action()
            print(single_command.announce_finish())

        print("Everything is finished, all is done.")
