from .Strategy.Selection.HighLvCommandSelector import HighLvCommandSelector
from .Strategy.Options.CombineIntoCharacterProfiles import CombineIntoCharacterProfiles
from .Strategy.Options.CorrectReportMasterlistSequenceNumbers import CorrectReportMasterlistSequenceNumbers
from .Strategy.Options.GenerateLocationMasterlist import GenerateLocationMasterlist
from .Strategy.Options.GenerateCampaignSummaries import GenerateCampaignSummaries


class HighLvCommandFactory:

    def create_command_from(self, selected):

        created = []

        if selected == HighLvCommandSelector.NotSelected.value:
            raise KeyError("Lack of main command for the application to use.")

        if selected & HighLvCommandSelector.FormCharacterProfilesFromRd.value:
            created.append(CombineIntoCharacterProfiles())

        if selected & HighLvCommandSelector.CorrectReportMasterlistSeqNo.value:
            created.append(CorrectReportMasterlistSequenceNumbers())

        if selected & HighLvCommandSelector.GenerateLocationMasterlist.value:
            created.append(GenerateLocationMasterlist())

        if selected & HighLvCommandSelector.GenerateCampaignSummaries.value:
            created.append(GenerateCampaignSummaries())

        return created
