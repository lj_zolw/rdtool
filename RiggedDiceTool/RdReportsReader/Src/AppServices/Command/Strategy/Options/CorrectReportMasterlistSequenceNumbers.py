from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Domain.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persistor.Report.PersistReport import PersistReport


class CorrectReportMasterlistSequenceNumbers:

    def announce_start(self):
        return "ReportMasterlist sequence number update started."

    def perform_action(self):
        return self._simple_masterlist_correction()

    def announce_finish(self):
        return "ReportMasterlist updated with correct sequence numbers."

    def _simple_masterlist_correction(self):

        report_masterlist = ProvideFromReportDomain(ZipAggregatedSource()).masterlist()
        PersistReport().masterlist_at_dest(report_masterlist, FileAggregatedDestination())
