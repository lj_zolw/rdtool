from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Domain.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.DomModel.Report.Masterlist.Campaign.Summary.Collection.Creation.CreateCampaignSummaryCollection import CreateCampaignSummaryCollection
from RdReportsReader.Src.Infrastructure.Write.Persistor.Report.PersistReport import PersistReport
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination


class GenerateCampaignSummaries:

    def announce_start(self):
        return "Generation of CampaignSummaries started."

    def perform_action(self):
        return self._from_report_data()

    def announce_finish(self):
        return "Generation of CampaignSummaries completed."

    def _from_report_data(self):

        report_provider = ProvideFromReportDomain(ZipAggregatedSource())
        report_collection = report_provider.collection()
        report_masterlist = report_provider.masterlist()

        campaign_summaries = CreateCampaignSummaryCollection().having(report_collection, report_masterlist)

        PersistReport().campaign_summary_collection_at_dest(campaign_summaries, FileAggregatedDestination())
