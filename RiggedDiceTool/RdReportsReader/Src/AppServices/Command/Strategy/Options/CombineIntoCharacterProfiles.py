from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Provider.CharacterProfile.ProvideFromProfileDomain import ProvideFromProfileDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Domain.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persistor.CharacterProfile.PersistProfile import PersistProfile
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Collection.Creation.CreateCompleteProfileCollection import CreateCompleteProfileCollection


class CombineIntoCharacterProfiles:

    def announce_start(self):
        return "Integration of RdReports and RdCharacters started."

    def perform_action(self):
        return self._from_reports_and_existing_profiles()

    def announce_finish(self):
        return "Integration of RdReports and RdCharacters completed."

    def _from_reports_and_existing_profiles(self):

        report_provider = ProvideFromReportDomain(ZipAggregatedSource())
        reports = report_provider.collection()

        profile_provider = ProvideFromProfileDomain(ZipAggregatedSource())
        read_profiles_component = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles_component = reports.generate_profiles()

        combined_profile_collection = CreateCompleteProfileCollection().mergeReadAndGenerated(read_profiles_component, generated_profiles_component, profile_masterlist)
        PersistProfile().complete_collection_at_dest(combined_profile_collection, FileAggregatedDestination())
