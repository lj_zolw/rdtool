from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Zip.Domain.ZipAggregatedSource import ZipAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persistor.Location.PersistLocation import PersistLocation
from RdReportsReader.Src.DomModel.Location.Masterlist.Creation.CreateLocationMasterlist import CreateLocationMasterlist

class GenerateLocationMasterlist:

    def announce_start(self):
        return "Generation of LocationMasterlist started."

    def perform_action(self):
        return self._location_masterlist_generation()

    def announce_finish(self):
        return "Generation of LocationMasterlist finished."

    def _location_masterlist_generation(self):

        report_collection = ProvideFromReportDomain(ZipAggregatedSource()).collection()
        generated_location_masterlist = CreateLocationMasterlist().having_report_collection(report_collection)

        PersistLocation().masterlist_at_dest(generated_location_masterlist, FileAggregatedDestination())
