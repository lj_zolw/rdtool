from enum import Enum


class HighLvCommandSelector(Enum):

    NotSelected = 0b0
    FormCharacterProfilesFromRd = 0b1
    CorrectReportMasterlistSeqNo = 0b10
    GenerateLocationMasterlist = 0b100
    GenerateCampaignSummaries = 0b1000
