import unittest
from unittest.mock import MagicMock

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persistor.Report.PersistReport import PersistReport
from RdReportsReader.Src.DomModel.Report.Masterlist.Campaign.Summary.Collection.Creation.CreateCampaignSummaryCollection import CreateCampaignSummaryCollection


class TestGenerateCampaignSummaries(unittest.TestCase):

    def test_having_2_missions_with_perfect_data__generates_properly(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M02_MPW_ConsequencesProgressionLocations())
        provider = ProvideFromReportDomain(selected_source)

        report_collection = provider.collection()
        report_masterlist = provider.masterlist()

        campaign_summaries = CreateCampaignSummaryCollection().having(report_collection, report_masterlist)

        data_destination = FileAggregatedDestination()
        data_destination.persist_campaign_summary = MagicMock(return_value=None)

        # When

        PersistReport().campaign_summary_collection_at_dest(campaign_summaries, data_destination)

        # Then

        expected_campaign_name = report_masterlist.all_campaigns()[0].name()
        expected_consequence_1 = report_collection.mission_reports()[0].consequences().to_rd_text()
        expected_consequence_2 = report_collection.mission_reports()[1].consequences().to_rd_text()

        actual_campaign_text_to_be_persisted = data_destination.persist_campaign_summary.call_args_list[0][0][1]

        self.assertTrue(actual_campaign_text_to_be_persisted, "Text to be persisted is falsy ('' or None)")
        self.assertTrue(expected_campaign_name in actual_campaign_text_to_be_persisted, "Campaign name not found in text to be persisted")
        self.assertTrue(expected_consequence_1 in actual_campaign_text_to_be_persisted, "Consequences from mission_1 not found in text to be persisted")
        self.assertTrue(expected_consequence_2 in actual_campaign_text_to_be_persisted, "Consequences from mission_2 not found in text to be persisted")

    def test_having_only_2_missions_with_no_data__generates_name(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M02_MPW_Simplest_Locations())
        provider = ProvideFromReportDomain(selected_source)

        report_collection = provider.collection()
        report_masterlist = provider.masterlist()

        campaign_summaries = CreateCampaignSummaryCollection().having(report_collection, report_masterlist)

        data_destination = FileAggregatedDestination()
        data_destination.persist_campaign_summary = MagicMock(return_value=None)

        # When

        PersistReport().campaign_summary_collection_at_dest(campaign_summaries, data_destination)

        # Then

        expected_campaign_name = report_masterlist.all_campaigns()[0].name()

        actual_campaign_text_to_be_persisted = data_destination.persist_campaign_summary.call_args_list[0][0][0]

        self.assertTrue(actual_campaign_text_to_be_persisted, "Text to be persisted is falsy ('' or None)")
        self.assertTrue(expected_campaign_name in actual_campaign_text_to_be_persisted, "Campaign name not found in text to be persisted")
