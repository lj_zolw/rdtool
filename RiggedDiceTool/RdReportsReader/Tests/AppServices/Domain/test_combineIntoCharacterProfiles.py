import unittest
from unittest.mock import MagicMock

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Resources.TestData.Profile.Collection.ProfileCollectionResource import ProfileCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Read.Provider.CharacterProfile.ProvideFromProfileDomain import ProvideFromProfileDomain
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Collection.Creation.CreateCompleteProfileCollection import CreateCompleteProfileCollection
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persistor.CharacterProfile.PersistProfile import PersistProfile


class TestCombineIntoCharacterProfiles(unittest.TestCase):

    def test_combineIntoProfiles_everythingOnMocksIncludingSave(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M06_CorrectComplete())
        selected_source.selectProfileCollectionResource(ProfileCollectionResource().F03S04P08_SimpleComplete())

        reports = ProvideFromReportDomain(selected_source).collection()

        profile_provider = ProvideFromProfileDomain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        complete_profile_collection = CreateCompleteProfileCollection().mergeReadAndGenerated(read_profiles, generated_profiles, profile_masterlist)

        destination_to_test = FileAggregatedDestination()
        destination_to_test.persist_single_profile = MagicMock(return_value=None)

        # When

        PersistProfile().complete_collection_at_dest(complete_profile_collection, destination_to_test)

        # Then

        self.assertTrue(destination_to_test.persist_single_profile.call_count == 15, "15 profiles were not 'written' to MagicMock.")

        call_list = destination_to_test.persist_single_profile.call_args_list

        andrea_call = call_list[0][0]
        diana_call = call_list[5][0]

        self.assertTrue(andrea_call[0] == "Andrea Wilgacz" and "+++ Jakie ma zasoby" in andrea_call[1], "Andrea record malformed")
        self.assertTrue(diana_call[0] == "Diana Weiner" and "+++ Jaka jest" in diana_call[1], "Diana record malformed")
