import unittest
from unittest.mock import MagicMock

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persistor.Report.PersistReport import PersistReport


class TestRecalculateReportNumbers(unittest.TestCase):

    def test_recalculateReportNumbers_FullRecalcOnMocks(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportMasterlistResource(ReportMasterlistResource().C09_M10_SeqNoWrong())

        # When

        reports_masterlist = ProvideFromReportDomain(selected_source).masterlist()

        # Then

        self.assertTrue(reports_masterlist.report_number(1).sequence_number() == "001", "Recalculated sequenceNumber is wrong, should be '1' was: " + reports_masterlist.report_number(1).sequence_number())
        self.assertTrue(reports_masterlist.report_number(2).sequence_number() == "002", "Recalculated sequenceNumber is wrong, should be '2' was: " + reports_masterlist.report_number(2).sequence_number())
        self.assertTrue(reports_masterlist.report_number(3).sequence_number() == "003", "Recalculated sequenceNumber is wrong, should be '3' was: " + reports_masterlist.report_number(3).sequence_number())

        # Given

        data_destination = FileAggregatedDestination()
        data_destination.persist_report_masterlist = MagicMock(return_value=None)

        expected_masterlist_text = reports_masterlist.to_rd_text()

        # When

        PersistReport().masterlist_at_dest(reports_masterlist, data_destination)

        # Then

        actual_masterlist_text = data_destination.persist_report_masterlist.call_args_list[0][0][0]

        self.assertTrue(expected_masterlist_text == actual_masterlist_text, "Masterlist text was wrong when passed to persistReportMasterlist.")
