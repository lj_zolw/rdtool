import unittest
from unittest.mock import MagicMock

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Write.Destination.File.Domain.FileAggregatedDestination import FileAggregatedDestination
from RdReportsReader.Src.Infrastructure.Write.Persistor.Location.PersistLocation import PersistLocation
from RdReportsReader.Src.DomModel.Location.Masterlist.Creation.CreateLocationMasterlist import CreateLocationMasterlist


class TestGenerateLocations(unittest.TestCase):

    def test_generate_location_masterlist__on_mocks_including_save(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M02_MPW_Simplest_Locations())

        report_collection = ProvideFromReportDomain(selected_source).collection()

        generated_location_masterlist = CreateLocationMasterlist().having_report_collection(report_collection)

        destination_to_test = FileAggregatedDestination()
        destination_to_test.persist_location_masterlist = MagicMock(return_value=None)

        # When

        PersistLocation().masterlist_at_dest(generated_location_masterlist, destination_to_test)

        # Then

        persisted_location_masterlist = destination_to_test.persist_location_masterlist.call_args_list
        persisted_masterlist_text = persisted_location_masterlist[0][0][0]

        self.assertTrue("+ Lokalizacje:" in persisted_masterlist_text, "Lokalizacje header not in persisted_masterlist_text")
        self.assertTrue(" # Śląsk" in persisted_masterlist_text, "Śląsk on deoth == 1 not in persisted_masterlist_text")
        self.assertTrue("    # Wielkie Muzeum Śląskie" in persisted_masterlist_text, "Museum uniquely from 'Irytka' on deoth == 4 not in persisted_masterlist_text")
        self.assertTrue("     # Sala audiencyjna Lorda Terminusa" in persisted_masterlist_text,
                        "Audience Hall uniquely from 'Millennium' on deoth == 5 not in persisted_masterlist_text")
        self.assertTrue(persisted_masterlist_text.count("# Śląsk\n") == 1, "non-unique Śląsk appears more than once in persisted_masterlist_text")
