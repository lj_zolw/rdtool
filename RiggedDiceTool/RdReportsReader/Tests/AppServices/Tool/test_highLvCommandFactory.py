import unittest

from RdReportsReader.Src.AppServices.Command.HighLvCommandFactory import HighLvCommandFactory
from RdReportsReader.Src.AppServices.Command.Strategy.Selection.HighLvCommandSelector import HighLvCommandSelector


class TestHighLvCommandFactory(unittest.TestCase):

    def test_ifNothingSelectedWorks(self):
        with self.assertRaises(KeyError):

            # Given
            selected = 0

            # When & Then
            HighLvCommandFactory().create_command_from(selected)

    def test_highLevelOperation(self):

        # Given

        selected1 = HighLvCommandSelector.FormCharacterProfilesFromRd.value
        selected2 = HighLvCommandSelector.CorrectReportMasterlistSeqNo.value
        selected = selected1 | selected2

        # When

        commands = HighLvCommandFactory().create_command_from(selected)

        # Then
        commands_count = len(commands)
        self.assertTrue(commands_count == 2, "The factory did not return two objects but: " + str(len(commands)))
