import unittest

from RdReportsReader.Src.DomModel.Location.Metadata.CompositeRecord.Creation.CreateLocationMetadataCompositeRecord import CreateLocationMetadataCompositeRecord
from RdReportsReader.Src.DomModel.Location.Metadata.CompositeRecord.Command.MergeCompositeLocationMetadata import MergeCompositeLocationMetadata


class TestMergeLocationMetadataCollections(unittest.TestCase):

    def test_canMergeTwoSimpleMetadataCollections(self):

        # Given

        firstLineBlock =  "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB".split("\n")
        secondLineBlock = "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n   # DistrictB\n    # Tertias".split("\n")

        firstCollection = CreateLocationMetadataCompositeRecord().fromTextLineArray(firstLineBlock)
        secondCollection = CreateLocationMetadataCompositeRecord().fromTextLineArray(secondLineBlock)

        expectedResult = "# Świat\n # Śląsk\n  # Kopalin\n   # DistrictA\n    # Prime\n    # Secundus\n   # DistrictB\n    # Tertias".split("\n")
        expectedCollection = CreateLocationMetadataCompositeRecord().fromTextLineArray(expectedResult)

        # When

        mergedCollection = MergeCompositeLocationMetadata().twoIntoNewOne(firstCollection.root(), secondCollection.root())

        # Then

        rootContains1Node = len(mergedCollection.root().children()) == len(expectedCollection.root().children())
        disAContains2Nodes = len(mergedCollection.root().children()[0].children()[0].children()[0].children()[0].children()) == len(expectedCollection.root().children()[0].children()[0].children()[0].children()[0].children())
        disBContains1Node = len(mergedCollection.root().children()[0].children()[0].children()[0].children()[1].children()) == len(expectedCollection.root().children()[0].children()[0].children()[0].children()[1].children())
        disAContainsPrime = mergedCollection.root().children()[0].children()[0].children()[0].children()[0].children()[0].name() == expectedCollection.root().children()[0].children()[0].children()[0].children()[0].children()[0].name()
        disAContainsSecondus = mergedCollection.root().children()[0].children()[0].children()[0].children()[0].children()[1].name() == expectedCollection.root().children()[0].children()[0].children()[0].children()[0].children()[1].name()
        disBContainsTertias = mergedCollection.root().children()[0].children()[0].children()[0].children()[1].children()[0].name() == expectedCollection.root().children()[0].children()[0].children()[0].children()[1].children()[0].name()

        self.assertTrue(rootContains1Node, "Composite root contains more than only the 'Kopalin' node; does not match reference collection")
        self.assertTrue(disAContains2Nodes, "DistrictA does not contain 2 nodes (expected: 'Prime, Secundus'; does not match reference collection")
        self.assertTrue(disBContains1Node, "DistrictB does not contain 1 node (expected: 'Tertias'; does not match reference collection")
        self.assertTrue(disAContainsPrime, "DistrictA does not contain 'Prime' node with appropriate name; does not match reference collection")
        self.assertTrue(disAContainsSecondus, "DistrictA does not contain 'Secundus' node with appropriate name; does not match reference collection")
        self.assertTrue(disBContainsTertias, "DistrictB does not contain 'Tertias' node with appropriate name; does not match reference collection")

    def test_merge_properly_ignoring_case(self):

        # Given

        first_line_block =  "# Świat\n # Śląsk\n  # Kopalin".split("\n")
        second_line_block = "# Świat\n # śląsk\n  # Myślin".split("\n")

        first_collection = CreateLocationMetadataCompositeRecord().fromTextLineArray(first_line_block)
        second_collection = CreateLocationMetadataCompositeRecord().fromTextLineArray(second_line_block)

        expected_result = "# Świat\n # Śląsk\n  # Kopalin\n  # Myślin".split("\n")
        expected_collection = CreateLocationMetadataCompositeRecord().fromTextLineArray(expected_result)

        # When

        merged_collection = MergeCompositeLocationMetadata().twoIntoNewOne(first_collection.root(), second_collection.root())

        # Then

        should_have_1_1_structure = len(merged_collection.root().children()[0].children()) == len(expected_collection.root().children()[0].children())
        should_have_1_1_2_structure = len(merged_collection.root().children()[0].children()[0].children()) == len(expected_collection.root().children()[0].children()[0].children())

        self.assertTrue(should_have_1_1_structure, "Probably merging is case-sensitive (while we expect ignoring case); not '1' at 'Śląsk' level: " +
                        merged_collection.root().children()[0].children()[0].name())
        self.assertTrue(should_have_1_1_2_structure, "Probably merging is case-sensitive (while we expect ignoring case); not '2' at 'Kopalin/Myślin' level: " +
                        merged_collection.root().children()[0].children()[0].children()[0].name())
