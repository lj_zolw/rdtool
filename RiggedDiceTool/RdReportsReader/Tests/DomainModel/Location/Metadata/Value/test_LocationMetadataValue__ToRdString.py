import unittest

from RdReportsReader.Src.DomModel.Location.Metadata.Value.Creation.CreateLocationMetadataValue import CreateLocationMetadataValue


class test_LocationMetadataValue__ToRdString(unittest.TestCase):

    def test_masterlist_display__record_with_identifier_no_deed(self):

        # Given
        record = "  # Kopalin"

        # When
        metadata_value = CreateLocationMetadataValue().fromLocationLine(record)

        # Then
        self.assertTrue(metadata_value.name() == "Kopalin", "Identifier wrongly parsed. Expected: Kopalin, actual: " + metadata_value.name())
        self.assertTrue(not metadata_value.deed(), "Deed was not empty with empty record.")

    def test_masterlist_display__record_with_identifier_and_deed(self):

        # Given
        record = "  # Kopalin, where, against all hopes, the power of the Silver Candle started to wane."

        # When
        metadata_value = CreateLocationMetadataValue().fromLocationLine(record)

        # Then
        self.assertTrue(metadata_value.name() == "Kopalin", "Identifier wrongly parsed. Expected: Kopalin, actual: " + metadata_value.name())
        self.assertTrue(metadata_value.deed() == "where, against all hopes, the power of the Silver Candle started to wane.", "Deed was empty or did not correspond to expected string.")
