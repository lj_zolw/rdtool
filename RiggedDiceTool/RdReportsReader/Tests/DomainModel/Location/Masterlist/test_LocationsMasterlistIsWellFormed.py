import unittest

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.DomModel.Location.Masterlist.Creation.CreateLocationMasterlist import CreateLocationMasterlist

class TestLocationsMasterlistIsWellFormed(unittest.TestCase):

    def test_ignores_deeds__having_comma_delimiter(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M03_MPW_locations_with_deeds())

        report_collection = ProvideFromReportDomain(selected_source).collection()
        generated_location_masterlist = CreateLocationMasterlist().having_report_collection(report_collection)

        # When

        masterlist_text = generated_location_masterlist.to_rd_text()

        # Then

        slask_amount = masterlist_text.count(" # Śląsk")
        deed_text = "wykorzystywane jako dowodzenie"

        self.assertTrue(slask_amount == 1, "Śląsk record detected twice; probably Deed not extracted properly from LocationMasterlist.")
        self.assertTrue(deed_text not in masterlist_text, "Location deed detected in masterlist_text (should not be present, as it is a part of Single Location).")
