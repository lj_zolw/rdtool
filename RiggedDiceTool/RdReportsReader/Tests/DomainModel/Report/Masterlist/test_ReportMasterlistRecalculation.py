import unittest

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Creation.ReportMasterListFactory import ReportMasterlistFactory
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Report.Masterlist.MockReportMasterlistSource import MockReportMasterlistSource


class TestReportMasterlistRecalculation(unittest.TestCase):

    def test_reportMasterlistRecalculateReportNumbersWorksForMock3MasterListImproperSeqNo(self):

        # Given
        mlist_key = ReportMasterlistResource().C09_M10_SeqNoWrong()
        mlist_source = MockReportMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)
        report_masterlist = ReportMasterlistFactory().createFromText(mlist_text)

        # When

        report_masterlist.correct_report_seq_numbers()

        # Then

        self.assertTrue(report_masterlist.report_number(1).sequence_number() == "001", "Recalculated sequenceNumber is wrong, expecting 1")
        self.assertTrue(report_masterlist.report_number(2).sequence_number() == "002", "Recalculated sequenceNumber is wrong, expecting 2")
        self.assertTrue(report_masterlist.report_number(3).sequence_number() == "003", "Recalculated sequenceNumber is wrong, expecting 3")

        text = report_masterlist.to_rd_text()

        amount_of_records = text.count("* [[[", 0, len(text))
        index_of_last = text.rfind("* [[[", 0, len(text))
        index_of_next_line = text.find("\n", index_of_last, len(text))

        interesting_line = text[index_of_last: index_of_next_line]
        delimiter_index = interesting_line.index("|")
        record_number_text = interesting_line[delimiter_index + 1: delimiter_index + 1 + 3]
        record_number = int(record_number_text)

        self.assertTrue(amount_of_records == record_number, "Recalculation did not properly set last record")
