import unittest

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain


class TestReportMasterlistToText(unittest.TestCase):

    def test_reportMasterlistContainsExpectedReportName(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportMasterlistResource(ReportMasterlistResource().C09_M10_SeqNoWrong())

        provide = ProvideFromReportDomain(selected_source)
        report_masterlist = provide.masterlist()

        # When

        report_section_text = report_masterlist.core().to_rd_text()

        # Then

        self.assertTrue("Ale nie można zmusić go" in report_section_text, "Report containing the section 'Ale nie można zmusić go' not found")
        self.assertTrue("++++" in report_section_text, "No campaign section found")
        self.assertTrue("* " in report_section_text, "No wikidot list found")

    def test_reportMasterlistContainsExpectedCampaignName_EmptyCampaign(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportMasterlistResource(ReportMasterlistResource().C09_M10_SeqNoWrong())

        provide = ProvideFromReportDomain(selected_source)
        report_masterlist = provide.masterlist()

        expected = "++++ Pierwsza Inwazja\n\n\n++++ Światło w Zależu Leśnym"

        # When

        report_section_text = report_masterlist.core().to_rd_text()

        # Then

        self.assertTrue(expected in report_section_text, "MasterListReportSection into text does not match mock pure source which SHOULD correspond (check it manually!)")

    def test_reportMasterlistDoesNotLoseExtraInformationAt_toText(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportMasterlistResource(ReportMasterlistResource().C02_M10_OutOfTime())

        provide = ProvideFromReportDomain(selected_source)
        report_masterlist = provide.masterlist()

        extraText = "(kiedyś: upadek Blakenbauerów)"

        # When

        report_section_text = report_masterlist.core().to_rd_text()

        # Then

        self.assertTrue(extraText in report_section_text, "Record in RdText format does not match the initial data at Masterlist conversion level; extra information was lost.")


