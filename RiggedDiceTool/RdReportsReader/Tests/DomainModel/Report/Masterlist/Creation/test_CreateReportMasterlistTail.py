import unittest

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Structure.Tail.Creation.CreateReportMasterlistTail import CreateReportMasterlistTail
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Report.Masterlist.MockReportMasterlistSource import MockReportMasterlistSource


class TestCreateReportMasterlistTail(unittest.TestCase):

    def test_with_Tail(self):

        # Given
        mlistKey = ReportMasterlistResource().C01_M06_Simple()
        mlistSource = MockReportMasterlistSource()

        mlistText = mlistSource.returnSelectedMasterlistHavingKey(mlistKey)

        tail = "tail"
        mlistText = mlistText + tail

        # When
        report_masterlist_tail = CreateReportMasterlistTail().from_text(mlistText)

        # Then
        self.assertTrue(report_masterlist_tail is not None, "ReportMasterlistTail returned as None on mock source")

        retrieved = report_masterlist_tail.to_rd_text()

        self.assertTrue(tail in retrieved, "ReportMasterlistTail does not contain added tail")
