import unittest

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Structure.Prelude.Creation.CreateReportMasterlistPrelude import CreateReportMasterlistPrelude
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Report.Masterlist.MockReportMasterlistSource import MockReportMasterlistSource


class TestCreateReportMasterlistPrelude(unittest.TestCase):

    def test_with_NoHeader(self):

        # Given
        mlistKey = ReportMasterlistResource().C01_M06_Simple()
        mlistSource = MockReportMasterlistSource()

        mlistText = mlistSource.returnSelectedMasterlistHavingKey(mlistKey)

        # When
        report_masterlist_header = CreateReportMasterlistPrelude().from_text(mlistText)

        # Then
        self.assertTrue(report_masterlist_header is not None, "ReportMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_header.to_rd_text()

        self.assertTrue(not retrieved, "ReportMasterlistPrelude does not contain no text")

    def test_with_Header(self):

        # Given
        mlistKey = ReportMasterlistResource().C01_M06_Simple()
        mlistSource = MockReportMasterlistSource()

        mlistText = mlistSource.returnSelectedMasterlistHavingKey(mlistKey)

        header = "header"
        mlistText = header + mlistText

        # When
        report_masterlist_header = CreateReportMasterlistPrelude().from_text(mlistText)

        # Then
        self.assertTrue(report_masterlist_header is not None, "ReportMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_header.to_rd_text()

        self.assertTrue(header in retrieved, "ReportMasterlistPrelude does not contain added header")
