import unittest

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Structure.Core.Creation.CreateReportMasterlistCore import CreateReportMasterlistCore
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Report.Masterlist.MockReportMasterlistSource import MockReportMasterlistSource


class TestCreateReportMasterlistCore(unittest.TestCase):

    def test_with_C01_M06_Simple(self):

        # Given
        mlist_key = ReportMasterlistResource().C01_M06_Simple()
        mlist_source = MockReportMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)

        # When
        report_masterlist_core = CreateReportMasterlistCore().from_text(mlist_text)

        # Then
        self.assertTrue(report_masterlist_core is not None, "ReportMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_core.to_rd_text()

        self.assertTrue(retrieved in mlist_text, "ReportMasterlistCore is not generated as subset of general masterlist.")
