import unittest

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.DomModel.Report.Masterlist.Complete.Creation.ReportMasterListFactory import ReportMasterlistFactory
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Report.Masterlist.MockReportMasterlistSource import MockReportMasterlistSource


class TestCreateReportMasterlist(unittest.TestCase):

    def test_with_C01_M06_Simple(self):

        # Given
        mlist_key = ReportMasterlistResource().C01_M06_Simple()
        mlist_source = MockReportMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)

        # When
        report_masterlist = ReportMasterlistFactory().createFromText(mlist_text)

        # Then
        self.assertTrue(report_masterlist is not None, "ReportMasterlist was not created at all")

        retrieved = report_masterlist.to_rd_text()

        self.assertTrue(retrieved in mlist_text, "ReportMasterlist does not match the initial records from which it was created (no nned for seqNo recalculation)")
