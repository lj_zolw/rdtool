import unittest

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource


class TestReportCollectionGeneratesProfiles(unittest.TestCase):

    def test_proper_generation_C01M06Correct(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M06_CorrectComplete())

        report_collection = ProvideFromReportDomain(selected_source).collection()

        # When

        generated_profiles = report_collection.generate_profiles()

        # Then

        count = generated_profiles.count()

        self.assertTrue(generated_profiles is not None, "Nil while trying to generate profiles from ReportCollection")
        self.assertTrue(count == 12, "Expected 12 profiles to be generated, got: " + str(count))
