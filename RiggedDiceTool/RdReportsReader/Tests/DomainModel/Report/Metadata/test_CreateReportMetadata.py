import unittest

from RdReportsReader.Src.DomModel.Report.Metadata.Single.Creation.CreateSingleReportMetadata import CreateSingleReportMetadata


class TestCreateReportMetadata(unittest.TestCase):

    def test_report_metadata_rdformat_created_properly_no_comments_record(self):

        # Given
        original_record = "* [[[inwazja-konspekty:120507-preludium-historia-swiata|001 - 120507 - Preludium: Historia świata]]]"

        # When
        metadata = CreateSingleReportMetadata().fromRawRdMetadataRecord(original_record)

        # Then
        self.assertTrue(metadata is not None, "ReportMetadata has not been created at all.")
        self.assertTrue(metadata.name() == "Preludium: Historia świata", "ReportMetadata name is wrong")
        self.assertTrue(metadata.weblink() == "inwazja-konspekty:120507-preludium-historia-swiata", "ReportMetadata weblink is wrong")
        self.assertTrue(metadata.ziplink() == "inwazja-konspekty_120507-preludium-historia-swiata", "ReportMetadata ziplink is wrong")
        self.assertTrue(metadata.sequence_number() == "001", "ReportMetadata seqNo is wrong")
        self.assertTrue(metadata.date() == "120507", "ReportMetadata date is wrong")
