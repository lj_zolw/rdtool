import unittest

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Resources.TestData.Profile.Collection.ProfileCollectionResource import ProfileCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Read.Provider.CharacterProfile.ProvideFromProfileDomain import ProvideFromProfileDomain
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Collection.Creation.CreateCompleteProfileCollection import CreateCompleteProfileCollection


class TestSingleProfile_ToRdText(unittest.TestCase):

    def test_andrea_has_name_and_it_does_not_become_a_non_name_section(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M06_CorrectComplete())
        selected_source.selectProfileCollectionResource(ProfileCollectionResource().F03S04P08_SimpleComplete())

        reports = ProvideFromReportDomain(selected_source).collection()

        profile_provider = ProvideFromProfileDomain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        complete_profile_collection = CreateCompleteProfileCollection().mergeReadAndGenerated(read_profiles, generated_profiles, profile_masterlist)

        andrea_profile = complete_profile_collection.profileHavingName("Andrea Wilgacz")

        potential_anomaly_1 = "+ +++"
        potential_anomaly_2 = "+++ +"
        expected_proper_name = "+ Andrea Wilgacz"

        # When

        profile_text = andrea_profile.to_rd_text()

        # Then

        self.assertTrue(potential_anomaly_1 not in profile_text, "Anomaly detected: '+ +++' before name")
        self.assertTrue(potential_anomaly_2 not in profile_text, "Anomaly detected: '+++ +' before name")
        self.assertTrue(expected_proper_name in profile_text, "Name not present according to wikidot rules in profile")
