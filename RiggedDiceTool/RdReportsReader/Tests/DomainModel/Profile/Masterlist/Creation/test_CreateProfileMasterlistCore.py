import unittest

from RdReportsReader.Resources.TestData.Profile.Masterlist.ProfileMasterlistResource import ProfileMasterlistResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Profile.Masterlist.MockProfileMasterlistSource import MockProfileMasterlistSource
from RdReportsReader.Src.DomModel.CharacterProfile.Masterlist.Structure.Core.Creation.CreateProfileMasterlistCore import CreateProfileMasterlistCore


class TestCreateProfileMasterlistCore(unittest.TestCase):

    def test_with__F03S04P08_Simple(self):

        # Given
        mlist_key = ProfileMasterlistResource().F03S04P08_Simple()
        mlist_source = MockProfileMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)

        # When
        profile_masterlist_core = CreateProfileMasterlistCore().from_text(mlist_text)

        # Then
        self.assertTrue(profile_masterlist_core is not None, "ProfileMasterlistCore returned as None on mock source")

        metadata_count = profile_masterlist_core.metadata_count()

        self.assertTrue(metadata_count == 8, "ProfileMasterlistCore does not contain 8 profiles (as expected with the list)")
