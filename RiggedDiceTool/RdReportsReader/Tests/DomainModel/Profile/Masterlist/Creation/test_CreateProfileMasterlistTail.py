import unittest

from RdReportsReader.Resources.TestData.Profile.Masterlist.ProfileMasterlistResource import ProfileMasterlistResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Profile.Masterlist.MockProfileMasterlistSource import MockProfileMasterlistSource
from RdReportsReader.Src.DomModel.CharacterProfile.Masterlist.Structure.Tail.Creation.CreateProfileMasterlistTail import CreateProfileMasterlistTail


class TestCreateProfileMasterlistTail(unittest.TestCase):

    def test_with_tail(self):

        # Given
        mlist_key = ProfileMasterlistResource().F03S04P08_Simple()
        mlist_source = MockProfileMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)

        tail = ", Tail"
        mlist_text_with_tail = mlist_text + tail

        # When
        report_masterlist_tail = CreateProfileMasterlistTail().from_text(mlist_text_with_tail)

        # Then
        self.assertTrue(report_masterlist_tail is not None, "ProfileMasterlistTail returned as None on mock source")

        retrieved = report_masterlist_tail.to_rd_text()

        self.assertTrue(retrieved is not None, "ProfileMasterlistTail does not contain any text.")
        self.assertTrue(tail in retrieved, "ProfileMasterlistTail does not contain tail.")
