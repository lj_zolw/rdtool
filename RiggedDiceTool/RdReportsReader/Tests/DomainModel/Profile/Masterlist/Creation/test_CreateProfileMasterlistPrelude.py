import unittest

from RdReportsReader.Resources.TestData.Profile.Masterlist.ProfileMasterlistResource import ProfileMasterlistResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Profile.Masterlist.MockProfileMasterlistSource import MockProfileMasterlistSource
from RdReportsReader.Src.DomModel.CharacterProfile.Masterlist.Structure.Prelude.Creation.CreateProfileMasterlistPrelude import CreateProfileMasterlistPrelude


class TestCreateProfileMasterlistPrelude(unittest.TestCase):

    def test_with_no_header(self):

        # Given
        mlist_key = ProfileMasterlistResource().F03S04P08_Simple()
        mlist_source = MockProfileMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)

        # When
        report_masterlist_prelude = CreateProfileMasterlistPrelude().from_text(mlist_text)

        # Then
        self.assertTrue(report_masterlist_prelude is not None, "ProfileMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_prelude.to_rd_text()

        self.assertTrue(not retrieved, "Empty ProfileMasterlistPrelude contains text (while it shouldn't).")

    def test_with_header(self):

        # Given
        mlist_key = ProfileMasterlistResource().F03S04P08_Simple()
        mlist_source = MockProfileMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)

        header = "Header:"
        mlist_text_with_header = header + mlist_text

        # When
        report_masterlist_prelude = CreateProfileMasterlistPrelude().from_text(mlist_text_with_header)

        # Then
        self.assertTrue(report_masterlist_prelude is not None, "ProfileMasterlistPrelude returned as None on mock source")

        retrieved = report_masterlist_prelude.to_rd_text()

        self.assertTrue(retrieved is not None, "ProfileMasterlistPrelude does not contain any text.")
        self.assertTrue(header in retrieved, "ProfileMasterlistPrelude does not contain header.")
