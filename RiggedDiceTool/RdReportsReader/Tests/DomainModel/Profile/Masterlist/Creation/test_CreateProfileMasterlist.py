import unittest

from RdReportsReader.Resources.TestData.Profile.Masterlist.ProfileMasterlistResource import ProfileMasterlistResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.Structure.Profile.Masterlist.MockProfileMasterlistSource import MockProfileMasterlistSource
from RdReportsReader.Src.DomModel.CharacterProfile.Masterlist.Creation.ProfileMasterlistFactory import ProfileMasterlistFactory


class TestCreateProfileMasterlist(unittest.TestCase):

    def test_with__F03S04P08_Simple(self):

        # Given
        mlist_key = ProfileMasterlistResource().F03S04P08_Simple()
        mlist_source = MockProfileMasterlistSource()

        mlist_text = mlist_source.returnSelectedMasterlistHavingKey(mlist_key)

        # When
        report_masterlist = ProfileMasterlistFactory().createFromText(mlist_text)

        # Then
        self.assertTrue(report_masterlist is not None, "ProfileMasterlistCore returned as None on mock source")
