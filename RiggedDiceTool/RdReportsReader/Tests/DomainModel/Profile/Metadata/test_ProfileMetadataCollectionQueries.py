import unittest

from RdReportsReader.Src.DomModel.CharacterProfile.Metadata.Collection.Creation.CreateProfileMetadataCollection import CreateProfileMetadataCollection


class TestProfileMetadataCollectionQueries(unittest.TestCase):

    def test_profile_metadata_collection_query__contains_name(self):

        # Given
        text_block = """mag: [[[inwazja-karty-postaci-1411:eryk-plomien|Eryk Płomień]]] (Piter)
mag: [[[inwazja-karty-postaci-1411:pawel-sepiak|Paweł Sępiak]]] (Bebuk)
mag: [[[inwazja-karty-postaci-1604:siluria-diakon|Siluria Diakon]]] (Kić) - Siluria Tyrania (Sabina)

mag: [[[inwazja-karty-postaci-1411:aleksandra-trawens|Aleksandra Trawens]]]"""

        metadata = CreateProfileMetadataCollection().from_text_block(text_block)

        # When

        result = metadata.contains_profile_name("Paweł Sępiak")

        # Then
        self.assertTrue(result is True, "ProfileMetadataCollection 'contains name' query did not recognize Paweł Sępiak.")

    def test_profile_metadata_collection_query__record_having_name(self):

        # Given
        text_block = """mag: [[[inwazja-karty-postaci-1411:eryk-plomien|Eryk Płomień]]] (Piter)
mag: [[[inwazja-karty-postaci-1411:pawel-sepiak|Paweł Sępiak]]] (Bebuk)
mag: [[[inwazja-karty-postaci-1604:siluria-diakon|Siluria Diakon]]] (Kić) - Siluria Tyrania (Sabina)

mag: [[[inwazja-karty-postaci-1411:aleksandra-trawens|Aleksandra Trawens]]]"""

        metadata = CreateProfileMetadataCollection().from_text_block(text_block)

        # When

        result = metadata.retrieve_single_metadata_having_name("Paweł Sępiak")

        # Then
        self.assertTrue(result.characterName() == "Paweł Sępiak", "ProfileMetadataCollection 'retrieve record having name' query did not retrieve Paweł Sępiak.")

