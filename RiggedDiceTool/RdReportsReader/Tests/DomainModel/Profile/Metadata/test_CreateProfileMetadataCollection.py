import unittest

from RdReportsReader.Src.DomModel.CharacterProfile.Metadata.Collection.Creation.CreateProfileMetadataCollection import CreateProfileMetadataCollection


class TestCreateProfileMetadataCollection(unittest.TestCase):

    def test_profile_metadata_collection_rdformat_created_properly(self):

        # Given
        text_block = """mag: [[[inwazja-karty-postaci-1411:eryk-plomien|Eryk Płomień]]] (Piter)
mag: [[[inwazja-karty-postaci-1411:pawel-sepiak|Paweł Sępiak]]] (Bebuk)
mag: [[[inwazja-karty-postaci-1604:siluria-diakon|Siluria Diakon]]] (Kić) - Siluria Tyrania (Sabina)

mag: [[[inwazja-karty-postaci-1411:aleksandra-trawens|Aleksandra Trawens]]]"""

        # When
        metadata = CreateProfileMetadataCollection().from_text_block(text_block)

        # Then
        self.assertTrue(metadata is not None, "ProfileMetadataCollection has not been created at all.")
        self.assertTrue(metadata.profiles_count() == 4, "ProfileMetadataCollection does not have proper (4) amount of records.")

