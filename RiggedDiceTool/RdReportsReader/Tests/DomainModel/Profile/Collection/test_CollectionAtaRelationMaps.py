import unittest
import re

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Resources.TestData.Profile.Collection.ProfileCollectionResource import ProfileCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Read.Provider.CharacterProfile.ProvideFromProfileDomain import ProvideFromProfileDomain
from RdReportsReader.Src.DomModel.CharacterProfile.Complete.Collection.Creation.CreateCompleteProfileCollection import CreateCompleteProfileCollection


class TestCollectionAtaRelationMaps(unittest.TestCase):

    def test_ata_relation_map__should_not_have_profile_masterlist_link__if_character_is_not_in_profile_masterlist(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M06_CorrectComplete())
        selected_source.selectProfileCollectionResource(ProfileCollectionResource().F03S04P08_SimpleComplete())

        reports = ProvideFromReportDomain(selected_source).collection()

        profile_provider = ProvideFromProfileDomain(selected_source)
        read_profiles = profile_provider.collection()
        profile_masterlist = profile_provider.masterlist()

        generated_profiles = reports.generate_profiles()

        complete_profile_collection = CreateCompleteProfileCollection().mergeReadAndGenerated(read_profiles, generated_profiles, profile_masterlist)
        single_profile = complete_profile_collection.profileHavingName("Andromeda (Kasia Nowak)")

        # When

        profile_text = single_profile.to_rd_text()

        # Then

        matches = re.findall('.*August Bankierz.*', profile_text)

        self.assertTrue(len(matches) == 1, "A number different to 1 record of August Bankierz has been found in AtaRelationMap; expected exactly 1, got: " + str(len(matches)))

        character_from_report = matches[0]
        char_lines = character_from_report.split("||")

        name_line = ""
        for single_line in char_lines:
            if "August Bankierz" in single_line:
                name_line = single_line
                break

        contains_link = "[[[" in name_line or "]]]" in name_line
        self.assertTrue(contains_link is False, "AtaRelationMap contains link to ProfileMasterlist where the Character does not have its own profile.")

    def test_ata_relation_map__should_have_profile_masterlist_link__if_character_is_in_profile_masterlist(self):

        # Given

        selectedSource = MockAggregatedSource()
        selectedSource.selectReportCollectionResource(ReportCollectionResource().C01M06_CorrectComplete())
        selectedSource.selectProfileCollectionResource(ProfileCollectionResource().F03S04P08_SimpleComplete())

        reports = ProvideFromReportDomain(selectedSource).collection()

        profileProvider = ProvideFromProfileDomain(selectedSource)
        readProfiles = profileProvider.collection()
        profileMasterlist = profileProvider.masterlist()

        generatedProfiles = reports.generate_profiles()

        completeProfileCollection = CreateCompleteProfileCollection().mergeReadAndGenerated(readProfiles, generatedProfiles, profileMasterlist)
        singleProfile = completeProfileCollection.profileHavingName("Andromeda (Kasia Nowak)")

        # When

        profile_text = singleProfile.to_rd_text()

        # Then

        matches = re.findall('.*Samira Diakon.*', profile_text)

        self.assertTrue(len(matches) == 1, "A number different to 1 record of Samira Diakon has been found in AtaRelationMap; expected exactly 1, got: " + str(len(matches)))

        character_from_report = matches[0]
        char_lines = character_from_report.split("||")

        name_line = ""
        for single_line in char_lines:
            if "Samira Diakon" in single_line:
                name_line = single_line
                break

        contains_link = "[[[" in name_line or "]]]" in name_line
        self.assertTrue(contains_link is True, "AtaRelationMap does not contain link to ProfileMasterlist where the Character has its own profile.")
