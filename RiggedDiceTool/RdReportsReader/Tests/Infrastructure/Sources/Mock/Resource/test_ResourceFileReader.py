import unittest

from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo
from RdReportsReader.Resources.TestData.Profile.Collection.ProfileCollectionResource import ProfileCollectionResource
from RdReportsReader.Resources.TestData.Profile.Masterlist.ProfileMasterlistResource import ProfileMasterlistResource
from RdReportsReader.Resources.TestData.Profile.Single.ProfileSingleResource import ProfileSingleResource
from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Resources.TestData.Report.Single.ReportSingleResource import ReportSingleResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Tool.ResourceReader.ResourcesFileReader import ResourcesFileReader


class TestResourceFileReader(unittest.TestCase):

    def test_readsExistingReportSingle(self):

        # Given
        report_single_folder_path = ResourceTestDataInfo().ReportSingleFolderPath()
        accessible_reports = ReportSingleResource().keys()
        report_to_read = ReportSingleResource().filename_matching_key(accessible_reports[0])

        # When
        resource = ResourcesFileReader().loadResource(report_single_folder_path, report_to_read)

        # Then
        self.assertTrue(resource is not None, "Single report was not read (nothing was read; we got a None).")
        self.assertTrue("+ Konspekt pisany" in resource, "Read resource is not a well formed report.")

    def test_readsExistingReportMasterlist(self):

        # Given
        profile_masterlist_path = ResourceTestDataInfo().ReportMasterlistFolderPath()
        accessible_masterlists = ReportMasterlistResource().keys()
        masterlist_to_read = ReportMasterlistResource().filename_matching_key(accessible_masterlists[0])

        # When
        resource = ResourcesFileReader().loadResource(profile_masterlist_path, masterlist_to_read)

        # Then
        self.assertTrue(resource is not None, "ReportMasterlist was not read (nothing was read; we got a None).")
        self.assertTrue("+++ Inwazja:" in resource, "Read resource is not a well formed ReportMasterlist.")

    def test_readsExistingProfileSingle(self):

        # Given
        profile_single_folder_path = ResourceTestDataInfo().ProfileSingleFolderPath()
        accessible_reports = ProfileSingleResource().keys()
        report_to_read = ProfileSingleResource().filename_matching_key(accessible_reports[0])

        # When
        resource = ResourcesFileReader().loadResource(profile_single_folder_path, report_to_read)

        # Then
        self.assertTrue(resource is not None, "Single profile was not read (nothing was read; we got a None).")
        self.assertTrue("+ " in resource, "Read resource is not a well formed profile.")

    def test_readsExistingProfileMasterlist(self):

        # Given
        profile_masterlist_path = ResourceTestDataInfo().ProfileMasterlistFolderPath()
        accessible_masterlists = ProfileMasterlistResource().keys()
        masterlist_to_read = ProfileMasterlistResource().filename_matching_key(accessible_masterlists[0])

        # When
        resource = ResourcesFileReader().loadResource(profile_masterlist_path, masterlist_to_read)

        # Then
        self.assertTrue(resource is not None, "ProfileMasterlist was not read (nothing was read; we got a None).")
        self.assertTrue("+ Karty postaci Inwazji" in resource, "Read resource is not a well formed ProfileMasterlist.")

    def test_readsExistingReportCollection(self):

        # Given
        report_collection_folder_path = ResourceTestDataInfo().ReportCollectionFolderPath()
        accessible_collections = ReportCollectionResource().keys()
        collection_to_read = ReportCollectionResource().filename_matching_key(accessible_collections[0])

        # When
        resource = ResourcesFileReader().loadResource(report_collection_folder_path, collection_to_read)

        # Then
        self.assertTrue(resource is not None, "ReportCollection was not read (nothing was read; we got a None).")
        self.assertTrue("mlist:" in resource, "Read resource is not a well formed ReportCollection (no mlist:).")
        self.assertTrue("report:" in resource, "Read resource is not a well formed ReportCollection (no report:).")

    def test_readsExistingProfileCollection(self):

        # Given
        collection_folder_path = ResourceTestDataInfo().ProfileCollectionFolderPath()
        accessible_collections = ProfileCollectionResource().keys()
        collection_to_read = ProfileCollectionResource().filename_matching_key(accessible_collections[0])

        # When
        resource = ResourcesFileReader().loadResource(collection_folder_path, collection_to_read)

        # Then
        self.assertTrue(resource is not None, "ProfileCollection was not read (nothing was read; we got a None).")
        self.assertTrue("mlist:" in resource, "Read resource is not a well formed ProfileCollection (no mlist:).")
        self.assertTrue("profile:" in resource, "Read resource is not a well formed ProfileCollection (no profile:).")

    def test_worksWithModuleConstants(self):

        # Given
        collection_folder_path = ResourceTestDataInfo().ProfileCollectionFolderPath()
        collection_to_read_key = ProfileCollectionResource().F03S04P08_SimpleComplete()
        collection_to_read = ProfileCollectionResource().filename_matching_key(collection_to_read_key)

        # When
        resource = ResourcesFileReader().loadResource(collection_folder_path, collection_to_read)

        # Then
        self.assertTrue(resource is not None, "ProfileCollection was not read (nothing was read; we got a None).")
        self.assertTrue("mlist:" in resource, "Read resource is not a well formed ProfileCollection (no mlist:).")
        self.assertTrue("profile:" in resource, "Read resource is not a well formed ProfileCollection (no profile:).")