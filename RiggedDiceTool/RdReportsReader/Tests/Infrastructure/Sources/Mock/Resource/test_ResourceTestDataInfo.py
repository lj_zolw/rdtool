import unittest
import os

from RdReportsReader.Resources.TestData.ResourceTestDataInfo import ResourceTestDataInfo


class TestResourceTestDataInfo(unittest.TestCase):

    def test_ifPathIsProperlyExpandedToRoot(self):

        # Given
        this_location = os.path.dirname(__file__)
        root_of_this_location = os.path.normpath(os.path.join(this_location, '../../../../../'))

        # When
        profile_masterlist_path = ResourceTestDataInfo().ProfileMasterlistFolderPath()

        # Then
        self.assertTrue(root_of_this_location in profile_masterlist_path, "Root is not a part of a masterlist path.")
        self.assertTrue(len(os.listdir(profile_masterlist_path)) > 0, "Path does not point to a place that exists or the directory is empty.")
