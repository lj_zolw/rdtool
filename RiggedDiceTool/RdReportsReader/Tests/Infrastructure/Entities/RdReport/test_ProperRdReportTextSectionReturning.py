import unittest

from RdReportsReader.Src.Infrastructure.Entities.Report.RdReport import RdReport


class TestProperRdReportTextSectionReturns(unittest.TestCase):

    def test_properly_returns_consequences__when_present(self):

        # Given

        input_text_section = """+ Raport właściwy:
some random text here
+ Konsekwencje:
consequences text here
        """
        irrelevant_metadata = None

        report = RdReport(irrelevant_metadata, input_text_section)

        # When
        consequences_section = report.consequences_text_section()

        # Then
        self.assertTrue("consequences text" in consequences_section, "Consequences section was not present in Consequences section - and it should be.")

    def test_does_not_return_consequences__when_not_present(self):

        # Given

        input_text_section = """+ Raport właściwy:
something something Konsekwencje something
        """
        irrelevant_metadata = None

        report = RdReport(irrelevant_metadata, input_text_section)

        # When
        consequences_section = report.consequences_text_section()

        # Then
        self.assertTrue(not consequences_section, "Consequences section is not falsy - and it should be.")
