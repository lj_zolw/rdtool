import unittest

from RdReportsReader.Resources.TestData.Report.Masterlist.ReportMasterlistResource import ReportMasterlistResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain


class TestProvideReportMasterlist(unittest.TestCase):

    def test_reportMasterListProvider_worksWith_C01M06_Correct(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.selectReportMasterlistResource(ReportMasterlistResource().C01_M06_Simple())

        provide = ProvideFromReportDomain(selected_source)

        # When
        report_masterlist = provide.masterlist()

        # Then
        self.assertTrue(report_masterlist is not None, "ReportMasterlist returned as None on mock source")
        self.assertTrue(report_masterlist.metadata_count() == 6, "Wrong amount of reports in masterlist")
        self.assertTrue(report_masterlist.campaign_count() == 1, "Wrong amount of campaigns in masterlist")


