import unittest

from RdReportsReader.Resources.TestData.Report.Collection.ReportCollectionResource import ReportCollectionResource
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Src.Infrastructure.Read.Provider.Report.ProvideFromReportDomain import ProvideFromReportDomain


class TestProvideReportMasterlist(unittest.TestCase):

    def test_reportCollectionProvider_worksWith_C01M06_CorrectComplete(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M06_CorrectComplete())

        provide = ProvideFromReportDomain(selected_source)

        # When
        report_collection = provide.collection()

        self.assertTrue(report_collection is not None, "Provider returned None instead of a ReportCollection.")
        self.assertTrue(report_collection.count() == 6, "Returned ReportCollection did not contain 6 missions as expected.")

    def test_reportCollectionProvider_worksWith_C01M02_MPW_Simplest_Locations(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.selectReportCollectionResource(ReportCollectionResource().C01M02_MPW_Simplest_Locations())

        provide = ProvideFromReportDomain(selected_source)

        # When
        report_collection = provide.collection()

        self.assertTrue(report_collection is not None, "Provider returned None instead of a ReportCollection.")
        self.assertTrue(report_collection.count() == 2, "Returned ReportCollection did not contain 6 missions as expected.")
