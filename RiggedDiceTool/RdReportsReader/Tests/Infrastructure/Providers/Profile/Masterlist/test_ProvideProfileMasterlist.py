import unittest

from RdReportsReader.Src.Infrastructure.Read.Provider.CharacterProfile.ProvideFromProfileDomain import ProvideFromProfileDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Resources.TestData.Profile.Masterlist.ProfileMasterlistResource import ProfileMasterlistResource


class TestProvideProfileMasterlist(unittest.TestCase):

    def test_reportMasterListProvider_worksWith_C01M06_Correct(self):

        # Given
        selected_source = MockAggregatedSource()
        selected_source.selectProfileMasterlistResource(ProfileMasterlistResource().F03S04P08_Simple())

        provide = ProvideFromProfileDomain(selected_source)

        # When

        profile_masterlist = provide.masterlist()

        # Then

        self.assertTrue(profile_masterlist is not None, "ProfileMasterlist returned as nil on mock source")
        self.assertTrue(profile_masterlist.metadata_count() == 8, "Wrong amount of reports in profileMasterlist")
