import unittest

from RdReportsReader.Src.Infrastructure.Read.Provider.CharacterProfile.ProvideFromProfileDomain import ProvideFromProfileDomain
from RdReportsReader.Src.Infrastructure.Read.Source.Mock.Domain.MockAggregatedSource import MockAggregatedSource
from RdReportsReader.Resources.TestData.Profile.Collection.ProfileCollectionResource import ProfileCollectionResource


class TestProvideProfileCollection(unittest.TestCase):

    def test_profileCollectionProvider_worksWith_P03_CorrectComplete(self):

        # Given

        selected_source = MockAggregatedSource()
        selected_source.selectProfileCollectionResource(ProfileCollectionResource().F03S04P08_SimpleComplete())

        provide = ProvideFromProfileDomain(selected_source)

        # When

        profile_collection = provide.collection()

        # Then

        self.assertTrue(profile_collection is not None, "Provider returned None instead of a ProfileCollection.")
        self.assertTrue(profile_collection.count() == 8, "Returned ProfileCollection did not contain 8 profiles as expected.")
