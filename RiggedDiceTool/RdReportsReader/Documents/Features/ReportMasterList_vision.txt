ReportMasterList is composed of following structural systems:

How does it look like:

    <Prelude>

    +++ Inwazja:

    * [[[konspekty-zacmienie:120507-preludium-historia-swiata|001 - 120507 - Preludium: Historia �wiata]]]

    ++++ Pierwsza Inwazja:

    ++++ �wiat�o w Zale�u Le�nym

    * [[[konspekty-zacmienie:150427-kult-zaleskiego-aniola|002 - 150427 - Kult zaleskiego anio�a (An, Mo)]]]
    * [[[konspekty-zacmienie:150429-terminusi-w-zalezu|003 - 150429 - Terminusi w Zale�u (JG, MG)]]]
    * [[[konspekty-zacmienie:150501-szalona-czarodziejka-zaleza|004 - 150501 - Szalona "czarodziejka" Zale�a (PT)]]]

    ++++ Czarodziejka Luster

    * [[[konspekty-zacmienie:120918-mozna-doprowadzic-maga-do|005 - 120918 - Mo�na doprowadzi� maga do problemu... (An)]]]
    * [[[konspekty-zacmienie:120920-ale-nie-mozna-zmusic-go-do|006 - 120920 - Ale nie mo�na zmusi� go do jego rozwi�zania... (An)]]]
    (...)

    ++++ Poza czasem (alternatywne? Nie umieszczone? Anulowane?)
    * [[[konspekty-zacmienie:140702-misleg-zniknac|0xx - 140702 - Mi�l�g znikn��! (temp * 3)]]]

    * [[[konspekty-zacmienie:130514-chlopak-ktory-chcial-byc-bohat|0xx - 130514 - (SKR) Ch�opak, kt�ry chcia� by� bohaterem (Do, Jan, Ca)]]]
    * [[[konspekty-zacmienie:130529-zguba-w-muzeum|0xx - 130529 - Zguba w muzeum (temp?)]]]
    * [[[konspekty-zacmienie:131117-cel-uswieca-srodki-op|0xx - 131117 - (RD) Cel u�wi�ca �rodki - pocz�tek (Pa)]]]

    ++++ Koniec znanej historii Inwazji

    <Tail>

How does it match into MasterList structure:

logical structure:

MasterList
--- Prelude
--- Invasion
------ "T�o"
------ "Pierwsza Inwazja"
------ "�wiat�o w Zale�u Le�nym"
---------- Metadata "Kult zaleskiego anio�a"
---------- Metadata "Terminusi w Zale�u"
---------- Metadata "Szalona 'czarodziejka' Zale�a"
------ "Czarodziejka Luster"
    (...)
------ Koniec znanej historii Inwazji
--- Tail

iterated construction:

MasterList
--- Prelude
--- Invasion               |
------ Campaign            | loop
--------- ReportMetadata   | loop in loop
------ Koniec znanej historii Inwazji
--- Tail


Prelude:                    text being present BEFORE Invasion starts; should copy/paste
Core:                       core
    Campaign:               campaign name
        ReportMetadata:     report metadata
    finalizer               special campaign key to finish
Tail:                       text being present AFTER Invasion ends; should copy/paste