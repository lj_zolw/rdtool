The postcode problem.
We have a location tree. Something like that the below:

# Świat
 # Śląsk
  # Kopalin
   # Dzielnica Trzech Myszy
    # Kanały, gdzie Blackenauerowie zbudowali sieć ewakuacji
   # Dzielnica Owadów
    # Szpital Pasteura, ufortyfikowany, gdzie mieści się portal Millennium prowadzący poza Kopalin
    # Kanały, gdzie Blackenauerowie zbudowali sieć ewakuacji

While building deeds, every single identifier of the location (that is, the thing before the comma) will become a distinct location entity.
Something like a character profile, but for a distinct location. So we would have a location like: Swiat_Slask_Kopalin_DzielnicaX_Kanaly and this would
represent the canals below the Kopalin city located in a particular district. Thing is, we have more than one district and the canals
are located in some of them: Swiat_Slask_Kopalin_DzielnicaZ_Kanaly.

And every location page, or location entity needs to have a unique readable link - like a character profile.
Yet unlike the profiles we cannot guarantee the uniqueness of the identifier. We can create a naïve approach - link containing whole path to it,
like: Swiat_Slask_Kopalin_DzielnicaZ_Kanaly. But wikidot allows only links of length 256, not more. I do have a problem with reports sometimes.
This implies, that this is not really a solution.

To make everything more fun, the links are supposed to be readable by human (so I can hover above a link and know where is it leading)
but also they are supposed to be unchanging. I would like to give the location entities the character profile treatment -
a part of location is auto generated, the historical part (read through the reports) but the second part is supposed to be
something like me writing down history of a place, things to know, things one should remember about the city and which did not appear in any report.
Something like the current mechanics of character profile.

In the reality surrounding us this problem is rectified by something called postcodes. But location master list is not created; it is emergent.
This means that a new district or a new section can pop up in every second. And as shown before, we want the location entities to have unique links
and links which do not change at all. That is, no matter how the master list changes, the links are to be guaranteed unchanging for a given the path to a particular location.

The obvious solution is to take SHA or MD5 and apply it on a path. Those algorithms are supposed to generate a unique string;
one could make it eight (or 16?) characters if I remember properly and this could become a part of the link.
Now, this would solve some problems. The link would look like this, more or less:

karty-lokacji:12345678-kanaly

But this link may not be readable enough; for now, I am not really automatically generating things on a website.
Right now I am generating things into text files which I manually move to the website, because I do not want to manually pollute
my only data source (which would be “slightly inconvenient”). Therefore I would not like to open the locations folder and
find a lot of random numbers followed by potentially nondescriptive names.

A better form of the link would be:

karty-lokacji:kopalin-12345678-kanaly

Because at least I would be able to look at the master city in which the particular location is present.
And this is the approach which - I think - I should be going forward with.
