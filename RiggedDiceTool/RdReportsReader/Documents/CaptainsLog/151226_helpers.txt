A very interesting object to consider would be the one denoted below:

require_relative '../../Src/DomModel/CharacterProfile/CommonTools/ProfileListDataExtractor'

Note this: introduction of the helper object even in form of a common tool – even in place were the code is actually
really reusable led to slightly problematic anomaly in the collection objects which use this tool. Contrast it with two following classes below:

require_relative '../../Src/DomModel/CharacterProfile/Complete/Single/Structure/History/Structure/AtaRelations/Creation/CreateAtaRelationHistory'
require_relative '../../Src/DomModel/CharacterProfile/Complete/Single/Structure/History/Structure/Deeds/Creation/CreateMissionDeedsHistory'

The difference between the helper object and those two creators results from the fact that profile list data extractor
does not really know how will how will a object look like, because it doesn’t have access to the type of the collection.
Of course, it is possible to create such a type aware collection, but then it would be much easier to simply create two
classes without playing with a single helper object.

The result of those collections can be null. In type aware collection I am able to call a particular factory to generate a null object scenario.
We have a helper object which by definition is supposed to help in many places – it needs to be type not aware.

Therefore the nullability cascaded. It is not located in profile list data extractor, it has forced me
to create two subfactories to deal with nullability.

This is why reusability might not be the holy Grail we always thought it is. It has its disadvantages as well and it might be worth it
to create a slightly duplicated code structure, but the code is only duplicated mechanically while the intention of the code is always singular.
